package com.wissen.xcelerate.encircle.pojo;

import java.io.Serializable;

/*
 * class for Customer.
 */
public class Customer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int restCompId;
	private String customerName;
    private String address1;
	private String address2;
    private String city;
	private String email;
    private String firstName;
    private String lastName;
    private String mobile;
	private String phone;
	private String workPhone;
	private String state;
    private String zip;
    private String customerType;
    private String lossDescription;
    
    public Customer(){}
    
    /**
	 * Method to get Customer Name.
	 * @return Customer Name as string.
	 */
	public String getCustomerName() {
		return customerName;
	}
	
	/**
	 * Method to set Customer Name.
	 * @param  Customer Name-the Customer Name is string.
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	/**
	 * Method to get Address 1.
	 * @return Address 1 as string.
	 */
	public String getAddress1() {
		return address1;
	}
	
	/**
	 * Method to set Address 1.
	 * @param  Address 1-the Address 1 is string.
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	/**
	 * Method to get Address 2.
	 * @return Address 2 as string.
	 */
	public String getAddress2() {
		return address2;
	}
	
	/**
	 * Method to set Address 2.
	 * @param  Address 2-the Address 2 is string.
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	/**
	 * Method to get city.
	 * @return city as string.
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * Method to set city.
	 * @param  city-the city is string.
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * Method to get email.
	 * @return email as string.
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Method to set email.
	 * @param  email-the email is string.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Method to get First Name.
	 * @return First Name as string.
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Method to set First Name.
	 * @param  First Name-the First Name is string.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * Method to get Last Name.
	 * @return Last Name as string.
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Method to set Last Name.
	 * @param  Last Name-the Last Name is string.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Method to get mobile.
	 * @return mobile as string.
	 */
	public String getMobile() {
		return mobile;
	}
	
	/**
	 * Method to set mobile.
	 * @param  mobile-the mobile is string.
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	/**
	 * Method to get phone.
	 * @return phone as string.
	 */
	public String getPhone() {
		return phone;
	}
	
	/**
	 * Method to set phone.
	 * @param  phone-the phone is string.
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	/**
	 * Method to get Work Phone .
	 * @return Work Phone as string.
	 */
	public String getWorkPhone() {
		return workPhone;
	}
	
	/**
	 * Method to set Work Phone.
	 * @param Work Phone-the Work Phone is string.
	 */
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	
	/**
	 * Method to get State .
	 * @return State as string.
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Method to set State.
	 * @param State-the State is string.
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Method to get Zip .
	 * @return Zip as string.
	 */
	public String getZip() {
		return zip;
	}
	
	/**
	 * Method to set Zip.
	 * @param Zip-the Zip is string.
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	/**
	 * Method to get Rest CompId .
	 * @return Rest CompId as integer.
	 */
	public int getRestCompId() {
		return restCompId;
	}
	
	/**
	 * Method to Rest CompId.
	 * @param Rest CompId-the Rest CompId is integer.
	 */
	public void setRestCompId(int restCompId) {
		this.restCompId = restCompId;
	}
	
	/**
	 * Method to get Customer Type .
	 * @return Customer Type as string.
	 */
	public String getCustomerType() {
		return customerType;
	}
	
	/**
	 * Method to Customer Type.
	 * @param Customer Type-the Customer Type is string.
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	
	/**
	 * Method to get Loss Description .
	 * @return Loss Description as string.
	 */
	public String getLossDescription() {
		return lossDescription;
	}
	
	/**
	 * Method to Loss Description.
	 * @param Loss Description-the Loss Description is string.
	 */
	public void setLossDescription(String lossDescription) {
		this.lossDescription = lossDescription;
	}
}
