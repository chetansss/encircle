package com.wissen.xcelerate.encircle.repository;

/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

import java.util.List;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.wissen.xcelerate.encircle.model.CommonTasks;

/**
 * Repository class for common tasks list of find By SourceId And TaskSequence Is Greater Than Order By Task Sequence,
 * find By Source And SourceId And Action Required And Status
 * @author Rama
 */
public interface CommonTasksRepository extends Repository<CommonTasks, Long> {
	
	CommonTasks save(CommonTasks task);
	
	
	List<CommonTasks> findBySourceIdAndTaskSequenceIsGreaterThanOrderByTaskSequence
	(@Param("sourceId") int sourceId,@Param("taskSequence") int taskSequence);
	CommonTasks findBySourceIdAndTaskSequence(@Param("sourceId") int sourceId,
			@Param("taskSequence") int taskSequence);
	
	List<CommonTasks> findBySourceAndSourceIdAndActionRequiredAndStatus(@Param("source") String
	source,@Param("sourceId") int sourceId,@Param("actionRequired")
	String actionRequired,@Param("status") String status);
	 
	/*
	 * List<CommonTasks> findBySource(String source);
	 * 
	 * List<CommonTasks> findBySourceAndSourceId(@Param("source") String
	 * source,@Param("sourceId") int sourceId);
	 * 
	 * List<CommonTasks>
	 * findBySourceAndSourceIdAndActionRequiredAndStatus(@Param("source") String
	 * source,@Param("sourceId") int sourceId,@Param("actionRequired") String
	 * actionRequired,@Param("status") String status);
	 * 
	 * List<CommonTasks>
	 * findBySourceAndSourceIdAndAssignTypeAndAssignTo(@Param("source") String
	 * source,
	 * 
	 * @Param("sourceId") int sourceId,@Param("assignType") String
	 * assignType,@Param("assignTo") Integer assignTo);
	 * 
	 * List<CommonTasks>
	 * findByStatusAndSourceOrderByExpectedEndDatetime(@Param("status") String
	 * status,@Param("source") String source);
	 * 
	 * List<CommonTasks>
	 * findByAssignTypeAndStatusOrderByExpectedEndDatetime(@Param("assignType")
	 * String assignType,@Param("status") String status);
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.assignTo is not null and t.assignTo>0 and 
	 * (t.status='Open') order by t.expectedEndDatetime"
	 * ) List<CommonTasks> findAssignedTasks();
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.status='Open' order by t.expectedEndDatetime"
	 * ) List<CommonTasks> findOpenTasks();
	 * 
	 * @Query("SELECT t FROM CommonTasks t WHERE t.expectedEndDatetime<:currentDatetime and 
	 * t.status!='Completed' and t.status!='Rejected' order by t.expectedEndDatetime"
	 * ) List<CommonTasks> find(@Param("currentDatetime") Timestamp
	 * currentDatetime);
	 * 
	 * //Need to change the below query once user and department mapping is done
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.status=:status and ((t.assignType='Role' and 
	 * t.assignTo=:userId) or (t.assignType='User' and t.assignTo=:userId)) order by t.expectedEndDatetime"
	 * ) List<CommonTasks> findMyTasks(@Param("status") String
	 * status, @Param("userId") int userId);
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.status=:status and t.assignType='Role' order by t.expectedEndDatetime"
	 * ) List<CommonTasks> findMyTasksForAdmin(@Param("status") String status);
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.status='Open' and t.sourceId in (select j.jobId 
	 * from JobDetails j where j.restCompLocationId=:locationId) and t.assignType='Division' and (select count(ud)
	 *  from UserDivision ud where ud.userId=:userId and ud.status='Active' and ud.divisionId=t.assignTo)>0"
	 * ) List<CommonTasks> findMyDivisionTasks(@Param("userId") int
	 * userId,@Param("locationId") int locationId);
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.status='Open' and t.sourceId in (select j.jobId 
	 * from JobDetails j where j.restCompLocationId=:locationId) and t.assignType IN('Division','Role') "
	 * +
	 * " and ((select count(ud) from UserDivision ud where ud.userId=:userId and ud.status='Active' 
	 * and t.assignType='Division' and ud.divisionId=t.assignTo)>0 or (select count(ur) from
	 *  UserUserRoles ur where ur.userId=:userId and ur.roleId=t.assignTo and t.assignType='Role'
	 *   and ur.status='Active')>0)"
	 * ) List<CommonTasks> findMyDivisionOrRoleTasks(@Param("userId") int
	 * userId,@Param("locationId") int locationId);
	 * 
	 * //@
	 * Query("SELECT t FROM CommonTasks t where t.sourceId in (select j.jobId from 
	 * JobDetails j where j.restCompLocationId=:locationId) and ((t.assignType='Role' 
	 * and t.assignTo=:userId) or (t.assignType='User' and t.assignTo=:userId))
	 *  order by t.expectedEndDatetime desc"
	 * )
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.sourceId in (select j.jobId 
	 * from JobDetails j where j.restCompLocationId=:locationId) and (t.assignType='User' 
	 * and t.assignTo=:userId) and t.status='Open' order by t.expectedEndDatetime desc"
	 * ) List<CommonTasks> findDashboardTasks(@Param("userId") int
	 * userId,@Param("locationId") int locationId);
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.expectedEndDatetime<:currentDatetime 
	 * and t.status!='Completed' and t.status!='Rejected' and ((t.assignType='Role' 
	 * and t.assignTo=:userId) or (t.assignType='Division')) order by t.expectedEndDatetime"
	 * ) List<CommonTasks> findMyOverdueTasks(@Param("userId") int
	 * userId,@Param("currentDatetime") Timestamp currentDatetime);
	 * 
	 * CommonTasks findByTaskId(@Param("taskId") int taskId);
	 * 
	 * CommonTasks findBySourceIdAndTaskSequence(@Param("sourceId") int
	 * sourceId,@Param("taskSequence") int taskSequence);
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.sourceId=:sourceId and  t.status!='Completed'
	 *  and t.status!='Rejected'"
	 * ) List<CommonTasks> findOpenTasksByJob(@Param("sourceId") int sourceId);
	 * 
	 * List<CommonTasks>
	 * findBySourceIdAndTaskSequenceIsGreaterThanOrderByTaskSequence(@Param(
	 * "sourceId") int sourceId,@Param("taskSequence") int taskSequence);
	 * 
	 * CommonTasks save(CommonTasks task);
	 * 
	 * // Kumuda -- Mobile related
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.status='Open' and t.sourceId in "
	 * +
	 * " (select j.jobId from JobDetails j where j.restCompLocationId=:locationId)
	 *  and t.assignType IN ('Division','Role','User') and "
	 * +
	 * " ((select count(ud) from UserDivision ud where ud.userId=:userId and ud.status='Active'
	 *  and t.assignType='Division' and ud.divisionId=t.assignTo)>0 or "
	 * +
	 * " (select count(ur) from UserUserRoles ur where ur.userId=:userId and ur.roleId=t.assignTo 
	 * and t.assignType='Role' and ur.status='Active')>0 or "
	 * +
	 * " (select count(us) from UserDetails us where us.userId=:userId and t.assignType='User' 
	 * and us.status='Active')>0) order by t.expectedEndDatetime "
	 * ) List<CommonTasks> getAllOpenTasks(@Param("userId") int
	 * userId,@Param("locationId") int locationId);
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.status='Open' and t.sourceId in
	 *  (select j.jobId from JobDetails j where j.restCompLocationId=:locationId) and "
	 * +
	 * "((t.assignType='User' and t.assignTo=:userId) or (t.assignType IN('Division','Role') "
	 * +
	 * " and ((select count(ud) from UserDivision ud where ud.userId=:userId and ud.status='Active'
	 *  and t.assignType='Division' and ud.divisionId=t.assignTo)>0 or (select count(ur) 
	 *  from UserUserRoles ur where ur.userId=:userId and ur.roleId=t.assignTo and t.assignType='Role' and ur.status='Active')>0)))"
	 * ) List<CommonTasks> getMyOpenTasks(@Param("userId") int
	 * userId,@Param("locationId") int locationId);
	 * 
	 * List<CommonTasks>
	 * findBySourceIdAndStatusAndPreviousStatusIsNotNull(@Param("sourceId") int
	 * sourceId,@Param("status") String status);
	 * 
	 * // Added by jani
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.status!='To Do'")
	 * List<CommonTasks> findAll();
	 * 
	 * @Query("SELECT t FROM CommonTasks t where t.status!='To Do' and t.assignTo is
	 *  not null and ((t.assignType='User' and t.assignTo=:userId)or (t.assignType IN('Division','Role') "
	 * +
	 * " and ((select count(ud) from UserDivision ud where ud.userId=:userId and
	 *  ud.status='Active' and t.assignType='Division' and ud.divisionId=t.assignTo)>0 or 
	 *  (select count(ur) from UserUserRoles ur where ur.userId=:userId and
	 *   ur.roleId=t.assignTo and t.assignType='Role' and ur.status='Active')>0)))) order by t.expectedEndDatetime"
	 * ) List<CommonTasks> findMyTasksByUserId(@Param("userId") int userId); // end
	 */}
 
