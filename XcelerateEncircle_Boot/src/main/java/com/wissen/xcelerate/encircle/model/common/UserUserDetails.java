/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model.common;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the user_user_details database table.
 */
@Entity
@Table(name="user_user_details",schema="common")
//@NamedQuery(name="UserUserDetails.findAll", query="SELECT u FROM UserUserDetails u")
public class UserUserDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_detail_id")
	private int userDetailId;

	@Column(name="created_by")
	private int createdBy;

	@Column(name="created_datetime")
	private Timestamp createdDatetime;

	@Column(name="email_id")
	private String emailId;

	@Column(name="first_name")
	private String firstName;

	@Column(name="inactive_reason")
	private String inactiveReason;

	@Column(name="last_name")
	private String lastName;

	@Column(name="mobile_no")
	private String mobileNo;

	@Column(name="phone_no")
	private String phoneNo;

	@Column(name="profile_pic")
	private Integer profilePic;
	
	@Column(name="time_zone")
	private String timeZone;

	private String status;

	@Column(name="user_id")
	private int userId;

	@Column(name="user_name")
	private String userName;
	
	@Column(name="updated_by")
	private Integer updatedBy;

	@Column(name="updated_datetime")
	private Timestamp updatedDatetime;
	
	@Column(name="privacy_policy")
	private String privacyPolicy;

	//bi-directional many-to-one association to UserUserAuthentication
	@OneToMany(mappedBy="userUserDetail")
	private List<UserUserAuthentication> userUserAuthentications;

	/**
	 * Method to User UserDetails
	 */
	public UserUserDetails() {
	}

	/**
	 * Method to get User DetailId.
	 * @return User DetailId as integer.
	 */
	public int getUserDetailId() {
		return this.userDetailId;
	}

	/**
	 * Method to set User DetailId.
	 * @param  User DetailId-the User DetailId is integer
	 */
	public void setUserDetailId(int userDetailId) {
		this.userDetailId = userDetailId;
	}

	/**
	 * Method to get CreatedBy.
	 * @return CreatedBy as integer.
	 */
	public int getCreatedBy() {
		return this.createdBy;
	}

	/**
	 * Method to set CreatedBy.
	 * @param  CreatedBy-the CreatedBy is integer
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Method to get Created Datetime.
	 * @return Created Datetime as timestamp.
	 */
	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	/**
	 * Method to set Created Datetime.
	 * @param  Created Datetime-the Created Datetime is timestamp.
	 */
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	/**
	 * Method to get EmailId.
	 * @return EmailId as string.
	 */
	public String getEmailId() {
		return this.emailId;
	}

	/**
	 * Method to set EmailId.
	 * @param  EmailId-the EmailId is string.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * Method to get First Name.
	 * @return First Name as string.
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Method to set First Name.
	 * @param  First Name-the First Name is string.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Method to get Inactive Reason.
	 * @return Inactive Reason as string.
	 */
	public String getInactiveReason() {
		return this.inactiveReason;
	}

	/**
	 * Method to set Inactive Reason.
	 * @param  Inactive Reason-the Inactive Reason is string.
	 */
	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}

	/**
	 * Method to get Last Name.
	 * @return Last Name as string.
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Method to set Last Name.
	 * @param  Last Name-the Last Name is string.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Method to get MobileNo.
	 * @return MobileNo as string.
	 */
	public String getMobileNo() {
		return this.mobileNo;
	}

	/**
	 * Method to set MobileNo.
	 * @param  MobileNo-the MobileNo is string.
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * Method to get PhoneNo.
	 * @return PhoneNo as string.
	 */
	public String getPhoneNo() {
		return this.phoneNo;
	}

	/**
	 * Method to set PhoneNo.
	 * @param PhoneNo-the PhoneNo is string.
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * Method to get ProfilePic.
	 * @return ProfilePic as integer.
	 */
	public Integer getProfilePic() {
		return this.profilePic;
	}

	/**
	 * Method to set ProfilePic.
	 * @param ProfilePic-the ProfilePic is integer.
	 */
	public void setProfilePic(Integer profilePic) {
		this.profilePic = profilePic;
	}

	/**
	 * Method to get TimeZone
	 * @return TimeZone as string.
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * Method to set TimeZone.
	 * @param TimeZone-the TimeZone is string.
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * Method to get Status
	 * @return Status as string.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Method to set Status.
	 * @param Status-the Status is string.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Method to get UserId.
	 * @return UserId as integer.
	 */
	public int getUserId() {
		return this.userId;
	}

	/**
	 * Method to set UserId.
	 * @param UserId-the UserId is integer.
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Method to get User Name.
	 * @return User Name as string.
	 */
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Method to set User Name.
	 * @param User Name-the  User Name is string.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Method to get Updated By.
	 * @return Updated By as integer.
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set UpdatedBy.
	 * @param UpdatedBy-the UpdatedBy is integer.
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get Updated Datetime.
	 * @return Updated Datetime as timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set Updated Datetime.
	 * @param Updated Datetime-the Updated Datetime is timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get Privacy Policy.
	 * @return Privacy Policy as string.
	 */
	public String getPrivacyPolicy() {
		return privacyPolicy;
	}

	/**
	 * Method to set Privacy Policy.
	 * @param Privacy Policy-the Privacy Policy is string.
	 */
	public void setPrivacyPolicy(String privacyPolicy) {
		this.privacyPolicy = privacyPolicy;
	}

	/**
	 * Method to get User UserAuthentications.
	 * @return User UserAuthentications as list.
	 */
	public List<UserUserAuthentication> getUserUserAuthentications() {
		return this.userUserAuthentications;
	}

	/**
	 * Method to set User UserAuthentications.
	 * @param User UserAuthentications-the User UserAuthentications is list.
	 */
	public void setUserUserAuthentications(List<UserUserAuthentication> userUserAuthentications) {
		this.userUserAuthentications = userUserAuthentications;
	}

}