/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.hibernate;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * class for multi tenant connection provider.
 */
@Component
public class MultiTenantConnectionProviderImpl implements MultiTenantConnectionProvider {
	private static final long serialVersionUID = 6246085840652870138L;
	@Autowired
	private DataSource dataSource;

	/**
	 * Method to establish DB connection.
	 * @return-connection status.
	 */
	@Override
	public Connection getAnyConnection() throws SQLException {
		return dataSource.getConnection();
	}

	/**
	 * Method to close established DB connection.
	 */
	@Override
	public void releaseAnyConnection(Connection connection) throws SQLException {
		connection.close();
	}

	/**
	 * Method to get the connection and creation of statement.
	 * @param-the tenant identifier String.
	 * @return-connection status.
	 */
	@Override
	public Connection getConnection(String tenantIdentifier) throws SQLException {
		final Connection connection = getAnyConnection();
		if (tenantIdentifier != null && tenantIdentifier != "common") {
			connection.createStatement().execute("EXECUTE AS USER= '" + tenantIdentifier + "'");
		}
//    try {
//    	  if (tenantIdentifier != null) {
//              connection.createStatement().execute("USE " + tenantIdentifier);
//          } else {
//              connection.createStatement().execute("USE common");
//          }
//    }
//    catch ( SQLException e ) {
//      throw new HibernateException(
//          "Could not alter JDBC connection to specified schema [" + tenantIdentifier + "]",
//          e
//          );
//    }
		return connection;
	}

	/**
	 * Method to close established DB connection.
	 */
	@Override
	public void releaseConnection(String tenantIdentifier, Connection connection) throws SQLException {
//    try {
//    	 connection.createStatement().execute( "USE common");
//    }
//    catch ( SQLException e ) {
//      throw new HibernateException(
//          "Could not alter JDBC connection to specified schema [" + tenantIdentifier + "]",
//          e
//          );
//    }
		connection.createStatement().execute("REVERT;");
		connection.close();
	}

	/**
	 * Method to verify the class wrapped or unwrapped.
	 * @return-status as boolean.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean isUnwrappableAs(Class unwrapType) {
		return false;
	}

	/**
	 * Method to return null if the class is unwrap.
	 */
	@Override
	public <T> T unwrap(Class<T> unwrapType) {
		return null;
	}

	/**
	 * Method to verify Agressive release is supported or not.
	 * @return-status as boolean.
	 */
	@Override
	public boolean supportsAggressiveRelease() {
		return true;
	}
}