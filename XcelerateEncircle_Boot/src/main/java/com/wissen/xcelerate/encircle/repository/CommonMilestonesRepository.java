package com.wissen.xcelerate.encircle.repository;

/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

import java.util.List;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.wissen.xcelerate.encircle.model.CommonMilestones;

/**
 * Repository class for common milestone list of find By Source Category And SourceId 
 */
public interface CommonMilestonesRepository extends Repository<CommonMilestones, Long>{
	
	CommonMilestones save(CommonMilestones milestone);
	
	CommonMilestones findBySourceId(@Param("sourceId") int sourceId);
	
	List<CommonMilestones> findBySourceCategoryAndSourceId(@Param("sourceCategory") String 
			sourceCategory,@Param("sourceId") int sourceId);
}
