package com.wissen.xcelerate.encircle.model;

/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * The persistent class for the common_milestones database table.
 * class for Common Milestones.
 */
@Entity
@Table(name="common_milestones")
@NamedQuery(name="CommonMilestones.findAll", query="SELECT c FROM CommonMilestones c")
public class CommonMilestones implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="job_milestones_id")
	private int jobMilestonesId;

	@Column(name="dateofLoss")
	private Date dateofLoss;
	
	@Column(name="dateofLoss_description")
	private String dateofLossDescription;
	
	@Column(name="dateReceived")
	private Date dateReceived;
	
	@Column(name="dateReceived_description")
	private String dateReceivedDescription;
	
	@Column(name="datecontacted")
	private Timestamp datecontacted;
	
	@Column(name="datecontacted_description")
	private String datecontactedDescription;
	
	@Column(name="dateinspected")
	private Timestamp dateinspected;
	
	@Column(name="dateinspected_description")
	private String dateinspectedDescription;
	
	@Column(name="dateofWorkAuthorization")
	private Timestamp dateofWorkAuthorization;
	
	@Column(name="dateofWorkAuthorization_description")
	private String dateofWorkAuthorizationDescription;
	
	@Column(name="dateEstimatesent")
	private Timestamp dateEstimatesent;
	
	@Column(name="dateEstimatesent_description")
	private String dateEstimatesentDescription;
	
	@Column(name="dateEstimateApproved")
	private Timestamp dateEstimateApproved;
	
	@Column(name="dateEstimateApproved_description")
	private String dateEstimateApprovedDescription;
	
	@Column(name="dateinventoried")
	private Timestamp dateinventoried;
	
	@Column(name="dateinventoried_description")
	private String dateinventoriedDescription;
	
	@Column(name="intoProductiondate")
	private Timestamp intoProductiondate;
	
	@Column(name="intoProductiondate_description")
	private String intoProductiondateDescription;
	
	@Column(name="target_start_date")
	private Timestamp targetStartDate;
	
	@Column(name="startdate")
	private Timestamp startdate;
	
	@Column(name="startdate_description")
	private String startdateDescription;
	
	@Column(name="dateofmajoritycompletion")
	private Timestamp dateofmajoritycompletion;
	
	@Column(name="dateofmajoritycompletion_description")
	private String dateofmajoritycompletionDescription;
	
	@Column(name="targetcompletiondate")
	private Timestamp targetcompletiondate;
	
	@Column(name="targetcompletiondate_description")
	private String targetcompletiondateDescription;
	
	@Column(name="dateOfcOs")
	private Timestamp dateOfcOs;
	
	@Column(name="dateOfcOs_description")
	private String dateOfcOsDescription;
	
	@Column(name="dateinvoiced")
	private Timestamp dateinvoiced;
	
	@Column(name="dateinvoiced_description")
	private String dateinvoicedDescription;
	
	@Column(name="final_invoice_date")
	private Timestamp finalInvoiceDate;
	
	@Column(name="datePaid")
	private Timestamp datePaid;
	
	@Column(name="datePaid_description")
	private String datePaidDescription;
	
	@Column(name="closeddate")
	private Timestamp closeddate;
	
	@Column(name="closeddate_description")
	private String closeddateDescription;
	
	@Column(name="source_category")
	private String sourceCategory;

	@Column(name="source_id")
	private int sourceId;

	//bi-directional many-to-one association to UserUserDetail
	@Column(name="entered_by")
	private Integer enteredBy;
	
	@Column(name="entered_datetime")
	private Timestamp enteredDatetime;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	@Column(name="updated_datetime")
	private Timestamp updatedDatetime;
	
	private String source;
	
//	@Column(name="warranty_start_date")
//	private Timestamp warrantyStartDate;
//	
//	@Column(name="warranty_end_date")
//	private Timestamp warrantyEndDate;
	
	/**
	 * Method for Common milestone.
	 */
	public CommonMilestones() {
	}

	/**
	 * Method to get Job Milestones Id.
	 * @return job Milestones Id as integer.
	 */
	public int getJobMilestonesId() {
		return jobMilestonesId;
	}

	/**
	 * Method to set Job Milestones Id.
	 * @param  Job Milestones Id-the Job Milestones Id is integer.
	 */
	public void setJobMilestonesId(int jobMilestonesId) {
		this.jobMilestonesId = jobMilestonesId;
	}

	/**
	 * Method to get Date of Loss.
	 * @return Date of Loss as date.
	 */
	public Date getDateofLoss() {
		return dateofLoss;
	}

	/**
	 * Method to set DateofLoss.
	 * @param  DateofLoss-the JDateofLoss is date.
	 */
	public void setDateofLoss(Date dateofLoss) {
		this.dateofLoss = dateofLoss;
	}

	/**
	 * Method to get Date of Loss Description.
	 * @return Date of Loss Description as string.
	 */
	public String getDateofLossDescription() {
		return dateofLossDescription;
	}

	/**
	 * Method to set DateofLoss Description.
	 * @param  DateofLoss Description-the DateofLoss  Description  is string.
	 */
	public void setDateofLossDescription(String dateofLossDescription) {
		this.dateofLossDescription = dateofLossDescription;
	}

	/**
	 * Method to get Date Received.
	 * @return Date Received as date.
	 */
	public Date getDateReceived() {
		return dateReceived;
	}

	/**
	 * Method to set DateReceived.
	 * @param  DateReceived-the DateReceived  is date.
	 */
	public void setDateReceived(Date dateReceived) {
		this.dateReceived = dateReceived;
	}

	/**
	 * Method to get Date Received Description.
	 * @return Date Received Description as string.
	 */
	public String getDateReceivedDescription() {
		return dateReceivedDescription;
	}

	/**
	 * Method to set DateReceived Description.
	 * @param  DateReceived Description-the DateReceived Description string.
	 */
	public void setDateReceivedDescription(String dateReceivedDescription) {
		this.dateReceivedDescription = dateReceivedDescription;
	}

	/**
	 * Method to get Date contacted.
	 * @return Date contacted as Timestamp.
	 */
	public Timestamp getDatecontacted() {
		return datecontacted;
	}

	/**
	 * Method to set Date contacted.
	 * @param  Date contacted-the Date contacted timestamp.
	 */
	public void setDatecontacted(Timestamp datecontacted) {
		this.datecontacted = datecontacted;
	}

	/**
	 * Method to get Date contacted Description.
	 * @return Date contacted Description as string.
	 */
	public String getDatecontactedDescription() {
		return datecontactedDescription;
	}

	/**
	 * Method to set Date contacted Description.
	 * @param  Date contacted Description-the Date contacted Description string.
	 */
	public void setDatecontactedDescription(String datecontactedDescription) {
		this.datecontactedDescription = datecontactedDescription;
	}

	/**
	 * Method to get Date inspected.
	 * @return Date inspected as timestamp.
	 */
	public Timestamp getDateinspected() {
		return dateinspected;
	}

	/**
	 * Method to set Date inspected.
	 * @param  Dateinspected-the Date inspected timestamp.
	 */
	public void setDateinspected(Timestamp dateinspected) {
		this.dateinspected = dateinspected;
	}

	/**
	 * Method to get Date inspected Description.
	 * @return Date inspected Description as string.
	 */
	public String getDateinspectedDescription() {
		return dateinspectedDescription;
	}

	/**
	 * Method to set Date inspected Description.
	 * @param  Dateinspected Description-the Date inspected  Description string.
	 */

	public void setDateinspectedDescription(String dateinspectedDescription) {
		this.dateinspectedDescription = dateinspectedDescription;
	}

	/**
	 * Method to get Date of Work Authorization.
	 * @return Date of Work Authorization as Timestamp.
	 */
	public Timestamp getDateofWorkAuthorization() {
		return dateofWorkAuthorization;
	}

	/**
	 * Method to set Date of Work Authorization.
	 * @param  Dateinspected Description-the DateofWork Authorization timestamp.
	 */
	public void setDateofWorkAuthorization(Timestamp dateofWorkAuthorization) {
		this.dateofWorkAuthorization = dateofWorkAuthorization;
	}

	/**
	 * Method to get Date of Work Authorization Description.
	 * @return Date of Work Authorization Description as string.
	 */
	public String getDateofWorkAuthorizationDescription() {
		return dateofWorkAuthorizationDescription;
	}

	/**
	 * Method to set Date of Work Authorization Description.
	 * @param  Dateinspected Description Description-the DateofWork Authorization Description string.
	 */
	public void setDateofWorkAuthorizationDescription(String dateofWorkAuthorizationDescription) {
		this.dateofWorkAuthorizationDescription = dateofWorkAuthorizationDescription;
	}

	/**
	 * Method to get Date Estimate sent.
	 * @return date Estimate sent as Timestamp.
	 */
	public Timestamp getDateEstimatesent() {
		return dateEstimatesent;
	}

	/**
	 * Method to set Date Estimate sent.
	 * @param  Date Estimate sent-the Date Estimate sent timestamp.
	 */
	public void setDateEstimatesent(Timestamp dateEstimatesent) {
		this.dateEstimatesent = dateEstimatesent;
	}

	/**
	 * Method to get Date Estimatesent Description.
	 * @return Date Estimatesent Description as string.
	 */
	public String getDateEstimatesentDescription() {
		return dateEstimatesentDescription;
	}

	/**
	 * Method to set Date Estimate sent Description.
	 * @param  Date Estimate sent Description-the Date Estimate sent Description string.
	 */
	public void setDateEstimatesentDescription(String dateEstimatesentDescription) {
		this.dateEstimatesentDescription = dateEstimatesentDescription;
	}

	/**
	 * Method to get Date Estimate sent Approved.
	 * @return Date Estimate sent Approved as timestamp.
	 */
	public Timestamp getDateEstimateApproved() {
		return dateEstimateApproved;
	}

	/**
	 * Method to set Date Estimate Approved.
	 * @param  Date Estimate Approved-the Date Estimate Approved timestamp.
	 */
	public void setDateEstimateApproved(Timestamp dateEstimateApproved) {
		this.dateEstimateApproved = dateEstimateApproved;
	}

	/**
	 * Method to get Date Estimate sent Approved Description.
	 * @return  Date Estimate sent Approved Description as string.
	 */
	public String getDateEstimateApprovedDescription() {
		return dateEstimateApprovedDescription;
	}

	/**
	 * Method to set Date Estimate Approved Description.
	 * @param  Date Estimate Approved Description-the Date Estimate Approved Description string.
	 */
	public void setDateEstimateApprovedDescription(String dateEstimateApprovedDescription) {
		this.dateEstimateApprovedDescription = dateEstimateApprovedDescription;
	}

	/**
	 * Method to get Date inventoried.
	 * @return Date inventoried as timestamp.
	 */
	public Timestamp getDateinventoried() {
		return dateinventoried;
	}

	/**
	 * Method to set Date inventoried.
	 * @param  Date inventoried-the Date inventoried timestamp.
	 */
	public void setDateinventoried(Timestamp dateinventoried) {
		this.dateinventoried = dateinventoried;
	}

	/**
	 * Method to get Date inventoried Description.
	 * @return  Date inventoried Description as string.
	 */
	public String getDateinventoriedDescription() {
		return dateinventoriedDescription;
	}

	/**
	 * Method to set Date inventoried Description.
	 * @param  Date inventoried Description-the Date inventoried Description string.
	 */
	public void setDateinventoriedDescription(String dateinventoriedDescription) {
		this.dateinventoriedDescription = dateinventoriedDescription;
	}

	/**
	 * Method to get Into Production date.
	 * @return Into Production date as timestamp.
	 */
	public Timestamp getIntoProductiondate() {
		return intoProductiondate;
	}

	/**
	 * Method to set Into Productiondate.
	 * @param  Into Productiondaten-the Into Productiondate timestamp.
	 */
	public void setIntoProductiondate(Timestamp intoProductiondate) {
		this.intoProductiondate = intoProductiondate;
	}

	/**
	 * Method to get Into Production date Description.
	 * @return Into Production date Description as string.
	 */
	public String getIntoProductiondateDescription() {
		return intoProductiondateDescription;
	}

	/**
	 * Method to set Into Productiondate Description.
	 * @param  Into Productiondate Description-the Into Productiondate Description string.
	 */
	public void setIntoProductiondateDescription(String intoProductiondateDescription) {
		this.intoProductiondateDescription = intoProductiondateDescription;
	}

	/**
	 * Method to get Target Start Date.
	 * @return TargetStartDate as timestamp.
	 */
	public Timestamp getTargetStartDate() {
		return targetStartDate;
	}

	/**
	 * Method to set Target StartDate.
	 * @param  Target StartDate-the Target StartDate timedate.
	 */
	public void setTargetStartDate(Timestamp targetStartDate) {
		this.targetStartDate = targetStartDate;
	}

	/**
	 * Method to get Start Date.
	 * @return StartDate as timestamp.
	 */
	public Timestamp getStartdate() {
		return startdate;
	}

	/**
	 * Method to set Start date.
	 * @param  Start date-the Start date timedate.
	 */
	public void setStartdate(Timestamp startdate) {
		this.startdate = startdate;
	}

	/**
	 * Method to get Start Date Description.
	 * @return StartDate Description as string.
	 */
	public String getStartdateDescription() {
		return startdateDescription;
	}

	/**
	 * Method to set Start date Description.
	 * @param  Start date Description-the Start date  Description string.
	 */
	public void setStartdateDescription(String startdateDescription) {
		this.startdateDescription = startdateDescription;
	}

	/**
	 * Method to get date of majority completion.
	 * @return date of majority completion as timestamp.
	 */
	
	public Timestamp getDateofmajoritycompletion() {
		return dateofmajoritycompletion;
	}

	/**
	 * Method to set Date of majority completion.
	 * @param  Date of majority completion-the Date of majority completion timestamp.
	 */
	public void setDateofmajoritycompletion(Timestamp dateofmajoritycompletion) {
		this.dateofmajoritycompletion = dateofmajoritycompletion;
	}

	/**
	 * Method to get date of majority completion Description.
	 * @return date of majority completion Description as string.
	 */
	public String getDateofmajoritycompletionDescription() {
		return dateofmajoritycompletionDescription;
	}

	/**
	 * Method to set Date of majority completion Description.
	 * @param  Date of majority completion Description-the Date of majority completion Description string.
	 */
	public void setDateofmajoritycompletionDescription(String dateofmajoritycompletionDescription) {
		this.dateofmajoritycompletionDescription = dateofmajoritycompletionDescription;
	}

	/**
	 * Method to get Target completion date.
	 * @return Target completion date as timestamp.
	 */
	public Timestamp getTargetcompletiondate() {
		return targetcompletiondate;
	}

	/**
	 * Method to set Target completion date.
	 * @param  Target completion date-the Target completion date timestamp.
	 */

	public void setTargetcompletiondate(Timestamp targetcompletiondate) {
		this.targetcompletiondate = targetcompletiondate;
	}

	/**
	 * Method to get Target completion date Description.
	 * @return Target completion date Description as string.
	 */
	public String getTargetcompletiondateDescription() {
		return targetcompletiondateDescription;
	}

	/**
	 * Method to set Target completion date Description.
	 * @param  Target completion date Description-the Target completion date Description string.
	 */
	public void setTargetcompletiondateDescription(String targetcompletiondateDescription) {
		this.targetcompletiondateDescription = targetcompletiondateDescription;
	}

	/**
	 * Method to get Date Of cOs.
	 * @return Date Of cOs as timestamp.
	 */
	public Timestamp getDateOfcOs() {
		return dateOfcOs;
	}

	/**
	 * Method to set DateOfcOs.
	 * @param  DateOfcOs-the DateOfcOs timestamp.
	 */
	public void setDateOfcOs(Timestamp dateOfcOs) {
		this.dateOfcOs = dateOfcOs;
	}

	/**
	 * Method to get Date Of cOs Description.
	 * @return Date Of cOs Description as string.
	 */
	public String getDateOfcOsDescription() {
		return dateOfcOsDescription;
	}

	/**
	 * Method to set DateOfcOs Description.
	 * @param  DateOfcOs Description-the DateOfcOs  Description string.
	 */
	public void setDateOfcOsDescription(String dateOfcOsDescription) {
		this.dateOfcOsDescription = dateOfcOsDescription;
	}

	/**
	 * Method to get Date invoiced.
	 * @return Date invoiced as timestamp.
	 */
	public Timestamp getDateinvoiced() {
		return dateinvoiced;
	}

	/**
	 * Method to set Date invoiced.
	 * @param  Date invoiced-the Date invoiced timestamp.
	 */
	public void setDateinvoiced(Timestamp dateinvoiced) {
		this.dateinvoiced = dateinvoiced;
	}

	/**
	 * Method to get Date invoiced Description.
	 * @return Date invoiced Description as string.
	 */
	public String getDateinvoicedDescription() {
		return dateinvoicedDescription;
	}

	/**
	 * Method to set Date invoiced Description.
	 * @param  Date invoiced Description-the Date invoiced Description string.
	 */
	public void setDateinvoicedDescription(String dateinvoicedDescription) {
		this.dateinvoicedDescription = dateinvoicedDescription;
	}

	/**
	 * Method to get Final Invoice Date.
	 * @return Final Invoice Date as timestamp.
	 */
	public Timestamp getFinalInvoiceDate() {
		return finalInvoiceDate;
	}

	/**
	 * Method to set Final Invoice Date.
	 * @param  Final Invoice Date-the Final Invoice Date timestamp.
	 */
	public void setFinalInvoiceDate(Timestamp finalInvoiceDate) {
		this.finalInvoiceDate = finalInvoiceDate;
	}

	/**
	 * Method to get DatePai.
	 * @return DatePai as timestamp.
	 */
	public Timestamp getDatePaid() {
		return datePaid;
	}

	/**
	 * Method to set Date Paid.
	 * @param  Date Paid-the Date Paid timestamp.
	 */
	public void setDatePaid(Timestamp datePaid) {
		this.datePaid = datePaid;
	}

	/**
	 * Method to get Date Paid Description.
	 * @return Date Paid Description as string.
	 */
	public String getDatePaidDescription() {
		return datePaidDescription;
	}

	/**
	 * Method to set Date Paid Description.
	 * @param  Date Paid Description-the Date Paid Description string.
	 */
	public void setDatePaidDescription(String datePaidDescription) {
		this.datePaidDescription = datePaidDescription;
	}

	/**
	 * Method to get Closed date.
	 * @return Closed date as timestamp
	 */
	public Timestamp getCloseddate() {
		return closeddate;
	}

	/**
	 * Method to set Closed date.
	 * @param  Closed date-the Closed date timestamp.
	 */
	public void setCloseddate(Timestamp closeddate) {
		this.closeddate = closeddate;
	}

	/**
	 * Method to get Closed date Description.
	 * @return Closed date Description as string
	 */
	public String getCloseddateDescription() {
		return closeddateDescription;
	}
	/**
	 * Method to set Closed date Description.
	 * @param  Closed date Description-the Closed date Description string.
	 */
	public void setCloseddateDescription(String closeddateDescription) {
		this.closeddateDescription = closeddateDescription;
	}

	/**
	 * Method to get Source Id.
	 * @return Source Id as integer.
	 */
	public int getSourceId() {
		return sourceId;
	}

	/**
	 * Method to set SourceId.
	 * @param  source Id-the source Id integer.
	 */
	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * Method to get Source Category.
	 * @return Source Category as string.
	 */
	public String getSourceCategory() {
		return sourceCategory;
	}

	/**
	 * Method to set Source Category.
	 * @param  Source Category-the Source Category string.
	 */
	public void setSourceCategory(String sourceCategory) {
		this.sourceCategory = sourceCategory;
	}

	/**
	 * Method to get Entered By.
	 * @return EnteredBy as integer.
	 */
	public Integer getEnteredBy() {
		return enteredBy;
	}

	/**
	 * Method to set Entered By.
	 * @param  Entered By-the Entered By integer.
	 */

	public void setEnteredBy(Integer enteredBy) {
		this.enteredBy = enteredBy;
	}

	/**
	 * Method to get Entered Datetime.
	 * @return Entered Datetime as Timestamp.
	 */
	public Timestamp getEnteredDatetime() {
		return enteredDatetime;
	}

	/**
	 * Method to set Entered Datetime.
	 * @param  Entered datetime-the Entered datetime timestamp.
	 */
	public void setEnteredDatetime(Timestamp enteredDatetime) {
		this.enteredDatetime = enteredDatetime;
	}

	/**
	 * Method to get UpdatedBy.
	 * @return UpdatedBy as integer.
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set Updated By.
	 * @param  Updated By-the Updated By integer.
	 */	
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get Updated Date time.
	 * @return Updated Datetime as Timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set Updated Datetime.
	 * @param  Updated Datetime-the Updated Datetime timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get Source.
	 * @return Source as string.
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Method to set Source.
	 * @param  Source-the Source string.
	 */
	public void setSource(String source) {
		this.source = source;
	}

//	public Timestamp getWarrantyStartDate() {
//		return warrantyStartDate;
//	}
//
//	public void setWarrantyStartDate(Timestamp warrantyStartDate) {
//		this.warrantyStartDate = warrantyStartDate;
//	}
//
//	public Timestamp getWarrantyEndDate() {
//		return warrantyEndDate;
//	}
//
//	public void setWarrantyEndDate(Timestamp warrantyEndDate) {
//		this.warrantyEndDate = warrantyEndDate;
//	}

}