/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Class for defining Rabbit MQ Properties.
 */
@Configuration
@ConfigurationProperties(prefix = "rabbitmq")
public class RabbitMQProperties {
	private String queueName;
	private String exchangeName;
	private String routingKey;
	private String encirclePropertyClaimMediaQueue;
	private String encircleWebhookExchange;
	private String encirclePropertyClaimMediaRoutingKey;

	/**
	 * Method to get Encircle Property Claim Media Queue.
	 * @return-Encircle Property Claim Media Queue as String.
	 */
	public String getEncirclePropertyClaimMediaQueue() {
		return encirclePropertyClaimMediaQueue;
	}

	/**
	 * Method to set Encircle Property Claim Media Queue.
	 * @param encirclePropertyClaimMediaQueue-the Encircle Property Claim Media Queue String.
	 */
	public void setEncirclePropertyClaimMediaQueue(String encirclePropertyClaimMediaQueue) {
		this.encirclePropertyClaimMediaQueue = encirclePropertyClaimMediaQueue;
	}

	/**
	 * Method to get Encircle webhook exchange.
	 * @return-encircle webhook exchange as String.
	 */
	public String getEncircleWebhookExchange() {
		return encircleWebhookExchange;
	}

	/**
	 * Method to set Encircle webhook Exchange.
	 * @param encircleWebhookExchange-the encircle webhook exchange String.
	 */
	public void setEncircleWebhookExchange(String encircleWebhookExchange) {
		this.encircleWebhookExchange = encircleWebhookExchange;
	}

	/**
	 * Method to get Encicle property claim media routing key.
	 * @return-Encicle property claim media routing key as String.
	 */
	public String getEncirclePropertyClaimMediaRoutingKey() {
		return encirclePropertyClaimMediaRoutingKey;
	}

	/**
	 * Method to set Encicle property claim media routing key.
	 * @param encirclePropertyClaimMediaRoutingKey-the Encircle property claim media routing key String.
	 */
	public void setEncirclePropertyClaimMediaRoutingKey(String encirclePropertyClaimMediaRoutingKey) {
		this.encirclePropertyClaimMediaRoutingKey = encirclePropertyClaimMediaRoutingKey;
	}

	/**
	 * Method to get Queue name.
	 * @return-queue name as String.
	 */
	public String getQueueName() {
		return queueName;
	}

	/**
	 * Method to set Queue name.
	 * @param queueName-the queue name String.
	 */
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	/**
	 * Method to get exchange name.
	 * @return-exchange name as String.
	 */
	public String getExchangeName() {
		return exchangeName;
	}

	/**
	 * Method to set exchange name.
	 * @param exchangeName-the exchange name String.
	 */
	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}

	/**
	 * Method to get Routing key.
	 * @return-routing key as String.
	 */
	public String getRoutingKey() {
		return routingKey;
	}

	/**
	 * Method to set routing key.
	 * @param routingKey-the routing key String.
	 */
	public void setRoutingKey(String routingKey) {
		this.routingKey = routingKey;
	}
}