/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * The persistent class for the User_User_Authentication database table.
 */
@Entity
@Table(name = "user_user_authentication")
//@NamedQuery(name="UserAuthentication.findAll", query="SELECT u FROM UserAuthentication u")
public class UserAuthentication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_authentication_id")
	private int userAuthenticationId;

	@Column(name = "last_signin_date")
	private String lastSigninDate;

	private String password;

	@Column(name = "restoration_company_id")
	private int restorationCompanyId;

	@Column(name = "signin_attempts")
	private Integer signinAttempts;

	@Column(name = "user_name")
	private String userName;

	// bi-directional many-to-one association to UserUserDetails
	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserDetails userDetail;

	@Column(name = "updated_by")
	private Integer updatedBy;

	@Column(name = "updated_datetime")
	private Timestamp updatedDatetime;

	@Column(name = "old_password")
	private String oldPassword;

	public UserAuthentication() {
	}

	/**
	 * Method to get user authentication ID.
	 * @return-user authentication ID as Integer.
	 */
	public int getUserAuthenticationId() {
		return this.userAuthenticationId;
	}

	/**
	 * Method to set user authentication ID.
	 * @param userAuthenticationId-the user authentication ID Integer.
	 */
	public void setUserAuthenticationId(int userAuthenticationId) {
		this.userAuthenticationId = userAuthenticationId;
	}

	/**
	 * Method to get last sign in date.
	 * @return-last sign in date as String.
	 */
	public String getLastSigninDate() {
		return this.lastSigninDate;
	}

	/**
	 * Method to set last sign in date.
	 * @param lastSigninDate-the last sign in date String.
	 */
	public void setLastSigninDate(String lastSigninDate) {
		this.lastSigninDate = lastSigninDate;
	}

	/**
	 * Method to get password.
	 * @return-password as String.
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Method to set password.
	 * @param password-the password String.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Method to get Restoration Company ID.
	 * @return-Restoration Company ID as Integer.
	 */
	public int getRestorationCompanyId() {
		return this.restorationCompanyId;
	}

	/**
	 * Method to set Restoration Company ID.
	 * @param restorationCompanyId-the Restoration Company ID Integer. 
	 */
	public void setRestorationCompanyId(int restorationCompanyId) {
		this.restorationCompanyId = restorationCompanyId;
	}

	/**
	 * Method to get sign in attempts. 
	 * @return-sign in attempts as Integer.
	 */
	public Integer getSigninAttempts() {
		return this.signinAttempts;
	}

	/**
	 * Method to set sign in attempts.  
	 * @param signinAttempts-the sign in attempts Integer.
	 */
	public void setSigninAttempts(Integer signinAttempts) {
		this.signinAttempts = signinAttempts;
	}

	/**
	 * Method to get user name.
	 * @return-user name as String.
	 */
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Metod to set user name.
	 * @param userName-the user name String.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Method to get user details.
	 * @return-user details as User details.
	 */
	public UserDetails getUserDetail() {
		return userDetail;
	}

	/**
	 * Mwthod to set user details.
	 * @param userDetail-the user details.
	 */
	public void setUserDetail(UserDetails userDetail) {
		this.userDetail = userDetail;
	}

	/**
	 * Method to get updated by.
	 * @return-updated by as Integer.
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set updated by.
	 * @param updatedBy-the updated by Integer.
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get updated date and time.
	 * @return-updated date and time as Timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set updated date and time.
	 * @param updatedDatetime-the updated date and time Timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get old password.
	 * @return-old password as String.
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * Method to set old password.
	 * @param oldPassword-the old password String.
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
}
