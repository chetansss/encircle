/**

* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.util;

import java.util.HashMap;
import java.util.Map;

import com.wissen.xcelerate.encircle.model.JobKpiTargets;

/**
 * Class for Job util.
 */
public class JobUtil {
	private Map<String, String> jobSuffix = new HashMap<>();
	private Map<String, String> docCategory = new HashMap<>();
	private Map<String, String> taskAction = new HashMap<>();
	private Map<String, String> kpiNames = new HashMap<>();
	private Map<String, String> kpiDesc = new HashMap<>();
	private Map<String, String> kpiDashDesc = new HashMap<>();

	/**
	 * Method to get Job suffix.
	 * @return-job suffix as Map.
	 */
	public Map<String, String> getJobSuffix() {
		jobSuffix.put("Water", "-W");
		jobSuffix.put("Reconstruction", "-R");
		jobSuffix.put("Content", "-C");
		jobSuffix.put("Mold", "-M");
		jobSuffix.put("Asbestos", "-A");
		jobSuffix.put("Board Up", "-B");
		jobSuffix.put("Consulting", "-CST");
		jobSuffix.put("Bio", "-BIO");
		jobSuffix.put("Structure Cleaning", "-S");
		jobSuffix.put("Other", "-O");
		jobSuffix.put("Roofing", "-RF");
		return jobSuffix;
	}
	
	/**
	 * Method to get Document category.
	 * @return-document category as Map.
	 */
	public Map<String, String> getDocumentCategory() {
		docCategory.put("Upload Estimate", "Estimate");
		docCategory.put("Upload Invoice", "Invoice");
		docCategory.put("Upload WO", "Work Authorization");
		docCategory.put("Upload of COC", "COC");
		return docCategory;
	}

	/**
	 * Method to get task action.
	 * @return-task action as Map.
	 */
	public Map<String, String> getTaskAction() {
		taskAction.put("Estimate", "Upload Estimate");
		taskAction.put("Invoice", "Upload Invoice");
		taskAction.put("Work Authorization", "Upload WO");
		taskAction.put("COC", "Upload of COC");
		return taskAction;
	}

	/**
	 * Method to get KPI names.
	 * @return-KPI names as Map.
	 */
	public Map<String, String> getKpiNames() {
		kpiNames.put("Cycle Time (Received to Final Invoiced)", "Cycle Time");
		kpiNames.put("WIP Time (Start to COC)", "WIP Time");
		kpiNames.put("Total Final Invoice Time (COC to Final Invoice)", "Total Final Invoice Time");
		kpiNames.put("Days to Start (WA date to Start Date)", "Days to Start");
		kpiNames.put("Estimate Bid Time (Inspected to Estimate)", "Estimate Bid Time");
		kpiNames.put("Final Estimate Upload Time (COC to Estimate)", "Final Estimate Upload Time");
		kpiNames.put("Onsite Arrival Time", "Onsite Arrival Time");
		kpiNames.put("Target Completion Accuracy (COC Date versus Target Complete)", "Target Completion Accuracy");
		kpiNames.put("Contact Customer Time (Date Created to Contact Customer)", "Contact Customer Time");
		kpiNames.put("Days to Sign (Inspected to Work Authorization)", "Days to Sign");
		kpiNames.put("Final Invoice Delay Time (Estimate to Invoice)", "Final Invoice Delay Time");
		kpiNames.put("Gross Profit %", "Gross Profit %");
		kpiNames.put("Labor Efficiency Ratio", "Labor Efficiency Ratio");
		return kpiNames;
	}

	/**
	 * Method to get kpi target value for job.
	 * @param target-the target Job KPI targets.
	 * @param jobType-the Job type String.
	 * @return-target value as Float.
	 */
	public Float getKpiTargetValue(JobKpiTargets target, String jobType) {
		Float targetValue = null;
		if (jobType.equalsIgnoreCase("Water")) {
			targetValue = target.getWater();
		} else if (jobType.equalsIgnoreCase("Reconstruction")) {
			targetValue = target.getRecon();
		} else if (jobType.equalsIgnoreCase("Content")) {
			targetValue = target.getContent();
		} else if (jobType.equalsIgnoreCase("Mold")) {
			targetValue = target.getMold();
		} else if (jobType.equalsIgnoreCase("Asbestos")) {
			targetValue = target.getAsbestos();
		} else if (jobType.equalsIgnoreCase("Board Up")) {
			targetValue = target.getBoardUp();
		} else if (jobType.equalsIgnoreCase("Consulting")) {
			targetValue = target.getConsulting();
		} else if (jobType.equalsIgnoreCase("Board Up")) {
			targetValue = target.getBoardUp();
		} else if (jobType.equalsIgnoreCase("Bio")) {
			targetValue = target.getBio();
		} else if (jobType.equalsIgnoreCase("Structure Cleaning")) {
			targetValue = target.getStructureCleaning();
		} else if (jobType.equalsIgnoreCase("Other")) {
			targetValue = target.getOthers();
		} else if (jobType.equalsIgnoreCase("Roofing")) {
			targetValue = target.getRoofing();
		}
		return targetValue;
	}

	/**
	 * Method to get Mouse hover KPI description.
	 * @return-KPI descrption as Map.
	 */
	public Map<String, String> getKpiDesc() {
		kpiDesc.put("Cycle Time (Received to Final Invoiced)", "Date Received to Date of Final Invoiced");
		kpiDesc.put("WIP Time (Start to COC)", "Date Received to Date of COC");
		kpiDesc.put("Total Final Invoice Time (COC to Final Invoice)", "Date of COC to Date of Final Invoice");
		kpiDesc.put("Days to Start (WA date to Start Date)", "Date of Work Authorization to Date Started");
		kpiDesc.put("Estimate Bid Time (Inspected to Estimate)", "Date Arrived Onsite to Date Estimate Uploaded");
		kpiDesc.put("Final Estimate Upload Time (COC to Estimate)", "Date of COC to Date Estimate Uploaded");
		kpiDesc.put("Onsite Arrival Time", "Date Received to Date Arrived Onsite");
		kpiDesc.put("Target Completion Accuracy (COC Date versus Target Complete)",
				"Date of COC versus Date Target Completion");
		kpiDesc.put("Contact Customer Time (Date Created to Contact Customer)", "Date Received to Customer Contacted");
		kpiDesc.put("Days to Sign (Inspected to Work Authorization)",
				"Date Arrived Onsite to Date of Work Authorization");
		kpiDesc.put("Final Invoice Delay Time (Estimate to Invoice)", "Date Estimate Uploaded to Date Final Invoiced");
		kpiDesc.put("Gross Profit %", "Gross Margin/Total Estimates");
		kpiDesc.put("Labor Efficiency Ratio", "Total Estimates/Total Labor Cost");
		return kpiDesc;
	}
	
	/**
	 * Method to get KPI dash description.
	 * @return-KPI dash description as Map.
	 */
	public Map<String, String> getKpiDashDesc() {
		kpiDashDesc.put("Cycle Time", "Date Received to Date of Final Invoiced");
		kpiDashDesc.put("WIP Time", "Date Received to Date of COC");
		kpiDashDesc.put("Total Final Invoice Time", "Date of COC to Date of Final Invoice");
		kpiDashDesc.put("Days to Start", "Date of Work Authorization to Date Started");
		kpiDashDesc.put("Estimate Bid Time", "Date Arrived Onsite to Date Estimate Uploaded");
		kpiDashDesc.put("Final Estimate Upload Time", "Date of COC to Date Estimate Uploaded");
		kpiDashDesc.put("Onsite Arrival Time", "Date Received to Date Arrived Onsite");
		kpiDashDesc.put("Target Completion Accuracy", "Date of COC versus Date Target Completion");
		kpiDashDesc.put("Contact Customer Time", "Date Received to Customer Contacted");
		kpiDashDesc.put("Days to Sign", "Date Arrived Onsite to Date of Work Authorization");
		kpiDashDesc.put("Final Invoice Delay Time", "Date Estimate Uploaded to Date Final Invoiced");
		kpiDashDesc.put("Gross Profit %", "Gross Margin/Total Estimates");
		kpiDashDesc.put("Labor Efficiency Ratio", "Total Estimates/Total Labor Cost");
		return kpiDashDesc;
	}
}