/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

/**
 * The persistent class for the mast_num_details database table.
 */
@Entity
@Table(name = "mast_num_details")
//@NamedQuery(name="MastNumDetails.findAll", query="SELECT m FROM MastNumDetails m")
public class MastNumDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mast_num_id")
	private int mastNumId;

	@Column(name = "actual_mast_num_id")
	private Integer actualMastNumId;

	@Column(name = "cat_code")
	private String catCode;

	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "created_datetime")
	private Timestamp createdDatetime;

	private String company;

	@Column(name = "customer_id")
	private Long customerId;

	@Column(name = "loss_address")
	private String lossAddress;

	@Column(name = "loss_address_city")
	private String lossAddressCity;

	@Column(name = "loss_address_state")
	private String lossAddressState;

	@Column(name = "loss_address_zip")
	private String lossAddressZip;

	@Column(name = "billing_address")
	private String billingAddress;

	@Column(name = "billing_address_city")
	private String billingAddressCity;

	@Column(name = "billing_address_state")
	private String billingAddressState;

	@Column(name = "billing_address_zip")
	private String billingAddressZip;

	@Column(name = "is_billing_address_same")
	private String isBillingAddressSame;

	@Column(name = "tenant_name")
	private String tenantName;

	@Column(name = "tenant_number")
	private String tenantNumber;

	@Column(name = "damage_type")
	private String damageType;

	@Column(name = "duplicate_comments")
	private String duplicateComments;

	@Column(name = "duplicate_entered_by")
	private Integer duplicateEnteredBy;

	@Column(name = "duplicate_entered_datetime")
	private Timestamp duplicateEnteredDatetime;

	@Column(name = "is_duplicate")
	private String isDuplicate;

	@Column(name = "is_insured")
	private String isInsured;

	@Column(name = "mast_num_code")
	private String mastNumCode;

	@Column(name = "mast_num_desc")
	private String mastNumDesc;

	@Column(name = "mast_num_hash")
	private String mastNumHash;

	@Column(name = "property_type")
	private String propertyType;

	@Column(name = "rest_comp_location_id")
	private Integer restCompLocationId;

	@Column(name = "restoration_type")
	private String restorationType;

	@Column(name = "source_id")
	private Integer sourceId;

	@Column(name = "source_type")
	private String sourceType;

	@Column(name = "program_type")
	private Integer programType;

	@Column(name = "cause_of_loss")
	private Integer causeOfLoss;

	@Column(name = "year_built")
	private Integer yearBuilt;

	private String status;

	@Column(name = "referred_source")
	private String referredSource;

	@Column(name = "updated_by")
	private int updatedBy;

	@Column(name = "updated_datetime")
	private Timestamp updatedDatetime;

	@Column(name = "responsible_party")
	private String responsibleParty;

	@Column(name = "master_name")
	private String masterName;

	private String source;

	@Column(name = "loss_date")
	private Date lossDate;

	@Column(name = "master_job_received_date")
	private Date masterJobReceivedDate;

	private Long tpa;

	@Column(name = "tpa_name")
	private String tpaName;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	private String email;

	private String mobile;

	@Column(name = "work_phone")
	private String workPhone;

	@Column(name = "company_id")
	private Integer companyId;

	private String phone1;

	@Column(name = "customer_type")
	private String customerType;

	@Column(name = "customer_company")
	private Integer customerCompany;

	@Column(name = "customer_company_name")
	private String customerCompanyName;

	private Integer salesRepresentative;

	@Column(name = "unit_number")
	private String unitNumber;

	@Column(name = "loss_address_country")
	private String lossAddressCountry;

	@Column(name = "loss_address_lat")
	private String lossAddressLat;

	@Column(name = "loss_address_lan")
	private String lossAddressLan;

	@Column(name = "loss_address_formatted")
	private String lossAddressFormatted;

	@Column(name = "billing_address_country")
	private String billingAddressCountry;

	@Column(name = "billing_address_lat")
	private String billingAddressLat;

	@Column(name = "billing_address_lan")
	private String billingAddressLan;

	@Column(name = "billing_address_formatted")
	private String billingAddressFormatted;

	//Default constructor.
	public MastNumDetails() {
	}

	//Parameterized constructor.
	public MastNumDetails(int mastNumId, String mastNumHash, String mastNumCode, String firstName, String lastName,
			String company, String lossAddressCity, String lossAddressState, String lossAddressZip,
			Date createdDatetime, Date lossDate, String status, String lossAddress, String phone1) {
		this.mastNumId = mastNumId;
		this.mastNumHash = mastNumHash;
		this.mastNumCode = mastNumCode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.company = company;
		this.lossAddressCity = lossAddressCity;
		this.lossAddressState = lossAddressState;
		this.lossAddressZip = lossAddressZip;
		this.createdDatetime = (Timestamp) createdDatetime;
		this.lossDate = lossDate;
		this.status = status;
		this.lossAddress = lossAddress;
		this.phone1 = phone1;
	}

	//Parameterized constructor.
	public MastNumDetails(int mastNumId, String mastNumHash, String mastNumCode, String firstName, String lastName,
			String company, String lossAddressCity, String lossAddressState, String lossAddressZip,
			Date createdDatetime, Date lossDate, String status, String lossAddress, String phone1, String mobile,
			String workPhone) {
		this.mastNumId = mastNumId;
		this.mastNumHash = mastNumHash;
		this.mastNumCode = mastNumCode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.company = company;
		this.lossAddressCity = lossAddressCity;
		this.lossAddressState = lossAddressState;
		this.lossAddressZip = lossAddressZip;
		this.createdDatetime = (Timestamp) createdDatetime;
		this.lossDate = lossDate;
		this.status = status;
		this.lossAddress = lossAddress;
		this.phone1 = phone1;
		this.mobile = mobile;
		this.workPhone = workPhone;
	}

	//Parameterzied constructor.
	public MastNumDetails(int mastNumId, String mastNumHash, String mastNumCode, String firstName, String lastName,
			String company, String lossAddressCity, String lossAddressState, String lossAddressZip,
			Date createdDatetime, Date lossDate, String status, String lossAddress, String phone1, String mobile,
			String workPhone, String customerType, Integer customerCompany, String customerCompanyName,
			String masterName) {
		this.mastNumId = mastNumId;
		this.mastNumHash = mastNumHash;
		this.mastNumCode = mastNumCode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.company = company;
		this.lossAddressCity = lossAddressCity;
		this.lossAddressState = lossAddressState;
		this.lossAddressZip = lossAddressZip;
		this.createdDatetime = (Timestamp) createdDatetime;
		this.lossDate = lossDate;
		this.status = status;
		this.lossAddress = lossAddress;
		this.phone1 = phone1;
		this.mobile = mobile;
		this.workPhone = workPhone;
		this.customerType = customerType;
		this.customerCompany = customerCompany;
		this.customerCompanyName = customerCompanyName;
		this.masterName = masterName;
	}

	//Parameterized constructor.
	public MastNumDetails(int mastNumId, String mastNumHash, String mastNumCode, String firstName, String lastName,
			String company, String lossAddressCity, String lossAddressState, String lossAddressZip,
			Date createdDatetime, Date lossDate, String status, String lossAddress, String phone1, String mobile,
			String workPhone, String customerType, Integer customerCompany, String customerCompanyName,
			String masterName, String email) {
		this.mastNumId = mastNumId;
		this.mastNumHash = mastNumHash;
		this.mastNumCode = mastNumCode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.company = company;
		this.lossAddressCity = lossAddressCity;
		this.lossAddressState = lossAddressState;
		this.lossAddressZip = lossAddressZip;
		this.createdDatetime = (Timestamp) createdDatetime;
		this.lossDate = lossDate;
		this.status = status;
		this.lossAddress = lossAddress;
		this.phone1 = phone1;
		this.mobile = mobile;
		this.workPhone = workPhone;
		this.customerType = customerType;
		this.customerCompany = customerCompany;
		this.customerCompanyName = customerCompanyName;
		this.masterName = masterName;
		this.email = email;
	}

	/**
	 * Method to get master number ID.
	 * @return-master number ID as Integer.
	 */
	public int getMastNumId() {
		return this.mastNumId;
	}

	/**
	 * Method to set Master number ID.
	 * @param mastNumId-the master number ID Integer.
	 */
	public void setMastNumId(int mastNumId) {
		this.mastNumId = mastNumId;
	}

	/**
	 * Method to get actual  master number ID.
	 * @return-actual master number ID as Integer.
	 */
	public Integer getActualMastNumId() {
		return this.actualMastNumId;
	}

	/**
	 * Method to set actual  master number ID. 
	 * @param actualMastNumId-the actual master number ID Integer.
	 */
	public void setActualMastNumId(Integer actualMastNumId) {
		this.actualMastNumId = actualMastNumId;
	}

	/**
	 * Method to get Cat code. 
	 * @return-cat code as String.
	 */
	public String getCatCode() {
		return this.catCode;
	}

	/**
	 * Method to set Cat code.  
	 * @param catCode-the cat code String.
	 */
	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	/**
	 * Method to get created by. 
	 * @return-created by as Integer.
	 */
	public int getCreatedBy() {
		return this.createdBy;
	}

	/**
	 * Method to set created by.  
	 * @param createdBy-the created by Integer. 
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Method to get created date and time.
	 * @return-created date and time as Timestamp.
	 */
	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	/**
	 * Method to set created date and time. 
	 * @param createdDatetime-the created date and time Timestamp.
	 */
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	/**
	 * Method to get COmpany.
	 * @return-company as String.
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * Method to set COmpany. 
	 * @param company-the company String.
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * Method to get customer ID.
	 * @return-customer ID as Long.
	 */
	public Long getCustomerId() {
		return this.customerId;
	}

	/**
	 * Method to set customer ID.
	 * @param customerId-the customer ID Long.
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	
	/**
	 * Method to get Loss address.
	 * @return-loss address as String.
	 */
	public String getLossAddress() {
		return lossAddress;
	}

	/**
	 * Method to set Loss address. 
	 * @param lossAddress-the loss address String.
	 */
	public void setLossAddress(String lossAddress) {
		this.lossAddress = lossAddress;
	}

	/**
	 * Method to get Loss address city.
	 * @return-loss address city as String.
	 */
	public String getLossAddressCity() {
		return lossAddressCity;
	}

	/**
	 * Method to set Loss address city. 
	 * @param lossAddressCity-the loss address city String.
	 */
	public void setLossAddressCity(String lossAddressCity) {
		this.lossAddressCity = lossAddressCity;
	}

	/**
	 * Method to get Loss address state.
	 * @return-loss address state as String.
	 */
	public String getLossAddressState() {
		return lossAddressState;
	}

	/**
	 * Method to set Loss address state. 
	 * @param lossAddressState-the loss address state String.
	 */
	public void setLossAddressState(String lossAddressState) {
		this.lossAddressState = lossAddressState;
	}

	/**
	 * Method to get Loss address zip. 
	 * @return-loss address zip as String.
	 */
	public String getLossAddressZip() {
		return lossAddressZip;
	}

	/**
	 * Method to set Loss address zip.  
	 * @param lossAddressZip-the the loss address Zip String.
	 */
	public void setLossAddressZip(String lossAddressZip) {
		this.lossAddressZip = lossAddressZip;
	}

	/**
	 * Method to get Billing address.
	 * @return-billing address as String.
	 */
	public String getBillingAddress() {
		return billingAddress;
	}

	/**
	 * Method to set Billing address. 
	 * @param billingAddress-the billing address String.
	 */
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	/**
	 * Method to get Billing address city.  
	 * @return-billing address city as String.
	 */
	public String getBillingAddressCity() {
		return billingAddressCity;
	}

	/**
	 * Method to set Billing address city. 
	 * @param billingAddressCity-the billing address city String.
	 */
	public void setBillingAddressCity(String billingAddressCity) {
		this.billingAddressCity = billingAddressCity;
	}

	/**
	 * Method to get Billing address state. 
	 * @return-billing address state as String.
	 */
	public String getBillingAddressState() {
		return billingAddressState;
	}

	/**
	 * Method to set Billing address state. 
	 * @param billingAddressState-the Billing address state String.
	 */
	public void setBillingAddressState(String billingAddressState) {
		this.billingAddressState = billingAddressState;
	}

	/**
	 * Method to get Billing address zip.
	 * @return-billing address zip as String.
	 */
	public String getBillingAddressZip() {
		return billingAddressZip;
	}

	/**
	 *  Method to set Billing address zip.
	 * @param billingAddressZip-the billing address zip String.
	 */
	public void setBillingAddressZip(String billingAddressZip) {
		this.billingAddressZip = billingAddressZip;
	}

	/**
	 * Method to get Billing address same. 
	 * @return-billing address same as String.
	 */
	public String getIsBillingAddressSame() {
		return isBillingAddressSame;
	}

	/**
	 * Method to set Is Billing Address Same.
	 * @param isBillingAddressSame-the Billing address same String. 
	 */
	public void setIsBillingAddressSame(String isBillingAddressSame) {
		this.isBillingAddressSame = isBillingAddressSame;
	}

	/**
	 * Method to get tenant name.
	 * @return-tenant name as String.
	 */
	public String getTenantName() {
		return tenantName;
	}

	/**
	 * Method to set tenant name.
	 * @param tenantName-the tenant name String.
	 */
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	/**
	 * Method to get tenant number. 
	 * @return-tenant number as String.
	 */
	public String getTenantNumber() {
		return tenantNumber;
	}

	/**
	 * Method to set tenant number.  
	 * @param tenantNumber-the tenant number String.
	 */
	public void setTenantNumber(String tenantNumber) {
		this.tenantNumber = tenantNumber;
	}

	/**
	 * Method to get damage type. 
	 * @return-damage type as String.
	 */
	public String getDamageType() {
		return this.damageType;
	}

	/**
	 * Method to set damage type. 
	 * @param damageType-the damage type String.
	 */
	public void setDamageType(String damageType) {
		this.damageType = damageType;
	}

	/**
	 * Method to get duplicate comments. 
	 * @return-duplicate comments as String.
	 */
	public String getDuplicateComments() {
		return this.duplicateComments;
	}

	/**
	 * Method to set duplicate comments.  
	 * @param duplicateComments-the duplicate comments String.
	 */
	public void setDuplicateComments(String duplicateComments) {
		this.duplicateComments = duplicateComments;
	}

	/**
	 * Method to get duplicate entered by. 
	 * @return-duplicate entered by as Integer.
	 */
	public Integer getDuplicateEnteredBy() {
		return this.duplicateEnteredBy;
	}

	/**
	 * Method to set duplicate entered by.  
	 * @param duplicateEnteredBy-the duplicate entered by Integer.  
	 */
	public void setDuplicateEnteredBy(Integer duplicateEnteredBy) {
		this.duplicateEnteredBy = duplicateEnteredBy;
	}

	/**
	 * Method to get duplicate entered date and time. 
	 * @return-duplicate entered date and time as Timestamp. 
	 */
	public Timestamp getDuplicateEnteredDatetime() {
		return this.duplicateEnteredDatetime;
	}

	/**
	 * Method to set duplicate entered date and time.  
	 * @param duplicateEnteredDatetime-the duplicate entered date and time Timestamp. 
	 */
	public void setDuplicateEnteredDatetime(Timestamp duplicateEnteredDatetime) {
		this.duplicateEnteredDatetime = duplicateEnteredDatetime;
	}

	/**
	 * Method to get Is duplicate.  
	 * @return-is duplicate as String.
	 */
	public String getIsDuplicate() {
		return this.isDuplicate;
	}

	/**
	 * Method to set Is duplicate.   
	 * @param isDuplicate-the is duplicate String.
	 */
	public void setIsDuplicate(String isDuplicate) {
		this.isDuplicate = isDuplicate;
	}

	/**
	 * Method to get Is insured. 
	 * @return-is insured as String.
	 */
	public String getIsInsured() {
		return this.isInsured;
	}

	/**
	 * Method to set Is insured.  
	 * @param isInsured-the is insured String.
	 */
	public void setIsInsured(String isInsured) {
		this.isInsured = isInsured;
	}

	/**
	 * Method to get master number code. 
	 * @return-master number code as String.
	 */
	public String getMastNumCode() {
		return this.mastNumCode;
	}

	/**
	 * Method to set master number code.  
	 * @param mastNumCode-the master number code String.
	 */
	public void setMastNumCode(String mastNumCode) {
		this.mastNumCode = mastNumCode;
	}

	/**
	 * Method to get master number description.
	 * @return-master number description as String.
	 */
	public String getMastNumDesc() {
		return this.mastNumDesc;
	}

	/**
	 * Method to set master number description.
	 * @param mastNumDesc-the master number description String.
	 */
	public void setMastNumDesc(String mastNumDesc) {
		this.mastNumDesc = mastNumDesc;
	}

	/**
	 * Method to get master number hash. 
	 * @return-master number as String.
	 */
	public String getMastNumHash() {
		return this.mastNumHash;
	}

	/**
	 * Method to set master number hash.  
	 * @param mastNumHash-the master number String.
	 */
	public void setMastNumHash(String mastNumHash) {
		this.mastNumHash = mastNumHash;
	}

	/**
	 * Method to get property type. 
	 * @return-property type as String.
	 */
	public String getPropertyType() {
		return this.propertyType;
	}

	/**
	 * Method to set property type.  
	 * @param propertyType-the property type String.
	 */
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	/**
	 * Method to get restoration company location ID.  
	 * @return-restoration company location ID as Integer.  
	 */
	public Integer getRestCompLocationId() {
		return this.restCompLocationId;
	}

	/**
	 * Method to set restoration company location ID.  
	 * @param restCompLocationId-the restoration company location ID Integer.  
	 */
	public void setRestCompLocationId(Integer restCompLocationId) {
		this.restCompLocationId = restCompLocationId;
	}

	/**
	 * Method to get restoration type. 
	 * @return-restoration type as String.
	 */
	public String getRestorationType() {
		return this.restorationType;
	}

	/**
	 * Method to set restoration type.
	 * @param restorationType-the restoration type String.
	 */
	public void setRestorationType(String restorationType) {
		this.restorationType = restorationType;
	}

	/**
	 * Method to get source ID. 
	 * @return-source ID as Integer.
	 */
	public Integer getSourceId() {
		return this.sourceId;
	}

	/**
	 * Method to set source ID. 
	 * @param sourceId-the source ID Integer.
	 */
	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * Method to get source type. 
	 * @return-source type as String.
	 */
	public String getSourceType() {
		return this.sourceType;
	}

	/**
	 * Method to set source type.  
	 * @param sourceType-the source type String.
	 */
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	/**
	 * Method to get Program type.
	 * @return-program type as Integer.
	 */
	public Integer getProgramType() {
		return programType;
	}

	/**
	 * Method to set Program type.
	 * @param programType-the program type Integer.
	 */
	public void setProgramType(Integer programType) {
		this.programType = programType;
	}

	/**
	 * Method to get Cause Of Loss.
	 * @return-cause of loss as Integer.
	 */
	public Integer getCauseOfLoss() {
		return causeOfLoss;
	}

	/**
	 * Method to set Cause Of Loss. 
	 * @param causeOfLoss-the cause of loss Integer.
	 */
	public void setCauseOfLoss(Integer causeOfLoss) {
		this.causeOfLoss = causeOfLoss;
	}

	/**
	 * Method to get year built.
	 * @return-year built as Integer.
	 */
	public Integer getYearBuilt() {
		return yearBuilt;
	}

	/**
	 * Method to set year built. 
	 * @param yearBuilt-the year built Integer.
	 */
	public void setYearBuilt(Integer yearBuilt) {
		this.yearBuilt = yearBuilt;
	}

	/**
	 * Method to get status. 
	 * @return-status as String.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Method to set status.  
	 * @param status-the status String.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Method to get referred source. 
	 * @return-referred source as String.
	 */
	public String getReferredSource() {
		return referredSource;
	}

	/**
	 * Method to set referred source. 
	 * @param referredSource-the referred source String.
	 */
	public void setReferredSource(String referredSource) {
		this.referredSource = referredSource;
	}

	/**
	 * Method to get updated by. 
	 * @return-updated by as Integer.
	 */
	public int getUpdatedBy() {
		return this.updatedBy;
	}

	/**
	 * Method to set updated by.  
	 * @param updatedBy-the updated by Integer.
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get updated date and time. 
	 * @return-updated date and time as TImestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	/**
	 * Method to set updated date and time. 
	 * @param updatedDatetime-the updated date and time TImestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get responsible party.
	 * @return-responsible party as String.
	 */
	public String getResponsibleParty() {
		return responsibleParty;
	}

	/**
	 * Method to set responsible party.
	 * @param responsibleParty-the responsible party String.
	 */
	public void setResponsibleParty(String responsibleParty) {
		this.responsibleParty = responsibleParty;
	}

	/**
	 * Method to get Master name. 
	 * @return-master name as String.
	 */
	public String getMasterName() {
		return masterName;
	}

	/**
	 * Method to set Master name.  
	 * @param masterName-the master name String.
	 */
	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}

	/**
	 * Method to get Source.
	 * @return-source as String.
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Method to set Source.
	 * @param source-the source String.
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Method to get Loss date. 
	 * @return-loss date as Date.
	 */
	public Date getLossDate() {
		return lossDate;
	}

	/**
	 * Method to set Loss date.  
	 * @param lossDate-the loss date Date.
	 */
	public void setLossDate(Date lossDate) {
		this.lossDate = lossDate;
	}

	/**
	 * Method to get Master Job Received Date.
	 * @return-master job received date as Date.
	 */
	public Date getMasterJobReceivedDate() {
		return masterJobReceivedDate;
	}

	/**
	 * Method to set Master Job Received Date. 
	 * @param masterJobReceivedDate-the master job received date Date.
	 */
	public void setMasterJobReceivedDate(Date masterJobReceivedDate) {
		this.masterJobReceivedDate = masterJobReceivedDate;
	}

	/**
	 * Method to get Tpa.
	 * @return-tpa as Long.
	 */
	public Long getTpa() {
		return tpa;
	}

	/**
	 * Method to set Tpa. 
	 * @param tpa-the tpa Long.
	 */
	public void setTpa(Long tpa) {
		this.tpa = tpa;
	}

	/**
	 * Method to get Tpa name. 
	 * @return-tpa name as String.
	 */
	public String getTpaName() {
		return tpaName;
	}

	/**
	 * Method to set Tpa name.  
	 * @param tpaName-the tpa name String.
	 */
	public void setTpaName(String tpaName) {
		this.tpaName = tpaName;
	}

	/**
	 * Method to get first name. 
	 * @return-first name as String.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Method to set first name.  
	 * @param firstName-the first name String.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Method to get last name. 
	 * @return-last name as String.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Method to set last name.  
	 * @param lastName-the last name String.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Method to get E-mail.
	 * @return-Email as String.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Method to set E-mail. 
	 * @param email-the email String.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Method to get Mobile.
	 * @return-mobile as String.
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Method to set Mobile.
	 * @param mobile-the mobile String.
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * Method to get Work phone. 
	 * @return-work phone as String.
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * Method to set work phone.
	 * @param workPhone-the work phone String.
	 */
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	/**
	 * Method to get Phone1.
	 * @return-phone1 as String.
	 */
	public String getPhone1() {
		return phone1;
	}

	/**
	 * Method to set Phone1.
	 * @param phonethe phone1 String.
	 */
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	/**
	 * Method to get Company ID. 
	 * @return-company ID as Integer.
	 */
	public Integer getCompanyId() {
		return companyId;
	}

	/**
	 * Method to set Company ID.  
	 * @param companyId-the company ID Integer. 
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	/**
	 * Method to get Customer type.
	 * @return-customer type as String.
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * Method to set Customer type.
	 * @param customerType-the customer type String.
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * Method to get Customer company. 
	 * @return-customer company as Integer.
	 */
	public Integer getCustomerCompany() {
		return customerCompany;
	}

	/**
	 * Method to set Customer company.  
	 * @param customerCompany-the customer company Integer.
	 */
	public void setCustomerCompany(Integer customerCompany) {
		this.customerCompany = customerCompany;
	}

	/**
	 * Method to get Customer company name. 
	 * @return-Customer company name as String. 
	 */
	public String getCustomerCompanyName() {
		return customerCompanyName;
	}

	/**
	 * Method to set Customer company name.  
	 * @param customerCompanyName-the Customer company name String. 
	 */
	public void setCustomerCompanyName(String customerCompanyName) {
		this.customerCompanyName = customerCompanyName;
	}

	/**
	 * Method to get sales representative.
	 * @return-sales representative as Integer.
	 */
	public Integer getSalesRepresentative() {
		return salesRepresentative;
	}

	/**
	 * Method to set sales representative. 
	 * @param salesRepresentative-the sales representative Integer.
	 */
	public void setSalesRepresentative(Integer salesRepresentative) {
		this.salesRepresentative = salesRepresentative;
	}

	/**
	 * Method to get Unit number.
	 * @return-unit number as String.
	 */
	public String getUnitNumber() {
		return unitNumber;
	}

	/**
	 * Method to set Unit number. 
	 * @param unitNumber-the unit number String. 
	 */
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	/**
	 * Method to get Loss address city.
	 * @return-loss address city as String.
	 */
	public String getLossAddressCountry() {
		return lossAddressCountry;
	}

	/**
	 * Method to set Loss address city. 
	 * @param lossAddressCountry-the loss address city String.
	 */
	public void setLossAddressCountry(String lossAddressCountry) {
		this.lossAddressCountry = lossAddressCountry;
	}

	/**
	 * Method to get Loss address latitude. 
	 * @return-loss address latitude as String.
	 */
	public String getLossAddressLat() {
		return lossAddressLat;
	}

	/**
	 * Method to set Loss address latitude. 
	 * @param lossAddressLat-the loss address latitude String.
	 */
	public void setLossAddressLat(String lossAddressLat) {
		this.lossAddressLat = lossAddressLat;
	}

	/**
	 * Method to get Loss address longitude. 
	 * @return-loss address longitude as String.
	 */
	public String getLossAddressLan() {
		return lossAddressLan;
	}

	/**
	 *  Method to set Loss address longitude. 
	 * @param lossAddressLan-the loss address longitude String.
	 */
	public void setLossAddressLan(String lossAddressLan) {
		this.lossAddressLan = lossAddressLan;
	}

	/**
	 * Method to get Loss address formatted.
	 * @return-loss address formatted as String.
	 */
	public String getLossAddressFormatted() {
		return lossAddressFormatted;
	}

	/**
	 * Method to set Loss address formatted. 
	 * @param lossAddressFormatted-the loss address formatted String.
	 */
	public void setLossAddressFormatted(String lossAddressFormatted) {
		this.lossAddressFormatted = lossAddressFormatted;
	}

	/**
	 * Method to get billing address country.
	 * @return-billing address country as String.
	 */
	public String getBillingAddressCountry() {
		return billingAddressCountry;
	}

	/**
	 * Method to set billing address country.
	 * @param billingAddressCountry-the billing address country String.
	 */
	public void setBillingAddressCountry(String billingAddressCountry) {
		this.billingAddressCountry = billingAddressCountry;
	}

	/**
	 * Method to get billing address latitude.
	 * @return-billing address latitude as String.
	 */
	public String getBillingAddressLat() {
		return billingAddressLat;
	}

	/**
	 * Method to set billing address latitude. 
	 * @param billingAddressLat-the billing address latitude String.
	 */
	public void setBillingAddressLat(String billingAddressLat) {
		this.billingAddressLat = billingAddressLat;
	}

	/**
	 * Method to get billing address longitude. 
	 * @return-billing address longitude as String. 
	 */
	public String getBillingAddressLan() {
		return billingAddressLan;
	}

	/**
	 * Method to set billing address longitude.  
	 * @param billingAddressLan-the billing address longitude String.  
	 */
	public void setBillingAddressLan(String billingAddressLan) {
		this.billingAddressLan = billingAddressLan;
	}

	/**
	 * Method to get billing address formatted. 
	 * @return-billing address formatted as String.
	 */
	public String getBillingAddressFormatted() {
		return billingAddressFormatted;
	}

	/**
	 * Method to set billing address formatted.  
	 * @param billingAddressFormatted-the billing address formatted String.
	 */
	public void setBillingAddressFormatted(String billingAddressFormatted) {
		this.billingAddressFormatted = billingAddressFormatted;
	}
}