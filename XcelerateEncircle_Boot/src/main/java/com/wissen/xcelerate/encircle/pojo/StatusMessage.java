/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.pojo;

/**
 * class for status message.
 */
public class StatusMessage {
	private String status;
	private String message;
	private Object data;
	
	/**
	 * Method to get Status.
	 * @return Status as string.
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Method to set Status.
	 * @param  Status-the Status is string.
	 */

	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Method to get Message.
	 * @return Message as string.
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Method to set Message.
	 * @param  Message-the Message is string.
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * Method to get Data.
	 * @return Data as object.
	 */
	public Object getData() {
		return data;
	}
	
	/**
	 * Method to set data.
	 * @param  data-the data is object.
	 */
	public void setData(Object data) {
		this.data = data;
	}	
}