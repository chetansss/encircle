/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.config;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;

import com.wissen.xcelerate.encircle.config.RabbitMQProperties;
import com.wissen.xcelerate.encircle.listener.EncircleWebhookListner;

/**
 * Class for Queue configuration.
 */
@Configuration
public class QueueConfig {

	@Autowired
	private RabbitMQProperties rabbitMQProperties;

	/**
	 * Method to get Encircle property claim media Queue. 
	 * @return-Queue.
	 */
	@Bean
	@Primary
	Queue queue() {
		return new Queue(rabbitMQProperties.getEncirclePropertyClaimMediaQueue(), true);
	}

	/**
	 * Method to get Encircle Webhook Exchange.
	 * @return-Topic exchange.
	 */
	@Bean
	TopicExchange exchange() {
		return new TopicExchange(rabbitMQProperties.getEncircleWebhookExchange());
	}

	/**
	 * Method to bind Queue with topic exchange. 
	 * @param queue
	 * @param exchange
	 * @return-Queue topic exchange binding.
	 */
	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue()).to(exchange)
				.with(rabbitMQProperties.getEncirclePropertyClaimMediaRoutingKey());
	}

	/**
	 * Method to get Queue name.
	 * @return-Queue.
	 */
	@Bean
	Queue queue1() {
		return new Queue(rabbitMQProperties.getQueueName(), true);
	}

	/**
	 * Method to get topic exchange name.
	 * @return-Topic exchange.
	 */
	@Bean
	TopicExchange exchange1() {
		return new TopicExchange(rabbitMQProperties.getExchangeName());
	}

	/**
	 * Method to bind Queue and Topic exchange.
	 * @param queue
	 * @param exchange
	 * @return-Qeue topic exchange binding. 
	 */
	@Bean
	Binding binding1(Queue queue, @Qualifier("exchange1") TopicExchange exchange) {
		return BindingBuilder.bind(queue1()).to(exchange).with(rabbitMQProperties.getRoutingKey());
	}

	/**
	 * Method to convert consumer in jackson format to message.
	 * @return-Message.
	 */
	@Bean
	public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
		return new MappingJackson2MessageConverter();
	}

	/**
	 * Method to create rabbit template.
	 * @param connectionFactory
	 * @return-rabbit template.
	 */
	@Bean
	public RabbitTemplate amqpTemplate(ConnectionFactory connectionFactory) {
		final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(messageConverter());
		return rabbitTemplate;
	}

	/**
	 * Method to convert message from Jackson format to JSON format.
	 * @return-JSON message.
	 */
	@Bean
	public Jackson2JsonMessageConverter messageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	/**
	 * Method to get encircle webhook media listener adapter. 
	 * @param listener
	 * @return-message listener adapter.
	 */
	@Bean
	MessageListenerAdapter encircleWebhookMediaListenerAdapter(EncircleWebhookListner listener) {
		return new MessageListenerAdapter(listener, "send");
	}

	/**
	 * Method to create a container for Queue.
	 * @param connectionFactory
	 * @param listenerAdapter
	 * @return-simple message listener container.
	 */
	@Bean
	SimpleMessageListenerContainer queuecontainer(ConnectionFactory connectionFactory,
			@Qualifier("encircleWebhookMediaListenerAdapter") MessageListenerAdapter listenerAdapter) {
		System.out.println("<============SimpleMessageListenerContainer========================>");
		SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
		simpleMessageListenerContainer.setConnectionFactory(connectionFactory);
		simpleMessageListenerContainer.setQueues(queue());
		simpleMessageListenerContainer.setQueueNames(rabbitMQProperties.getEncirclePropertyClaimMediaQueue());
		simpleMessageListenerContainer.setMessageListener(listenerAdapter);
		simpleMessageListenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
		return simpleMessageListenerContainer;
	}
}