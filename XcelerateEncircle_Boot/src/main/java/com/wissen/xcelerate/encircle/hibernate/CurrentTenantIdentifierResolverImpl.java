/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.hibernate;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

import com.wissen.xcelerate.encircle.tenant.TenantContext;

/**
 * Class for Current tenant identifier resolver.
 */
@Component
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {

	/**
	 * Method to get the current tenant.
	 * @return-current tenant as String.
	 */
	@Override
	public String resolveCurrentTenantIdentifier() {
		return TenantContext.getCurrentTenant();
	}

	/**
	 * Method to validate existing current sessions.
	 * @return- true as boolean.
	 */
	@Override
	public boolean validateExistingCurrentSessions() {
		return true;
	}
}