package com.wissen.xcelerate.encircle.repository;

/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.wissen.xcelerate.encircle.model.JobKpiTargets;

/**
 * Repository class for job kpi target list of  find All,find By Kpi Id In.
 * @author Rama
 */
public interface JobKpiTargetsRepository extends Repository<JobKpiTargets, Long> {
	
	List<JobKpiTargets> findAll();
	
	JobKpiTargets findByKpiId(@Param("kpiId") int kpiId);

	JobKpiTargets save(JobKpiTargets jobKpiTargets);
	
	@Query("select j from JobKpiTargets j where kpiId in :kpiIds")
	List<JobKpiTargets> findByKpiIdIn(@Param("kpiIds") List<Integer> kpiIds);

}
