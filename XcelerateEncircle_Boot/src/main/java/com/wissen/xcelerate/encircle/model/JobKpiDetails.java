/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Persistent class For Job KPI details.
 */
@Entity
@Table(name = "job_kpi_details")
public class JobKpiDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "job_kpi_id")
	private long jobKpiId;

	@Column(name = "job_id")
	private long jobId;

	@Column(name = "kpi_id")
	private int kpiId;

	@Column(name = "kpi_name")
	private String kpiName;

	@Column(name = "kpi_target_value")
	private float kpiTargetValue;

	@Column(name = "kpi_value")
	private float kpiValue;

	private String source;

	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "created_datetime")
	private Timestamp createdDatetime;

	@Column(name = "updated_by")
	private int updatedBy;

	@Column(name = "updated_datetime")
	private Timestamp updatedDatetime;

	private String uom;

	/**
	 * Method to get Jod ID.
	 * @return-job ID as Long.
	 */
	public long getJobId() {
		return jobId;
	}

	/**
	 * Method to set job ID.
	 * @param jobId-the job ID Long.
	 */
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	/**
	 * Method to get Job KPI ID. 
	 * @return-job KPI ID as Long.
	 */
	public long getJobKpiId() {
		return jobKpiId;
	}

	/**
	 * Method to set Job KPI ID. 
	 * @parameter-the job KPI ID Long.
	 */
	public void setJobKpiId(long jobKpiId) {
		this.jobKpiId = jobKpiId;
	}

	/**
	 * Method to get KPI ID. 
	 * @return-job KPI ID as Long.
	 */
	public int getKpiId() {
		return kpiId;
	}

	/**
	 * Method to set KPI ID. 
	 * @parameter-the KPI ID Long.
	 */
	public void setKpiId(int kpiId) {
		this.kpiId = kpiId;
	}

	/**
	 * Method to get KPI name. 
	 * @return-KPI name as String.
	 */
	public String getKpiName() {
		return kpiName;
	}

	/**
	 * Method to set KPI name. 
	 * @parameter-the KPI name STring.
	 */
	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	/**
	 * Method to get KPI Target Value. 
	 * @return-KPI Target Value as Float.
	 */
	public float getKpiTargetValue() {
		return kpiTargetValue;
	}

	/**
	 * Method to set KPI Target Value. 
	 * @parameter-the KPI Target Value Float.
	 */
	public void setKpiTargetValue(float kpiTargetValue) {
		this.kpiTargetValue = kpiTargetValue;
	}

	/**
	 * Method to get KPI Value. 
	 * @return-KPI Value as Float.
	 */
	public float getKpiValue() {
		return kpiValue;
	}

	/**
	 * Method to set KPI Value. 
	 * @parameter-the KPI Target Value Float.
	 */
	public void setKpiValue(float kpiValue) {
		this.kpiValue = kpiValue;
	}

	/**
	 * Method to get Source. 
	 * @return-Source as String.
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Method to set Source. 
	 * @parameter-the Source String.
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Method to get Created by. 
	 * @return-Created by as Integer.
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * Method to set Created by. 
	 * @param createdBy-the Created by Integer.
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Method to get Created Date time. 
	 * @return-Created Date time as Timestamp.
	 */
	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}

	/**
	 * Method to set Created Date time.  
	 * @param createdDatetime-the Created Date time Timestamp.
	 */
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	/**
	 * Method to get Update by. 
	 * @return-updated by as Integer.
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set Update by.  
	 * @param updatedBy-the updated by Integer. 
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get updated Date time. 
	 * @return-Created Date time as Timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set updated Date time. 
	 * @param-the updated Date time Timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get UOM.
	 * @return-uom as String.
	 */
	public String getUom() {
		return uom;
	}

	/**
	 * Method to set UOM.
	 * @param uom-the UOM String.
	 */
	public void setUom(String uom) {
		this.uom = uom;
	}
}
