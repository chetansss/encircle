package com.wissen.xcelerate.encircle.pojo;

import java.io.Serializable;

/*
 * class for Encircle Webhook.
 */
public class EncircleWebhook  implements Serializable{

	private static final long serialVersionUID = 1L;
	private String baseUrl;
	private String payload;
	
	/**
	 * Method to get  Base Url.
	 * @return Base Url as string.
	 */
	public String getBaseUrl() {
		return baseUrl;
	}
	
	/**
	 * Method to set Base Url.
	 * @param  Base Url-the Base Url is string.
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	/**
	 * Method to get  Pay load.
	 * @return Pay load as string.
	 */
	public String getPayload() {
		return payload;
	}
	
	/**
	 * Method to set Pay load.
	 * @param  Pay load-the Pay load is string.
	 */
	public void setPayload(String payload) {
		this.payload = payload;
	}	
}
