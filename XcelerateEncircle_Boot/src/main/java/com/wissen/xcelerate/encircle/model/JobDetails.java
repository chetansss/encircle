/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model;

import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.*;
import java.io.Serializable;

//import com.wissen.xcelerate.pojo.SubJobDetails;

/**
 * The persistent class for the job_details database table.
 */
@Entity
@Table(name = "job_details")
//@NamedQuery(name="JobDetails.findAll", query="SELECT j FROM JobDetails j")
//@NamedStoredProcedureQueries({
//	@NamedStoredProcedureQuery(name="jobList",procedureName="JobList",resultClasses=SubJobDetails.class)
//})
public class JobDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "job_id")
	private int jobId;

	@Column(name = "actual_amount")
	private Integer actualAmount;

	@Column(name = "actual_job_id")
	private Integer actualJobId;

	@Column(name = "budgeted_amount")
	private Integer budgetedAmount;

	@Column(name = "previous_job_status")
	private String previousJobStatus;

	@Column(name = "current_job_status")
	private String currentJobStatus;

	@Column(name = "estimated_amount")
	private Integer estimatedAmount;

	@Column(name = "geo_location")
	private String geoLocation;

	@Column(name = "invoiced_amount")
	private Integer invoicedAmount;

	@Column(name = "is_duplicate")
	private String isDuplicate;

	@Column(name = "is_insured")
	private String isInsured;

	@Column(name = "job_amount")
	private Integer jobAmount;

	@Column(name = "job_code")
	private String jobCode;

	@Column(name = "job_name")
	private String jobName;

	@Column(name = "job_desc")
	private String jobDesc;

	@Column(name = "job_hash_code")
	private String jobHashCode;

	@Column(name = "payment_terms_id")
	private Integer paymentTermsId;

	@Column(name = "job_type")
	private String jobType;

	@Column(name = "loss_address")
	private String lossAddress;

	@Column(name = "loss_address_city")
	private String lossAddressCity;

	@Column(name = "loss_address_state")
	private String lossAddressState;

	@Column(name = "loss_address_zip")
	private String lossAddressZip;

	@Column(name = "billing_address")
	private String billingAddress;

	@Column(name = "billing_address_city")
	private String billingAddressCity;

	@Column(name = "billing_address_state")
	private String billingAddressState;

	@Column(name = "billing_address_zip")
	private String billingAddressZip;

	@Column(name = "is_billing_address_same")
	private String isBillingAddressSame;

	private String latitude;

	private String longitude;

	@Column(name = "master_number_copy")
	private String masterNumberCopy;

	@Column(name = "tenant_name")
	private String tenantName;

	@Column(name = "tenant_number")
	private String tenantNumber;

	@Column(name = "property_type")
	private String propertyType;

	@Column(name = "program_type")
	private Integer programType;

	@Column(name = "cause_of_loss")
	private Integer causeOfLoss;

	@Column(name = "year_built")
	private Integer yearBuilt;

	@Column(name = "referred_source")
	private String referredSource;

	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "created_datetime")
	private Timestamp createdDatetime;

	@Column(name = "updated_by")
	private int updatedBy;

	@Column(name = "updated_datetime")
	private Timestamp updatedDatetime;

	@ManyToOne
	@JoinColumn(name = "mast_num_id")
	private MastNumDetails mastNumDetails;

	// bi-directional many-to-one association to RestCompLocation
	@Column(name = "rest_comp_location_id")
	private int restCompLocationId;

	// bi-directional many-to-one association to UserUserDetails
	@ManyToOne
	@JoinColumn(name = "program_coordinator")
	private UserDetails programCoordinator;

	// bi-directional many-to-one association to UserUserDetails
	@ManyToOne
	@JoinColumn(name = "project_manager")
	private UserDetails projectManager;

	// bi-directional many-to-one association to UserUserDetails
	@ManyToOne
	@JoinColumn(name = "estimator")
	private UserDetails estimator;

	private String source;

	@Column(name = "encircle_claim_id")
	private Integer encircleClaimId;

	@Column(name = "dash_file_number")
	private String dashFileNumber;

	@Column(name = "dash_file_created_date")
	private Date dashFileCreatedDate;

	@Column(name = "loss_date")
	private Date lossDate;

	private Long tpa;

	@Column(name = "tpa_name")
	private String tpaName;

	@Column(name = "last_activity")
	private Timestamp lastActivity;
	// bi-directional many-to-one association to JobEstimates
	/*
	 * @OneToMany(mappedBy="jobDetail") private List<JobEstimates> jobEstimates;
	 * 
	 * //bi-directional many-to-one association to JobInsuranceDetails
	 * 
	 * @OneToMany(mappedBy="jobId") private List<JobInsuranceDetails>
	 * jobInsuranceDetails;
	 * 
	 * //bi-directional many-to-one association to JobStatusHistory
	 * 
	 * @OneToMany(mappedBy="jobDetail") private List<JobStatusHistory>
	 * jobStatusHistories;
	 */

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	private String email;

	private String mobile;

	@Column(name = "work_phone")
	private String workPhone;

	private String phone1;

	@Column(name = "customer_type")
	private String customerType;

	@Column(name = "customer_company")
	private Integer customerCompany;

	@Column(name = "customer_company_name")
	private String customerCompanyName;

	private String company;

	@Column(name = "company_id")
	private Integer companyId;

	@Column(name = "link_to_master")
	private String linkToMaster;

	@Column(name = "matterport_url")
	private String matterportUrl;

	@Column(name = "qb_status")
	private String qbStatus;

	private String scratchpad;

	@Column(name = "scratchpad_date")
	private Date scratchpadDate;

	@Column(name = "qb_date")
	private Date qbDate;

	@Column(name = "qb_flag")
	private String qbFlag;

	@Column(name = "nps_score")
	private Integer npsScore;

	@Column(name = "unit_number")
	private String unitNumber;

	@Column(name = "qbo_status")
	private String qboStatus;

	@Column(name = "qbo_date")
	private Date qboDate;

	@Column(name = "loss_address_country")
	private String lossAddressCountry;

	@Column(name = "loss_address_lat")
	private String lossAddressLat;

	@Column(name = "loss_address_lan")
	private String lossAddressLan;

	@Column(name = "loss_address_formatted")
	private String lossAddressFormatted;

	@Column(name = "billing_address_country")
	private String billingAddressCountry;

	@Column(name = "billing_address_lat")
	private String billingAddressLat;

	@Column(name = "billing_address_lan")
	private String billingAddressLan;

	@Column(name = "billing_address_formatted")
	private String billingAddressFormatted;

	@Column(name = "qbo_customer_failure")
	private String qboCustomerFailure;

	@Column(name = "encircle_permalink")
	private String encirclePermalink;

	@Column(name = "zapier_updated_by")
	private String zapierUpdatedBy;

	// Default Constructor.
	public JobDetails() {
	}

	// Parameterized constructor.
	public JobDetails(String jobCode, String jobHashCode, int jobId) {
		this.jobCode = jobCode;
		this.jobHashCode = jobHashCode;
		this.jobId = jobId;
	}

	// Parameterized constructor.
	public JobDetails(String jobCode, String jobHashCode, String jobType, String currentJobStatus) {
		this.jobHashCode = jobHashCode;
		this.jobCode = jobCode;
		this.jobType = jobType;
		this.currentJobStatus = currentJobStatus;
	}

	// Parameterized constructor.
	public JobDetails(String jobCode, String jobHashCode, String jobType, String currentJobStatus, String jobName) {
		this.jobHashCode = jobHashCode;
		this.jobCode = jobCode;
		this.jobType = jobType;
		this.currentJobStatus = currentJobStatus;
		this.jobName = jobName;
	}

	// Parameterized constructor.	
	public JobDetails(String jobCode, String jobHashCode, String jobType, String currentJobStatus, String qbStatus,
			String qbFlag, String firstName, String lastName, String customerType, String customerCompanyName,
			String company, int jobId) {
		this.jobHashCode = jobHashCode;
		this.jobCode = jobCode;
		this.jobType = jobType;
		this.currentJobStatus = currentJobStatus;
		this.qbStatus = qbStatus;
		this.qbFlag = qbFlag;
		this.firstName = firstName;
		this.lastName = lastName;
		this.customerType = customerType;
		this.customerCompanyName = customerCompanyName;
		this.jobId = jobId;
	}

	// Parameterized constructor.
	public JobDetails(String jobCode, String jobHashCode, String jobType, String currentJobStatus, String qbStatus,
			String qbFlag, String firstName, String lastName, String customerType, String customerCompanyName,
			String company, int jobId, Date qbDate) {
		this.jobHashCode = jobHashCode;
		this.jobCode = jobCode;
		this.jobType = jobType;
		this.currentJobStatus = currentJobStatus;
		this.qbStatus = qbStatus;
		this.qbFlag = qbFlag;
		this.firstName = firstName;
		this.lastName = lastName;
		this.customerType = customerType;
		this.customerCompanyName = customerCompanyName;
		this.jobId = jobId;
		this.qbDate = qbDate;
	}

	// Parameterized constructor.
	public JobDetails(String jobCode, String jobHashCode, String jobType, String currentJobStatus, String qbStatus,
			String qbFlag, String firstName, String lastName, String customerType, String customerCompanyName,
			String company, int jobId, Date qbDate, String qboStatus, Date qboDate) {
		this.jobHashCode = jobHashCode;
		this.jobCode = jobCode;
		this.jobType = jobType;
		this.currentJobStatus = currentJobStatus;
		this.qbStatus = qbStatus;
		this.qbFlag = qbFlag;
		this.firstName = firstName;
		this.lastName = lastName;
		this.customerType = customerType;
		this.customerCompanyName = customerCompanyName;
		this.jobId = jobId;
		this.qbDate = qbDate;
		this.qboStatus = qboStatus;
		this.qboDate = qboDate;
	}

	// Parameterized constructor.
	public JobDetails(int jobId, String jobCode, String jobHashCode, String jobType, String currentJobStatus) {
		this.jobId = jobId;
		this.jobHashCode = jobHashCode;
		this.jobCode = jobCode;
		this.jobType = jobType;
		this.currentJobStatus = currentJobStatus;
	}

	// Parameterized constructor.
	public JobDetails(String jobCode, String jobHashCode, String jobType, String currentJobStatus, String qbStatus,
			String qbFlag, String firstName, String lastName, String customerType, String customerCompanyName,
			String company, int jobId, Date qbDate, String qboStatus, Date qboDate, String qboCustomerFailure) {
		this.jobHashCode = jobHashCode;
		this.jobCode = jobCode;
		this.jobType = jobType;
		this.currentJobStatus = currentJobStatus;
		this.qbStatus = qbStatus;
		this.qbFlag = qbFlag;
		this.firstName = firstName;
		this.lastName = lastName;
		this.customerType = customerType;
		this.customerCompanyName = customerCompanyName;
		this.jobId = jobId;
		this.qbDate = qbDate;
		this.qboStatus = qboStatus;
		this.qboDate = qboDate;
		this.qboCustomerFailure = qboCustomerFailure;
	}

	/**
	 * Method to get Job ID.
	 * @return-job ID as Integer.
	 */
	public int getJobId() {
		return this.jobId;
	}

	/**
	 * Method to set Job ID. 
	 * @param jobId-the job ID Integer.
	 */
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	/**
	 * Method to get Job Actual Amount.
	 * @return-Actual Amount as Integer.
	 */
	public Integer getActualAmount() {
		return this.actualAmount;
	}

	/**
	 * Method to set Job Actual Amount. 
	 * @param Actual amount-the job Job Actual Amount Integer. 
	 */
	public void setActualAmount(Integer actualAmount) {
		this.actualAmount = actualAmount;
	}

	/**
	 * Method to get Actual Job iD.
	 * @return-Actual job ID as Integer.
	 */
	public Integer getActualJobId() {
		return this.actualJobId;
	}

	/**
	 * Method to set Actual Job iD. 
	 * @param jobId-the Actual job ID Integer.
	 */
	public void setActualJobId(Integer actualJobId) {
		this.actualJobId = actualJobId;
	}

	/**
	 * Method to get Budgeted Amount. 
	 * @return- Budgeted Amount. as Integer.
	 */
	public Integer getBudgetedAmount() {
		return this.budgetedAmount;
	}

	/**
	 * Method to set Budgeted Amount. 
	 * @param budgetedAmount-the Budgeted Amount Integer.
	 */
	public void setBudgetedAmount(Integer budgetedAmount) {
		this.budgetedAmount = budgetedAmount;
	}

	/**
	 * Method to get Previous Job Status.
	 * @return-Previous Job Status as String.
	 */
	public String getPreviousJobStatus() {
		return previousJobStatus;
	}

	/**
	 * Method to set Previous Job Status.
	 * @param previousjobstatus-thePrevious Job Status String.
	 */
	public void setPreviousJobStatus(String previousJobStatus) {
		this.previousJobStatus = previousJobStatus;
	}

	/**
	 * Method to get Current job Status. 
	 * @return-Current job Status as String. 
	 */
	public String getCurrentJobStatus() {
		return this.currentJobStatus;
	}

	/**
	 * Method to set Current job Status.  
	 * @param currentJobStatus-the  Current job Status as String.  
	 */
	public void setCurrentJobStatus(String currentJobStatus) {
		this.currentJobStatus = currentJobStatus;
	}

	/**
	 * Method to get Estimated amount. 
	 * @return-estimated amount as Integer. 
	 */
	public Integer getEstimatedAmount() {
		return this.estimatedAmount;
	}

	/**
	 * Method to set Estimated amount.  
	 * @param estimatedAmount-the Estimated amount Integer. 
	 */
	public void setEstimatedAmount(Integer estimatedAmount) {
		this.estimatedAmount = estimatedAmount;
	}

	/**
	 * Method to get geolocation.  
	 * @return-geo location as String.
	 */
	public String getGeoLocation() {
		return this.geoLocation;
	}

	/**
	 * Method to set geolocation.  
	 * @param geolocation-the geolocation String. 
	 */
	public void setGeoLocation(String geoLocation) {
		this.geoLocation = geoLocation;
	}

	/**
	 * Method to get Invoiced amount.  
	 * @return-invoiced amount as Integer.
	 */
	public Integer getInvoicedAmount() {
		return this.invoicedAmount;
	}

	/**
	 * Method to set Invoiced amount.  
	 * @param invoicedAmount-the invoiced amount Integer. 
	 */
	public void setInvoicedAmount(Integer invoicedAmount) {
		this.invoicedAmount = invoicedAmount;
	}

	/**
	 * Method to get Is Duplicate.  
	 * @return-Is Duplicate as String.
	 */
	public String getIsDuplicate() {
		return this.isDuplicate;
	}

	/**
	 * Method to set Is Duplicate.  
	 * @param isDuplicate-the Is Duplicate String.
	 */
	public void setIsDuplicate(String isDuplicate) {
		this.isDuplicate = isDuplicate;
	}

	/**
	 * Method to get Is Insured.  
	 * @return-Is Insured as String.
	 */
	public String getIsInsured() {
		return this.isInsured;
	}

	/**
	 * Method to set Is Insured.   
	 * @param isInsured-the Is Insured String.
	 */
	public void setIsInsured(String isInsured) {
		this.isInsured = isInsured;
	}

	/**
	 * Method to get Job amount. 
	 * @return-job amount as Integer.
	 */
	public Integer getJobAmount() {
		return this.jobAmount;
	}

	/**
	 * Method to set Job amount.  
	 * @param jobAmount-the ob amount Integer.
	 */
	public void setJobAmount(Integer jobAmount) {
		this.jobAmount = jobAmount;
	}

	/**
	 * Method to get Job code. 
	 * @return-job code as String.
	 */
	public String getJobCode() {
		return this.jobCode;
	}

	/**
	 * Method to set Job code. 
	 * @param jobCode-the job code String.
	 */
	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	/**
	 * Method to get Job name.  
	 * @return-job name as String.
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * Method to set Job name.   
	 * @param jobName-the job name String.
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * Method to get Job description. 
	 * @return-Job description as String.
	 */
	public String getJobDesc() {
		return this.jobDesc;
	}

	/**
	 * Method to set Job description.  
	 * @param jobDesc-the Job description String. 
	 */
	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	/**
	 * Method to get Job hash code. 
	 * @return-job hash code as String.
	 */
	public String getJobHashCode() {
		return this.jobHashCode;
	}

	/**
	 * Method to set Job hash code. 
	 * @param jobHashCode-the job hash code String.
	 */
	public void setJobHashCode(String jobHashCode) {
		this.jobHashCode = jobHashCode;
	}

	/**
	 * Method to get Payment Terms ID.  
	 * @return-Payment Terms ID as Integer.
	 */
	public Integer getPaymentTermsId() {
		return this.paymentTermsId;
	}

	/**
	 * Method to set Payment Terms ID.   
	 * @param paymentTermsId-the Payment Terms ID Integer.   
	 */
	public void setPaymentTermsId(Integer paymentTermsId) {
		this.paymentTermsId = paymentTermsId;
	}

	/**
	 * Method to get Job type.  
	 * @return-job type as String.
	 */
	public String getJobType() {
		return this.jobType;
	}

	/**
	 * Method to set Job type.   
	 * @param jobType-the job type String.
	 */
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	/**
	 * Method to get Loss Address. 
	 * @return-loss Address as String.
	 */
	public String getLossAddress() {
		return lossAddress;
	}

	/**
	 * Method to set Loss Address.  
	 * @param lossAddress-the Loss Address String. 
	 */
	public void setLossAddress(String lossAddress) {
		this.lossAddress = lossAddress;
	}

	/**
	 * Method to get Loss Address city. 
	 * @return-loss Address city as String.
	 */
	public String getLossAddressCity() {
		return lossAddressCity;
	}

	/**
	 * Method to set Loss Address city.  
	 * @param lossAddressCity-the loss Address city String.
	 */
	public void setLossAddressCity(String lossAddressCity) {
		this.lossAddressCity = lossAddressCity;
	}

	/**
	 * Method to get Loss Address state.   
	 * @return-loss address state as String.
	 */
	public String getLossAddressState() {
		return lossAddressState;
	}

	/**
	 * Method to set Loss Address state.   
	 * @param lossAddressState-the loss address state String. 
	 */
	public void setLossAddressState(String lossAddressState) {
		this.lossAddressState = lossAddressState;
	}

	/**
	 * Method to get Loss Address zip.   
	 * @return-loss address zip as String.
	 */
	public String getLossAddressZip() {
		return lossAddressZip;
	}

	/**
	 * Method to set Loss Address zip.    
	 * @param lossAddressZip-the loss address zip String.
	 */
	public void setLossAddressZip(String lossAddressZip) {
		this.lossAddressZip = lossAddressZip;
	}

	/**
	 * Method to get Billing address.   
	 * @return-Billing address as String.
	 */
	public String getBillingAddress() {
		return billingAddress;
	}

	/**
	 * Method to set Billing address.    
	 * @param billingAddress-the Billing address String.
	 */
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	/**
	 * Method to get Billing address city.   
	 * @return-Billing address city as String. 
	 */
	public String getBillingAddressCity() {
		return billingAddressCity;
	}

	/**
	 * Method to set Billing address city.   
	 * @param billingAddressCity-the Billing address city String. 
	 */
	public void setBillingAddressCity(String billingAddressCity) {
		this.billingAddressCity = billingAddressCity;
	}

	/**
	 * Method to get Billing address state.   
	 * @return-Billing address state as String. 
	 */
	public String getBillingAddressState() {
		return billingAddressState;
	}

	/**
	 * Method to set Billing address state. 
	 * @param billingAddressState-the Billing address state String. 
	 */
	public void setBillingAddressState(String billingAddressState) {
		this.billingAddressState = billingAddressState;
	}

	/**
	 * Method to get Billing address zip.   
	 * @return-Billing address zip as String. 
	 */
	public String getBillingAddressZip() {
		return billingAddressZip;
	}

	/**
	 * Method to set Billing address zip. 
	 * @param billingAddressState-the Billing address zip String. 
	 */
	public void setBillingAddressZip(String billingAddressZip) {
		this.billingAddressZip = billingAddressZip;
	}

	/**
	 * Method to get Is Billing Address Same.   
	 * @return-Is Billing Address Same as String.
	 */
	public String getIsBillingAddressSame() {
		return isBillingAddressSame;
	}

	/**
	 * Method to set Is Billing Address Same.    
	 * @param isBillingAddressSame-the Is Billing Address Same String.
	 */
	public void setIsBillingAddressSame(String isBillingAddressSame) {
		this.isBillingAddressSame = isBillingAddressSame;
	}

	/**
	 * Method to get Latitude.   
	 * @return-Latitude as String.
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * Method to set Latitude 
	 * @param-the latitude String.
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * Method to get Longitude.   
	 * @return-Longitude as String.
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * Method to set Longitude.   
	 * @param longitude-the Longitude String.
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * Method to get Master Number Copy.   
	 * @return-Master Number Copy as String.
	 */
	public String getMasterNumberCopy() {
		return masterNumberCopy;
	}

	/**
	 * Method to set Master Number Copy. 
	 * @param masterNumberCopy-the Master Number Copy String.
	 */
	public void setMasterNumberCopy(String masterNumberCopy) {
		this.masterNumberCopy = masterNumberCopy;
	}

	/**
	 * Method to get Tenant Name.   
	 * @return-Tenant Name as String.
	 */
	public String getTenantName() {
		return tenantName;
	}

	/**
	 * Method to set Tenant Name.    
	 * @param tenantName-the Tenant Name String.
	 */
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	/**
	 * Method to get Tenant Number. 
	 * @return-tenant number as String.
	 */
	public String getTenantNumber() {
		return tenantNumber;
	}

	/**
	 * Method to set Tenant Number.  
	 * @param tenantNumber-the tenant number String.
	 */
	public void setTenantNumber(String tenantNumber) {
		this.tenantNumber = tenantNumber;
	}

	/**
	 * Method to get property type.
	 * @return-program type as STring. 
	 */
	public String getPropertyType() {
		return propertyType;
	}

	/**
	 * Method to set property type. 
	 * @param propertyType-the property type String.
	 */
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	 
	/**
	 * Method to get property type. 
	 * @return-program type as Integer.
	 */
	public Integer getProgramType() {
		return programType;
	}

	/**
	 * Method to set property type.  
	 * @param programType-the program type Integer.
	 */
	public void setProgramType(Integer programType) {
		this.programType = programType;
	}

	/**
	 * Method to get cause of loss. 
	 * @return-cause of loss as Integer.
	 */
	public Integer getCauseOfLoss() {
		return causeOfLoss;
	}

	/**
	 * Method to set pcause of loss.  
	 * @param causeOfLoss-the cause of loss Integer.
	 */
	public void setCauseOfLoss(Integer causeOfLoss) {
		this.causeOfLoss = causeOfLoss;
	}

	/**
	 * Method to get Year Built. 
	 * @return-Year Built as Integer.
	 */
	public Integer getYearBuilt() {
		return yearBuilt;
	}

	/**
	 * Method to set Year Built. 
	 * @param yearBuilt-the year built Integer.
	 */
	public void setYearBuilt(Integer yearBuilt) {
		this.yearBuilt = yearBuilt;
	}

	/**
	 * Method to get referred source. 
	 * @return-referred source as String.
	 */
	public String getReferredSource() {
		return referredSource;
	}

	/**
	 * Method to set referred source. 
	 * @param referredSource-the referred source String.
	 */
	public void setReferredSource(String referredSource) {
		this.referredSource = referredSource;
	}

	/**
	 * Method to get created by.
	 * @return-created by as Integer.
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * Method to set created by. 
	 * @param createdBy-the created by Integer. 
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Method to get created date and time.
	 * @return-created date and time as Timestamp.
	 */
	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}

	/**
	 * Method to set created date and time.
	 * @param createdDatetime-the created date and time Timestamp.
	 */
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	/**
	 * Method to get updated by.
	 * @return-updated by as Integer.
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set updated by. 
	 * @param updatedBy-the updated by Integer.
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get updated Date time.
	 * @return-updated date and time as Timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set updated Date time.
	 * @param updatedDatetime-the updated date and time Timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get Master Number Details.
	 * @return-Master Number Details as Master Number Details. 
	 */
	public MastNumDetails getMastNumDetails() {
		return mastNumDetails;
	}

	/**
	 * Method to set Master Number Details. 
	 * @param mastNumDetails-the Master Number Details Master Number Details.
	 */
	public void setMastNumDetails(MastNumDetails mastNumDetails) {
		this.mastNumDetails = mastNumDetails;
	}

	/**
	 * Method to get Restoration company location ID.
	 * @return-Restoration company location ID as Integer.
	 */
	public int getRestCompLocationId() {
		return restCompLocationId;
	}

	/**
	 * Method to set Restoration company location ID. 
	 * @param restCompLocationId-the Restoration company location ID Integer.
	 */
	public void setRestCompLocationId(int restCompLocationId) {
		this.restCompLocationId = restCompLocationId;
	}

	/**
	 * Method to get Program coordinator.
	 * @return-program coordinator as User details.
	 */
	public UserDetails getProgramCoordinator() {
		return programCoordinator;
	}

	/**
	 * Method to set Program coordinator.
	 * @param programCoordinator-the program coordinator User details.
	 */
	public void setProgramCoordinator(UserDetails programCoordinator) {
		this.programCoordinator = programCoordinator;
	}

	/**
	 * Method to get Program Project Manager.
	 * @return-Project Manager as User details.
	 */
	public UserDetails getProjectManager() {
		return projectManager;
	}

	/**
	 * Method to set Program Project Manager. 
	 * @param projectManager-the Project Manager User details. 
	 */
	public void setProjectManager(UserDetails projectManager) {
		this.projectManager = projectManager;
	}

	/**
	 * Method to get Estimator.
	 * @return-estimator as User details.
	 */
	public UserDetails getEstimator() {
		return estimator;
	}

	/**
	 * Method to set estimator.
	 * @param estimator-the estimator User details.
	 */
	public void setEstimator(UserDetails estimator) {
		this.estimator = estimator;
	}

	/**
	 * Method to get Source.
	 * @return-Source as String.
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Method to set Source. 
	 * @param source-the source String.
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Method to get Encircle claim ID.
	 * @return-encircle claim ID as Integer.
	 */
	public Integer getEncircleClaimId() {
		return encircleClaimId;
	}

	/**
	 * Method to set Encircle claim ID. 
	 * @param encircleClaimId-the encircle claim ID Integer.
	 */
	public void setEncircleClaimId(Integer encircleClaimId) {
		this.encircleClaimId = encircleClaimId;
	}

	/**
	 * Method to get Dash file number.
	 * @return-dash file number as String.
	 */
	public String getDashFileNumber() {
		return dashFileNumber;
	}

	/**
	 * Method to set Dash file number. 
	 * @param dashFileNumber-dash file number String.
	 */
	public void setDashFileNumber(String dashFileNumber) {
		this.dashFileNumber = dashFileNumber;
	}

	/**
	 * Method to get Dash file created date.
	 * @return-dash file created date as Date.
	 */
	public Date getDashFileCreatedDate() {
		return dashFileCreatedDate;
	}

	/**
	 * Method to set Dash file created date. 
	 * @param dashFileCreatedDate-the dash file created date Date.
	 */
	public void setDashFileCreatedDate(Date dashFileCreatedDate) {
		this.dashFileCreatedDate = dashFileCreatedDate;
	}

	/**
	 * Method to get Loss date.
	 * @return-Loss date as Date.
	 */
	public Date getLossDate() {
		return lossDate;
	}

	/**
	 * Method to set Loss date. 
	 * @param lossDate-the loss date Date.
	 */
	public void setLossDate(Date lossDate) {
		this.lossDate = lossDate;
	}

	/**
	 * Method to get TPA.
	 * @return-TPA as Long.
	 */
	public Long getTpa() {
		return tpa;
	}

	/**
	 * Method to set TPA. 
	 * @param tpa-the TPA Long.
	 */
	public void setTpa(Long tpa) {
		this.tpa = tpa;
	}

	/**
	 * Method to get TPA name.
	 * @return-TPA name as String.
	 */
	public String getTpaName() {
		return tpaName;
	}

	/**
	 * Method to set TPA name. 
	 * @param tpaName-the TPA name String.
	 */
	public void setTpaName(String tpaName) {
		this.tpaName = tpaName;
	}

	/**
	 * Method to get first name.
	 * @return-first name as String.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Method to set first name. 
	 * @param firstName-the first name String.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Method to get last name.
	 * @return-last name as String.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Method to set last name. 
	 * @param lastName-the last name String.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Method to get E-mail.
	 * @return-Email as String.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Method to set E-mail.
	 * @param email-the Email String.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Method to get Mobile.
	 * @return-mobile as String.
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Method to set mobile.
	 * @param mobile-the mobile String.
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * Method to get work phone.
	 * @return-work phone as String.
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * Method to set work phone. 
	 * @param workPhone-the work phone String.
	 */
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	/**
	 * Method to get phone1. 
	 * @return-phone1 as String.
	 */
	public String getPhone1() {
		return phone1;
	}

	/**
	 * Method to set phone1. 
	 * @param phone1-the phone1 String.
	 */
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	/**
	 * Method to get Customer type.
	 * @return-customer type as String.
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * Method to set Customer type. 
	 * @param customerType-the customer type String.
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * Method to get Customer company.
	 * @return-customer company as Integer.
	 */
	public Integer getCustomerCompany() {
		return customerCompany;
	}

	/**
	 * Method to set Customer company.
	 * @param customerCompany-the customer company Integer.
	 */
	public void setCustomerCompany(Integer customerCompany) {
		this.customerCompany = customerCompany;
	}

	/**
	 * Method to get Customer company name. 
	 * @return-customer company name as String.
	 */
	public String getCustomerCompanyName() {
		return customerCompanyName;
	}

	/**
	 * Method to set Customer company name.  
	 * @param customerCompanyName-the customer company name String.
	 */
	public void setCustomerCompanyName(String customerCompanyName) {
		this.customerCompanyName = customerCompanyName;
	}

	/**
	 * Method to get company.
	 * @return-company as String.
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * Method to set company.
	 * @param company-the company String.
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * Method to get company ID. 
	 * @return-company Id as Integer.
	 */
	public Integer getCompanyId() {
		return companyId;
	}

	/**
	 * Method to set company ID. 
	 * @param companyId-the company Id Integer
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	/**
	 * Method to get Link To Master. 
	 * @return-Link To Master as String.
	 */
	public String getLinkToMaster() {
		return linkToMaster;
	}

	/**
	 * Method to set Link To Master.  
	 * @param linkToMaster-the Link To Master String.
	 */
	public void setLinkToMaster(String linkToMaster) {
		this.linkToMaster = linkToMaster;
	}

	/**
	 * Method to get Matter prot URl.
	 * @return-Matter prot URl as String.
	 */
	public String getMatterportUrl() {
		return matterportUrl;
	}

	/**
	 * Method to set Matter prot URl.
	 * @param matterportUrl-the Matter prot URl String.
	 */
	public void setMatterportUrl(String matterportUrl) {
		this.matterportUrl = matterportUrl;
	}

	/**
	 * Method to get Last Activity.
	 * @return-Last Activity as String.
	 */
	public Timestamp getLastActivity() {
		return lastActivity;
	}

	/**
	 * Method to set Last Activity. 
	 * @param lastActivity-the Last Activity String.
	 */
	public void setLastActivity(Timestamp lastActivity) {
		this.lastActivity = lastActivity;
	}

	/**
	 * Method to get QBO status.
	 * @return-QBO status as String.
	 */
	public String getQbStatus() {
		return qbStatus;
	}

	/**
	 * Method to set QBO status. 
	 * @param qbStatus-the QBO status String.
	 */
	public void setQbStatus(String qbStatus) {
		this.qbStatus = qbStatus;
	}

	/**
	 * Method to get Scratchpad.
	 * @return-Scratchpad as String.
	 */
	public String getScratchpad() {
		return scratchpad;
	}

	/**
	 * Method to set Scratchpad. 
	 * @param scratchpad-the Scratchpad String.
	 */
	public void setScratchpad(String scratchpad) {
		this.scratchpad = scratchpad;
	}

	/**
	 * Method to get Scratchpad date.
	 * @return-Scratchpad date as Date.
	 */
	public Date getScratchpadDate() {
		return scratchpadDate;
	}

	/**
	 * Method to set Scratchpad date.
	 * @param scratchpadDate-the Scratchpad date Date.
	 */
	public void setScratchpadDate(Date scratchpadDate) {
		this.scratchpadDate = scratchpadDate;
	}

	/**
	 * Method to get QBO date.
	 * @return-QBO date as Date.
	 */
	public Date getQbDate() {
		return qbDate;
	}

	/**
	 * Method to set QBO date.
	 * @param qbDate-the QBO date Date.
	 */
	public void setQbDate(Date qbDate) {
		this.qbDate = qbDate;
	}

	/**
	 * Method to get QB flag.
	 * @return-QB flag as String.
	 */
	public String getQbFlag() {
		return qbFlag;
	}

	/**
	 * Method to set QB flag. 
	 * @param qbFlag-the QB flag String.
	 */
	public void setQbFlag(String qbFlag) {
		this.qbFlag = qbFlag;
	}

	/**
	 * Method to get Nps Score.
	 * @return-NPS score as Integer.
	 */
	public Integer getNpsScore() {
		return npsScore;
	}

	/**
	 * Method to set Nps Score. 
	 * @param npsScore-the NPS score Integer.
	 */
	public void setNpsScore(Integer npsScore) {
		this.npsScore = npsScore;
	}

	/**
	 * Method to get Unit number.
	 * @return-unit number as String.
	 */
	public String getUnitNumber() {
		return unitNumber;
	}

	/**
	 * Method to set Unit number.
	 * @param unitNumber-the unit number String.
	 */
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	/**
	 * Method to get QBO status.
	 * @return-QBO status as String.
	 */
	public String getQboStatus() {
		return qboStatus;
	}

	/**
	 * Method to set QBO status.
	 * @param qboStatus-the QBO status String.
	 */
	public void setQboStatus(String qboStatus) {
		this.qboStatus = qboStatus;
	}

	/**
	 * Method to get QBO date.
	 * @return-QBO date as Date.
	 */
	public Date getQboDate() {
		return qboDate;
	}

	/**
	 * Method to set QBO date.
	 * @param qboDate-the QBO date Date.
	 */
	public void setQboDate(Date qboDate) {
		this.qboDate = qboDate;
	}

	/**
	 * Method to get Loss address country.
	 * @return-loss address country as String.
	 */
	public String getLossAddressCountry() {
		return lossAddressCountry;
	}

	/**
	 * Method to set Loss address country. 
	 * @param lossAddressCountry-the loss address country String.
	 */
	public void setLossAddressCountry(String lossAddressCountry) {
		this.lossAddressCountry = lossAddressCountry;
	}

	/**
	 * Method to get Loss address latitude. 
	 * @return-Loss address latitude as String.
	 */
	public String getLossAddressLat() {
		return lossAddressLat;
	}

	/**
	 * Method to set Loss address latitude.  
	 * @param lossAddressLat-the Loss address latitude String.
	 */
	public void setLossAddressLat(String lossAddressLat) {
		this.lossAddressLat = lossAddressLat;
	}

	/**
	 * Method to get loss address Longitude.
	 * @return-loss address longitude as String.
	 */
	public String getLossAddressLan() {
		return lossAddressLan;
	}

	/**
	 * Method to set loss address Longitude. 
	 * @param lossAddressLan-the loss address longitude String.
	 */
	public void setLossAddressLan(String lossAddressLan) {
		this.lossAddressLan = lossAddressLan;
	}

	/**
	 * Method to get loss address formatted. 
	 * @return-loss address formatted as String.
	 */
	public String getLossAddressFormatted() {
		return lossAddressFormatted;
	}

	/**
	 * Method to set loss address formatted.  
	 * @param lossAddressFormatted-the loss address formatted String.
	 */
	public void setLossAddressFormatted(String lossAddressFormatted) {
		this.lossAddressFormatted = lossAddressFormatted;
	}

	/**
	 * Method to get Billing Address Country. 
	 * @return-Billing Address Country as String. 
	 */
	public String getBillingAddressCountry() {
		return billingAddressCountry;
	}

	/**
	 * Method to get Billing Address Country. 
	 * @param billingAddressCountry-the Billing Address Country String. 
	 */
	public void setBillingAddressCountry(String billingAddressCountry) {
		this.billingAddressCountry = billingAddressCountry;
	}

	/**
	 * Method to get Billing Address Latitude. 
	 * @return-billing address latitude as String.
	 */
	public String getBillingAddressLat() {
		return billingAddressLat;
	}

	/**
	 * Method to set Billing Address Latitude. 
	 * @param billingAddressLat-the billing address latitude String.
	 */
	public void setBillingAddressLat(String billingAddressLat) {
		this.billingAddressLat = billingAddressLat;
	}

	/**
	 * Method to get Billing Address Longitude.  
	 * @return-billing address longitude as String.
	 */
	public String getBillingAddressLan() {
		return billingAddressLan;
	}

	/**
	 * Method to set Billing Address Longitude.  
	 * @param billingAddressLan-the billing address longitude String.
	 */
	public void setBillingAddressLan(String billingAddressLan) {
		this.billingAddressLan = billingAddressLan;
	}

	/**
	 * Method to get Billing Address Formatted. 
	 * @return-Billing Address formatted as String.
	 */
	public String getBillingAddressFormatted() {
		return billingAddressFormatted;
	}

	/**
	 * Method to set Billing Address Formatted. 
	 * @param billingAddressFormatted-the Billing Address formatted String. 
	 */
	public void setBillingAddressFormatted(String billingAddressFormatted) {
		this.billingAddressFormatted = billingAddressFormatted;
	}

	/**
	 * Method to get QBO customer failure. 
	 * @return-QBO customer failure as String. 
	 */
	public String getQboCustomerFailure() {
		return qboCustomerFailure;
	}

	/**
	 * Method to set QBO customer failure.
	 * @param qboCustomerFailure-the QBO customer failure String.
	 */
	public void setQboCustomerFailure(String qboCustomerFailure) {
		this.qboCustomerFailure = qboCustomerFailure;
	}

	/**
	 * Method to get encircle perma link. 
	 * @return-encircle perma link as String.
	 */
	public String getEncirclePermalink() {
		return encirclePermalink;
	}

	/**
	 * Method to set encircle perma link.  
	 * @param encirclePermalink-the encircle perma link String. 
	 */
	public void setEncirclePermalink(String encirclePermalink) {
		this.encirclePermalink = encirclePermalink;
	}

	/**
	 * Method to get zapier updated by. 
	 * @return-zapier updated by String.
	 */
	public String getZapierUpdatedBy() {
		return zapierUpdatedBy;
	}

	/**
	 * Method to set zapier updated by.
	 * @param zapierUpdatedBy-the zapier updated by String.
	 */
	public void setZapierUpdatedBy(String zapierUpdatedBy) {
		this.zapierUpdatedBy = zapierUpdatedBy;
	}
}