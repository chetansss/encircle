/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.tenant;

/**
 * Tenant class for context.
 */
public class TenantContext {

	final public static String DEFAULT_TENANT = "common";
	// final public static String DEFAULT_TENANT = "tenant1";

	private static ThreadLocal<String> currentTenant = new ThreadLocal<String>() {
		@Override
		protected String initialValue() {
			return DEFAULT_TENANT;
		}
	};

	/**
	 * Method to set Current tenant.
	 * @param tenant-the tenant String.
	 */
	public static void setCurrentTenant(String tenant) {
		currentTenant.set(tenant);
	}

	/**
	 * Method to get Current tenant.
	 * @return-current tenant as String.
	 */
	public static String getCurrentTenant() {
		return currentTenant.get();
	}

	/**
	 * Method to remove current tenant.
	 */
	public static void clear() {
		currentTenant.remove();
	}
}