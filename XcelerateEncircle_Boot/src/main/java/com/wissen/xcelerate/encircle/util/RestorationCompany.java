/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.util;

import java.io.Serializable;

/**
 * Class for Restoration company.
 */
public class RestorationCompany implements Serializable {
	private static final long serialVersionUID = 1L;
	private int companyId;

	/**
	 * Method to get COmpany ID>
	 * @return-company ID as Integer.
	 */
	public int getCompanyId() {
		return companyId;
	}

	/**
	 * Method to set Company ID.
	 * @param companyId-the company ID Integer.
	 */
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
}