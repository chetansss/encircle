/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model.common;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the common_document_details database table.
 */

@Entity
@Table(name="common_document_details",schema="common")
//@NamedQuery(name="CommonDocumentDetails.findAll", query="SELECT c FROM CommonDocumentDetails c")
public class CommonDocumentDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="document_id")
	private int documentId;

	@Column(name="document_category")
	private String documentCategory;

	@Column(name="comments")
	private String comments;

	@Column(name="document_hash")
	private String documentHash;

	@Column(name="document_name")
	private String documentName;

	@Column(name="document_path")
	private String documentPath;

	@Column(name="document_sub_category")
	private String documentSubCategory;

	@Column(name="internal_file_name")
	private String internalFileName;
	
	@Column(name="display_name")
	private String displayName;
	
	@Column(name="document_type")
	private String documentType;

	@Column(name="is_public")
	private String isPublic;

	@Column(name="source_category")
	private String sourceCategory;
	
	@Column(name="source_sub_category")
	private String sourceSubCategory;

	private String status;

	@Column(name="uploaded_by")
	private int uploadedBy;

	@Column(name="uploaded_datetime")
	private Timestamp uploadedDatetime;
	
	/**
	 * Method to get Document Details.
	 */
	public CommonDocumentDetails() {
	}

	/**
	 * Method to get Document Id.
	 * @return Document Id as integer.
	 */
	public int getDocumentId() {
		return documentId;
	}

	/**
	 * Method to set Document Id.
	 * @param  Document Id-the Document Id is integer
	 */
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	/**
	 * Method to get Document Category.
	 * @return Document Category as string.
	 */
	public String getDocumentCategory() {
		return documentCategory;
	}

	/**
	 * Method to set Document Category.
	 * @param  Document Category-the Document Category is string.
	 */
	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	/**
	 * Method to get Comments.
	 * @return Comments as string.
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Method to set Comments.
	 * @param  Comments-the Comments is string.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Method to get Document Hash.
	 * @return Document Hash as string.
	 */
	public String getDocumentHash() {
		return documentHash;
	}

	/**
	 * Method to set Document Hash.
	 * @param  Document Hash-the Document Hash is string.
	 */
	public void setDocumentHash(String documentHash) {
		this.documentHash = documentHash;
	}

	/**
	 * Method to get Document Name.
	 * @return Document Name as string.
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * Method to set Document Name.
	 * @param  Document Name-the Document Name is string.
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * Method to get Document Path.
	 * @return Document Path as string.
	 */
	public String getDocumentPath() {
		return documentPath;
	}

	/**
	 * Method to set Document Path.
	 * @param  Document Path-the Document Path is string.
	 */
	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	/**
	 * Method to get Document SubCategory.
	 * @return Document SubCategory as string.
	 */
	public String getDocumentSubCategory() {
		return documentSubCategory;
	}

	/**
	 * Method to set Document SubCategory.
	 * @param  Document SubCategory-the Document SubCategory is string.
	 */
	public void setDocumentSubCategory(String documentSubCategory) {
		this.documentSubCategory = documentSubCategory;
	}

	/**
	 * Method to get Internal FileName.
	 * @return Internal FileName as string.
	 */
	public String getInternalFileName() {
		return internalFileName;
	}

	/**
	 * Method to set Internal FileName.
	 * @param  Internal FileName-the Internal FileName is string.
	 */
	public void setInternalFileName(String internalFileName) {
		this.internalFileName = internalFileName;
	}

	/**
	 * Method to get Display Name.
	 * @return Display Name as string.
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Method to set Display Name.
	 * @param  Display Name-the Display Name is string.
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Method to get Document Type.
	 * @return Document Type as string.
	 */
	public String getDocumentType() {
		return documentType;
	}

	/**
	 * Method to set Document Type.
	 * @param  Document Type-the Document Type is string.
	 */
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	/**
	 * Method to get Is Public.
	 * @return Is Public as string.
	 */
	public String getIsPublic() {
		return isPublic;
	}

	/**
	 * Method to set Is Public.
	 * @param  Is Public-the Is Public is string.
	 */
	public void setIsPublic(String isPublic) {
		this.isPublic = isPublic;
	}

	/**
	 * Method to get Source Category.
	 * @return Source Category as string.
	 */
	public String getSourceCategory() {
		return sourceCategory;
	}

	/**
	 * Method to set Source Category.
	 * @param  Source Category-the Source Category is string.
	 */
	public void setSourceCategory(String sourceCategory) {
		this.sourceCategory = sourceCategory;
	}

	/**
	 * Method to get Source SubCategory.
	 * @return Source SubCategory as string.
	 */
	public String getSourceSubCategory() {
		return sourceSubCategory;
	}

	/**
	 * Method to set Source SubCategory.
	 * @param  Source SubCategory-the Source SubCategory is string.
	 */
	public void setSourceSubCategory(String sourceSubCategory) {
		this.sourceSubCategory = sourceSubCategory;
	}

	/**
	 * Method to get Status.
	 * @return Status as string.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Method to set Status.
	 * @param  Status-the Status is string.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Method to get Uploaded By.
	 * @return Uploaded By as integer.
	 */
	public int getUploadedBy() {
		return uploadedBy;
	}

	/**
	 * Method to set Uploaded By.
	 * @param  Uploaded By-the Uploaded By is integer.
	 */
	public void setUploadedBy(int uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	/**
	 * Method to get Uploaded Datetime.
	 * @return Uploaded Datetime as timestamp.
	 */

	public Timestamp getUploadedDatetime() {
		return uploadedDatetime;
	}

	/**
	 * Method to set Uploaded Datetime.
	 * @param  Uploaded Datetime-the Uploaded Datetime is timestamp.
	 */
	public void setUploadedDatetime(Timestamp uploadedDatetime) {
		this.uploadedDatetime = uploadedDatetime;
	}
}
