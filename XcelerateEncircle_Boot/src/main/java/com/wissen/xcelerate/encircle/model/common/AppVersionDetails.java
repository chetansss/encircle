/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model.common;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the app_version_details database table.
 */
@Entity
@Table(name="app_version_details",schema="common")
//@NamedQuery(name="AppVersionDetails.findAll", query="SELECT a FROM AppVersionDetails a")
public class AppVersionDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="app_version_id")
	private int appVersionIdd;

	@Column(name="app_version")
	private String appVersion;

	private String status;
	
	@Column(name="updated_by")
	private Integer updatedBy;

	@Column(name="updated_datetime")
	private Date updatedDatetime;
	
	/**
	 * Method to get App VersionIdd.
	 * @return App VersionIdd as integer.
	 */
	public int getAppVersionIdd() {
		return appVersionIdd;
	}

	/**
	 * Method to set App VersionIdd.
	 * @param  App VersionIdd-the App VersionIdd is integer
	 */
	public void setAppVersionIdd(int appVersionIdd) {
		this.appVersionIdd = appVersionIdd;
	}

	/**
	 * Method to get App Version.
	 * @return App Version as string
	 */
	public String getAppVersion() {
		return appVersion;
	}

	/**
	 * Method to set App Version.
	 * @param  App Version-the App Version is string.
	 */
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	/**
	 * Method to get Status.
	 * @return Status as string
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Method to set Status.
	 * @param Status-the Status is string.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Method to get Updated By.
	 * @return Updated By as integer.
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set Updated By.
	 * @param Updated By-the Updated By is integer.
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get Updated Datetime.
	 * @return Updated Datetime as date.
	 */
	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set updated Datetime.
	 * @param updated Datetime-the updated Datetime is date.
	 */
	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}
}
