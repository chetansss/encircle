/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * Persistent class for Job KPI Targers
 */
@Entity
@Table(name = "job_kpi_targets")
public class JobKpiTargets implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "job_kpi_target_id")
	private int jobKpiTargetId;

	@Column(name = "kpi_id")
	private int kpiId;

	@Column(name = "kpi_name")
	private String kpiName;

	@Column(name = "water")
	private Float water;

	@Column(name = "recon")
	private Float recon;

	@Column(name = "content")
	private Float content;

	@Column(name = "mold")
	private Float mold;

	@Column(name = "asbestos")
	private Float asbestos;

	@Column(name = "board_up")
	private Float boardUp;

	@Column(name = "consulting")
	private Float consulting;

	@Column(name = "bio")
	private Float bio;

	@Column(name = "structure_cleaning")
	private Float structureCleaning;

	@Column(name = "others")
	private Float others;

	@Column(name = "roofing")
	private Float roofing;

	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "created_datetime")
	private Timestamp createdDatetime;

	@Column(name = "updated_by")
	private Integer updatedBy;

	@Column(name = "updated_datetime")
	private Timestamp updatedDatetime;

	@Column(name = "source")
	private String source;
	private String uom;

	/**
	 * Method to get job KPI target ID.
	 * @return-job KPI target ID as Integer.
	 */
	public int getJobKpiTargetId() {
		return jobKpiTargetId;
	}

	/**
	 * Method to set job KPI target ID. 
	 * @param jobKpiTargetId-the job KPI target ID Integer.
	 */
	public void setJobKpiTargetId(int jobKpiTargetId) {
		this.jobKpiTargetId = jobKpiTargetId;
	}

	/**
	 * Method to get KPI ID. 
	 * @return-KPI ID as Integer. 
	 */
	public int getKpiId() {
		return kpiId;
	}

	/**
	 * Method to set KPI ID. 
	 * @param kpiId-the KPI ID Integer. 
	 */
	public void setKpiId(int kpiId) {
		this.kpiId = kpiId;
	}

	/**
	 * Method to get KPI name. 
	 * @return-kpi name as String.
	 */
	public String getKpiName() {
		return kpiName;
	}

	/**
	 * Method to set KPI name. 
	 * @param kpiName-the kpi name String.
	 */
	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	/**
	 * Method to get water.
	 * @return-water as Float.
	 */
	public Float getWater() {
		return water;
	}

	/**
	 * Method to set water. 
	 * @param water-the water Float.
	 */
	public void setWater(Float water) {
		this.water = water;
	}

	/**
	 * Method to get reconstruction.
	 * @return-reconstruction as Float.
	 */
	public Float getRecon() {
		return recon;
	}

	/**
	 * Method to set reconstruction. 
	 * @param recon-the reconstruction Float.
	 */
	public void setRecon(Float recon) {
		this.recon = recon;
	}

	/**
	 * Method to get Content.
	 * @return-content as Float.
	 */
	public Float getContent() {
		return content;
	}

	/**
	 * Method to set Content.
	 * @param content-the content Float.
	 */
	public void setContent(Float content) {
		this.content = content;
	}

	/**
	 * Method to get Mo ID..
	 * @return-Mo ID as Float.
	 */
	public Float getMold() {
		return mold;
	}

	/**
	 * Method to set Mo ID.
	 * @param mold-the Mo ID Float.
	 */
	public void setMold(Float mold) {
		this.mold = mold;
	}

	/**
	 * Method to get Adbestos. 
	 * @return-asbestos as Float.
	 */
	public Float getAsbestos() {
		return asbestos;
	}

	/**
	 * Method to set Astestos.
	 * @param mold-the Asbestos Float.
	 */
	public void setAsbestos(Float asbestos) {
		this.asbestos = asbestos;
	}

	/**
	 * Method to get Board up. 
	 * @return-board up as Float.
	 */
	public Float getBoardUp() {
		return boardUp;
	}

	/**
	 * Method to set board up.
	 * @param boardUp-the board up Float.
	 */
	public void setBoardUp(Float boardUp) {
		this.boardUp = boardUp;
	}

	/**
	 * Method to get consulting.
	 * @return-consulting as Float.
	 */
	public Float getConsulting() {
		return consulting;
	}

	/**
	 * Method to set Consulting.
	 * @param consulting-the consulting Float.
	 */
	public void setConsulting(Float consulting) {
		this.consulting = consulting;
	}

	/**
	 * Method to get Bio.
	 * @return-bio as Float.
	 */
	public Float getBio() {
		return bio;
	}

	/**
	 * Method to set bio.
	 * @param bio-the bio Float.
	 */
	public void setBio(Float bio) {
		this.bio = bio;
	}

	/**
	 * Method to get structure cleaning.
	 * @return-structure cleaning as Float.
	 */
	public Float getStructureCleaning() {
		return structureCleaning;
	}

	/**
	 * Method to set structure cleaning. 
	 * @param structureCleaning-the structure cleaning Float.
	 */
	public void setStructureCleaning(Float structureCleaning) {
		this.structureCleaning = structureCleaning;
	}

	/**
	 * Method to get others.
	 * @return-others as FLoat.
	 */
	public Float getOthers() {
		return others;
	}

	/**
	 * Method to set others.
	 * @param others-the others Float.
	 */
	public void setOthers(Float others) {
		this.others = others;
	}

	/**
	 * Method to get credited by.
	 * @return-credited by as Integer.
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * Method to set credited by.
	 * @param createdBy-the credited by Integer.
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Method to get Created Date time.
	 * @return-Created Date time as Timestamp.
	 */
	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}

	/**
	 * Method to set Created Date time. 
	 * @param createdDatetime-the Created Date time Timestamp.
	 */
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	/**
	 * Method to get updated by.
	 * @return-updated by as Integer.
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set updated by.
	 * @param updatedBy-the updated by Integer.
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get Updated Date time.
	 * @return-Updated Date time as Timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set Updated Date time. 
	 * @param updatedDatetime-the Updated Date time Timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get Source.
	 * @return-source as String.
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Method to set Source.
	 * @param source-the source String.
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Method to get uom.
	 * @return-uom as String.
	 */
	public String getUom() {
		return uom;
	}

	/**
	 * Method to set uom.
	 * @param source-the uom String.
	 */
	public void setUom(String uom) {
		this.uom = uom;
	}

	/**
	 * Method to get roofing.
	 * @return-roofing as Integer.
	 */
	public Float getRoofing() {
		return roofing;
	}

	/**
	 * Method to set roofing.
	 * @return- the roofing FLoat.
	 */
	public void setRoofing(Float roofing) {
		this.roofing = roofing;
	}
}
