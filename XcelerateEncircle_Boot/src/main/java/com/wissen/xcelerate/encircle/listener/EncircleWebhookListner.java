/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.wissen.xcelerate.encircle.pojo.MediaWebhook;
import com.wissen.xcelerate.encircle.service.JobDetailsServiceImpl;

/**
 * Class for Encircle webhook listener.
 */
@Component
public class EncircleWebhookListner {
	private static final Logger LOGGER = LogManager.getLogger(EncircleWebhookListner.class);
	@Autowired
	private JobDetailsServiceImpl service;

	/**
	 * Method to send message
	 * @param message-the message Byte.
	 */
	public void send(byte[] message) {
		System.out.println("********* Encircle Webhook Listner is Listning **********");
		LOGGER.info("********* Encircle Webhook Listner is Listning **********");
		String msg = new String(message);
		MediaWebhook wehhook = new Gson().fromJson(msg, MediaWebhook.class);
		System.out.println("Encircle Webhook Listner Queue Payload======>" + msg);
		LOGGER.info("Encircle Webhook Listner Queue Payload======>" + msg);
		if (!wehhook.getTenant().equals("common")) {
			service.getPicturesFromEncircle(wehhook);
		}
	}
}