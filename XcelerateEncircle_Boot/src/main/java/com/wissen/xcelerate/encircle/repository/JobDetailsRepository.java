/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.wissen.xcelerate.encircle.model.JobDetails;

/**
 * Repository class for find By Encircle ClaimId,save,find By JobId.
 */
@org.springframework.stereotype.Repository
public interface JobDetailsRepository extends Repository<JobDetails, Long>{
	

    
	/*
	 * @Query(
	 * value="select new JobDetails(j.jobId,j.jobCode) from JobDetails j
	 *  where j.currentJobStatus NOT IN ('Closed','Rejected') and j.createdDatetime>:
	 *  createdDatetime order by j.createdDatetime desc"
	 * ) List<JobDetails> findAllJobs(@Param("createdDatetime") Date
	 * createdDatetime);
	 */
	
	@Query(value="select j from JobDetails j  where j.encircleClaimId=:encircleClaimId") 
	JobDetails findByEncircleClaimId(@Param("encircleClaimId") Integer encircleClaimId);
	
	JobDetails save(JobDetails job); 
	
	JobDetails findByJobId(@Param("jobId") int jobId);
	
	}
