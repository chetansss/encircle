/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Class for Util Date.
 */
public class DateUtil {

	/**
	 * Method to get Current time. 
	 * @param timezone-the timezone String.
	 * @return-current time as Date.
	 */
	public Date getCurrentTime(String timezone) {
		Date currentTime = null;
		try {
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone(timezone));
			currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(df.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return currentTime;
	}

	/**
	 * Method to get Current date.
	 * @param timezone-the timezone String.
	 * @return-current time as Date.
	 */
	public Date getCurrentDate(String timezone) {
		Date currentTime = null;
		try {
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			df.setTimeZone(TimeZone.getTimeZone(timezone));
			currentTime = new SimpleDateFormat("yyyy-MM-dd").parse(df.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return currentTime;
	}

	/**
	 * Method to get Date from time stamp.
	 * @param time-the time Timestamp.
	 * @return-dateval as Date.
	 */
	public Date getDateFromTimestamp(Timestamp time) {
		Date dateVal = null;
		try {
			Date date = new Date(time.getTime());
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			dateVal = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(df.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateVal;

	}

	/**
	 * Method to set Data format.
	 * @param date-the     date Date.
	 * @param timezone-the Timezone String.
	 * @return-date as Dateformat.
	 * @throws Exception
	 */
	public static String DateFormat(Date date, String timezone) throws Exception {
		try {
			DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
			formatter.setTimeZone(TimeZone.getTimeZone(timezone));
			String dat = formatter.format(date);
			return dat;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Method to get duration.
	 * @param startDate-the start date Date.
	 * @param endDate-the   end date Date.
	 * @param uom-the       uom String.
	 * @return-duration as Float.
	 */
	public Float getDuration(Date startDate, Date endDate, String uom) {
		Float duration = null;
		long diff = endDate.getTime() - startDate.getTime();
		if (uom.equalsIgnoreCase("Minutes")) {
			duration = (float) diff / (60 * 1000);
		} else if (uom.equalsIgnoreCase("Hours")) {
			duration = (float) diff / (60 * 60 * 1000);
		} else if (uom.equalsIgnoreCase("Days")) {
			duration = (float) diff / (1000 * 60 * 60 * 24);
		}
		return duration;
	}

	/**
	 * Method to get Activity duration.
	 * @param startDate-the start date Date.
	 * @param endDate-the   end date Date.
	 * @param uom-the       uom String.
	 * @return-duration as Integer.
	 */
	public Integer getActivityDuration(Date startDate, Date endDate, String uom) {
		Float duration = null;
		long diff = endDate.getTime() - startDate.getTime();
		if (uom.equalsIgnoreCase("Minutes")) {
			duration = (float) diff / (60 * 1000);
		} else if (uom.equalsIgnoreCase("Hours")) {
			duration = (float) diff / (60 * 60 * 1000);
		} else if (uom.equalsIgnoreCase("Days")) {
			duration = (float) diff / (1000 * 60 * 60 * 24);
		}
		return Math.round(duration);
	}

	/**
	 * Method to get work duration.
	 * @param startDate-the start date Date.
	 * @param endDate-the   end date Date.
	 * @param uom-the       uom String.
	 * @return-work duration as Float.
	 */
	public Float getWorkDuration(Date startDate, Date endDate, String uom) {
		Float duration = null;
		long diff = endDate.getTime() - startDate.getTime();
		if (uom.equalsIgnoreCase("Minutes")) {
			duration = (float) diff / (60 * 1000);
		} else if (uom.equalsIgnoreCase("Hours")) {
			duration = (float) diff / (60 * 60 * 1000);
		} else if (uom.equalsIgnoreCase("Days")) {
			duration = (float) diff / (1000 * 60 * 60 * 24);
		}
		return duration;
	}

	/**
	 * Method to get Year.
	 * @param timezone-the time zone String.
	 * @return-year as String.
	 */
	public static int getYear(String timezone) {
		int lastDigits = 20;
		try {
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yy");
			df.setTimeZone(TimeZone.getTimeZone(timezone));
			Date currentTime = new SimpleDateFormat("yy").parse(df.format(date));
			System.out.println("Year: " + currentTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lastDigits;
	}

	/**
	 * Method to get Current date format.
	 * @param timezone-the timezone String.
	 * @return-current time as String.
	 */
	public Date getCurrentDateFormat(String timezone) {
		Date currentTime = null;
		try {
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			df.setTimeZone(TimeZone.getTimeZone(timezone));
			currentTime = new SimpleDateFormat("MM/dd/yyyy").parse(df.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return currentTime;
	}

	/**
	 * Method to format date.
	 * @param dbDate-the dbdate String.
	 * @return-parse date as Date.
	 */
	public Date formatDate(String dbDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date parseDate = null;
		try {
			parseDate = sdf.parse(dbDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return parseDate;
	}

	/**
	 * Method to compare dates.
	 * @param dbDate-the  DB date String.
	 * @param appDate-the app date String.
	 * @return-dateval as Boolean.
	 */
	public boolean compareDates(String dbDate, String appDate) {
		boolean dateVal = false;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date1;
		try {
			date1 = sdf.parse(dbDate);
			Date date2 = sdf.parse(appDate);
			if (date1.compareTo(date2) == 0) {
				dateVal = true;
			} else {
				dateVal = false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateVal;
	}

	/**
	 * Method to get end date.
	 * @param clockInTime-the clock in Date.
	 * @param duration-the    duration Float.
	 * @return-end date as Date.
	 */
	public Date getEndDate(Date clockInTime, float duration) {
		BigDecimal bd = new BigDecimal(duration).setScale(2, RoundingMode.HALF_EVEN);
		String time = String.valueOf(bd);
		long millis = 0;
		millis = (long) (Float.parseFloat(time) * 3600000);
		Date endDate = new Date(clockInTime.getTime() + millis);
		return endDate;
	}
}
