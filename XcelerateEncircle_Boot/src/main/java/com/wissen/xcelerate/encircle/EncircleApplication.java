package com.wissen.xcelerate.encircle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class for Encircle Application.
 */
@SpringBootApplication
public class EncircleApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncircleApplication.class, args);
	}

}
