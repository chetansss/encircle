package com.wissen.xcelerate.encircle.pojo;

import java.io.Serializable;

/**
 * class for job update.
 */
public class JobUpdate implements Serializable {

	private static final long serialVersionUID = 1L;
	private int restCompId;
	private Customer parent;
	private Customer jobDetails;
	
	/**
	 * Method for job update.
	 */
	public JobUpdate() {}

	
	/**
	 * Method to get Parent.
	 * @return Parent as customer.
	 */
	public Customer getParent() {
		return parent;
	}

	/**
	 * Method to set Parent.
	 * @param  Parent-the Parent is customer.
	 */
	public void setParent(Customer parent) {
		this.parent = parent;
	}

	/**
	 * Method to get Job Details.
	 * @return Job Details as customer.
	 */
	public Customer getJobDetails() {
		return jobDetails;
	}

	/**
	 * Method to set job Details.
	 * @param  job Details-the job Details is customer.
	 */
	public void setJobDetails(Customer jobDetails) {
		this.jobDetails = jobDetails;
	}

	/**
	 * Method to get Rest CompId.
	 * @return Rest CompId as integer.
	 */
	public int getRestCompId() {
		return restCompId;
	}

	/**
	 * Method to set Rest CompId.
	 * @param  Rest CompId-the Rest CompId is integer.
	 */
	public void setRestCompId(int restCompId) {
		this.restCompId = restCompId;
	}
}
