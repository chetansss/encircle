/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wissen.xcelerate.encircle.model.CommonFileDetails;
import com.wissen.xcelerate.encircle.model.CommonMilestones;
import com.wissen.xcelerate.encircle.model.CommonTasks;
import com.wissen.xcelerate.encircle.model.JobDetails;
import com.wissen.xcelerate.encircle.model.JobKpiDetails;
import com.wissen.xcelerate.encircle.model.JobKpiTargets;
import com.wissen.xcelerate.encircle.model.RestorationCompanyDetails;
import com.wissen.xcelerate.encircle.model.UserDetails;
import com.wissen.xcelerate.encircle.pojo.MediaWebhook;
import com.wissen.xcelerate.encircle.pojo.StatusMessage;
import com.wissen.xcelerate.encircle.repository.CommonFileDetailsRepository;
import com.wissen.xcelerate.encircle.repository.CommonMilestonesRepository;
import com.wissen.xcelerate.encircle.repository.CommonTasksRepository;
import com.wissen.xcelerate.encircle.repository.JobDetailsRepository;
import com.wissen.xcelerate.encircle.repository.JobKpiDetailsRepository;
import com.wissen.xcelerate.encircle.repository.JobKpiTargetsRepository;
import com.wissen.xcelerate.encircle.repository.RestorationCompanySchemaRepository;
import com.wissen.xcelerate.encircle.repository.UserRepository;
import com.wissen.xcelerate.encircle.tenant.TenantContext;
import com.wissen.xcelerate.encircle.util.DateUtil;
import com.wissen.xcelerate.encircle.util.GenerateHashcode;
import com.wissen.xcelerate.encircle.util.JobUtil;
import com.wissen.xcelerate.encircle.util.XcelerateConstants;

/**
 * Service class for Job details.
 */
@Service
public class JobDetailsServiceImpl {
	private static final Logger LOGGER = LogManager.getLogger(JobDetailsServiceImpl.class);
	@Autowired
	private JobDetailsRepository repository;
	@Autowired
	private CommonFileDetailsRepository fileRepository;
	@Autowired
	private CommonMilestonesRepository milestonesRepository;
	@Autowired
	private CommonTasksRepository tasksRepository;
	@Autowired
	private JobKpiTargetsRepository jobKpiTargetsRepository;
	@Autowired
	private JobKpiDetailsRepository kpiDetailsRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RestorationCompanyServiceImpl service;
	@Autowired
	private RestorationCompanySchemaRepository companyRepository;
	@Autowired
	private QBOCustomerServiceImpl qboCustomerService;

	/**
	 * Method to get Pictures from encircle.
	 * @param webhook-the webhook Media web hook.
	 * @return-status message.
	 */
	public StatusMessage getPicturesFromEncircle(MediaWebhook webhook) {
		LOGGER.info("*********  GetPicturesFromEncircle work flow is Started  ***********");
		StatusMessage statusMessage = new StatusMessage();
		int userId = 1;
		try {
			TenantContext.setCurrentTenant(webhook.getTenant());
			int propertyClaimId = Integer.parseInt(webhook.getPropertyClaimId());
			LOGGER.info("Claim Id ====>" + propertyClaimId);
			if (propertyClaimId > 0 && !webhook.getTenant().equals("common")) {
				int company = service.getRestorationCompanyId();
				System.out.println("company Id ====>" + company);
				RestorationCompanyDetails companyDetails = companyRepository.findByRestCompId(company);
				JobDetails jobDetails = repository.findByEncircleClaimId(propertyClaimId);
				System.out
						.println(" Job Id  & Job Code  ====>" + jobDetails.getJobId() + "  " + jobDetails.getJobCode());
				if (jobDetails != null && companyDetails != null) {
					if (companyDetails.getEncircleApiToken() != null) {
						validateAndInsertEncircleFiles(jobDetails, webhook, userId, company);
					}
				}
				statusMessage.setMessage(XcelerateConstants.SUCCESS);
			} else {
				statusMessage.setStatus(XcelerateConstants.FAILURE);
				statusMessage.setMessage(propertyClaimId + " Property ClaimId is Not Present in Xcelerate");
			}
		} catch (Exception e) {
			statusMessage.setStatus(XcelerateConstants.FAILURE);
			statusMessage.setMessage(e.getMessage());
			e.printStackTrace();
		}
		return statusMessage;
	}

	/**
	 * Method to validate and insert encircle files.
	 * 
	 * @param jobDetails-the job details.
	 * @param webhook-the    webhook.
	 * @param userId-the     user ID Integer.
	 * @param company-the    company Integer. @return-message.
	 */
	private StatusMessage validateAndInsertEncircleFiles(JobDetails jobDetails, MediaWebhook webhook, int userId,
			int company) {
		TenantContext.setCurrentTenant(webhook.getTenant());
		LOGGER.info("*********  validateAndInsertEncircleFiles is Started  ***********");
		LOGGER.info(
				"Current tenant is in validateAndInsertEncircleFiles method ===>" + TenantContext.getCurrentTenant());
		String type = "";
		CommonFileDetails commonFileDetails = null;
		StatusMessage message = new StatusMessage();
		try {
			commonFileDetails = fileRepository.findEncircleUrlDetails(XcelerateConstants.JOB, jobDetails.getJobId(),
					webhook.getFilename(), "EncircleUrl");
			if (commonFileDetails == null) {
				commonFileDetails = new CommonFileDetails();
				LOGGER.info(" no common file exits with job id" + jobDetails.getJobId());
			}
			commonFileDetails.setDisplayName(webhook.getFilename());
			commonFileDetails.setFilePath(webhook.getDownloadUri());
			commonFileDetails.setFileHash(String.valueOf(GenerateHashcode.gerenateHashCode()));
			commonFileDetails.setFileName(webhook.getFilename());
			commonFileDetails.setFileComments("Uploaded from Encircle App");
			commonFileDetails.setInternalFileName(webhook.getFilename());
			if (webhook.getContentType().indexOf("image") >= 0) {
				commonFileDetails.setFileType(XcelerateConstants.FILE_TYPE_PICTURE);
				type = "Picture";
				LOGGER.info("type is Picture");
			} else {
				commonFileDetails.setFileType(XcelerateConstants.FILE_TYPE_DOCUMENT);
				// setting document type
				if (webhook.getFilename().contains("Work Authorization")
						|| webhook.getFilename().contains("Work_Authorization")) {
					commonFileDetails.setFileCategory("Work Authorization");
					LOGGER.info("Work Authorization");
				} else if (webhook.getFilename().contains("Certificate of Completion")
						|| webhook.getFilename().contains("Certificate_of_Completion")
						|| webhook.getFilename().contains("Certificate of Satisfaction")
						|| webhook.getFilename().contains("Certificate_of_Satisfaction")) {
					commonFileDetails.setFileCategory("COC");
					LOGGER.info("type is COC");
				} else {
					commonFileDetails.setFileCategory("Encircle Document");
				}
				type = "Document";
				LOGGER.info("type is Documen");
			}
			commonFileDetails.setSourceCategory(XcelerateConstants.JOB);
			LOGGER.info("Job===>" + XcelerateConstants.JOB);
			commonFileDetails.setSourceId(jobDetails.getJobId());
			LOGGER.info("Job Id ===>" + jobDetails.getJobId());
			commonFileDetails.setFolderId(1);
			LOGGER.info("Folder Id ===>" + 1);
			commonFileDetails.setUploadedDatetime(new java.sql.Timestamp(
					new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
			LOGGER.info("Time Stamp ===>" + commonFileDetails.getUploadedDatetime().toString());
			commonFileDetails.setUploadedBy(userId);
			LOGGER.info("userId Id ===>" + userId);
			commonFileDetails.setStatus(XcelerateConstants.STATUS_ACTIVE);
			commonFileDetails.setSource("EncircleUrl");
			commonFileDetails = fileRepository.save(commonFileDetails);
			LOGGER.info("Saved File ID ======>" + commonFileDetails.getFileId());
			LOGGER.info("*********  Common File Details Saved Sucessful ***********");
			if (type.equalsIgnoreCase("Document")) {
				updateStatusAndMilestones(jobDetails, userId, commonFileDetails, company, webhook);
				UserDetails user = userRepository.findByUserId(userId);
				updateTasks(jobDetails, user, commonFileDetails, webhook);
				calculateJobKpis(jobDetails, userId, webhook);
			}
			message.setStatus(XcelerateConstants.SUCCESS);
			message.setMessage("Common File Details Saved Sucessful");

		} catch (Exception e) {
			message.setStatus(XcelerateConstants.FAILURE);
			message.setMessage(e.getMessage());
		}
		return message;
	}

	/**
	 * Method tpo update Status and Milestones.
	 * @param jobDetails-the  job details.
	 * @param userId-the      user ID Integer.
	 * @param fileDetails-the file details common file details.
	 * @param companyId-the   company ID Integer.
	 * @param webhook-the     webhook Media webhook.
	 */
	private void updateStatusAndMilestones(JobDetails jobDetails, int userId, CommonFileDetails fileDetails,
			int companyId, MediaWebhook webhook) {
		TenantContext.setCurrentTenant(webhook.getTenant());
		LOGGER.info("*********  Updating milestones is Started  ***********");
		CommonMilestones milestones = milestonesRepository.findBySourceId(fileDetails.getSourceId());
		if (fileDetails.getFileCategory().equalsIgnoreCase("Work Authorization")) {
			if (milestones != null && milestones.getDateofWorkAuthorization() == null) {
				milestones.setDateofWorkAuthorization(new java.sql.Timestamp(
						new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
				milestones.setUpdatedDatetime(new java.sql.Timestamp(
						new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
				milestones.setUpdatedBy(userId);
				milestonesRepository.save(milestones);
				qboCustomerService.createQboCustomer(milestones.getSourceId(), companyId);
			}
			if (jobDetails.getJobType().equalsIgnoreCase("Water") || jobDetails.getJobType().equalsIgnoreCase("Bio")
					|| jobDetails.getJobType().equalsIgnoreCase("Board Up")) {
				if (!jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_PRODUCTION)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_ESTIMATING_FINAL)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_INVOICING)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_RECEIVABLES)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_PAID)) {
					jobDetails.setCurrentJobStatus(XcelerateConstants.JOB_STATUS_PLANNING);
				}
			} else {
				if (!jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_PAID)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_RECEIVABLES)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_ESTIMATING_FINAL)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_INVOICING)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_PRODUCTION)) {
					jobDetails.setCurrentJobStatus(XcelerateConstants.JOB_STATUS_PLANNING);
				}
			}
		} else if (fileDetails.getFileCategory().equalsIgnoreCase("COC")) {
			if (milestones != null && milestones.getDateOfcOs() == null) {
				milestones.setDateOfcOs(new java.sql.Timestamp(
						new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
				milestones.setUpdatedDatetime(new java.sql.Timestamp(
						new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
				milestones.setUpdatedBy(userId);
				milestonesRepository.save(milestones);
			}
			if (jobDetails.getJobType().equalsIgnoreCase("Water") || jobDetails.getJobType().equalsIgnoreCase("Bio")
					|| jobDetails.getJobType().equalsIgnoreCase("Board Up")) {
				if (!jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_RECEIVABLES)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_PAID)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_INVOICING)) {
					jobDetails.setCurrentJobStatus(XcelerateConstants.JOB_STATUS_ESTIMATING_FINAL);
				}
			} else {
				if (!jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_PAID)
						&& !jobDetails.getCurrentJobStatus().equals(XcelerateConstants.JOB_STATUS_RECEIVABLES)) {
					jobDetails.setCurrentJobStatus(XcelerateConstants.JOB_STATUS_INVOICING);
				}
			}
		}
		jobDetails.setLastActivity(
				new java.sql.Timestamp(new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
		jobDetails.setUpdatedDatetime(
				new java.sql.Timestamp(new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
		jobDetails.setUpdatedBy(userId);
		repository.save(jobDetails);
		LOGGER.info("*********  Updating milestones is Sucessful  ***********");
	}

	/**
	 * Method to update Tasks.
	 * @param jobDetails-the  job details.
	 * @param user-the        user User details.
	 * @param fileDetails-the file details Common file details.
	 * @param webhook-the     webhook Media Webhook.
	 */
	private void updateTasks(JobDetails jobDetails, UserDetails user, CommonFileDetails fileDetails,
			MediaWebhook webhook) {
		// updating open tasks
		TenantContext.setCurrentTenant(webhook.getTenant());
		LOGGER.info("*********  Updating task  are  Started  ***********");
		Map<String, String> docCategory = new JobUtil().getDocumentCategory();
		Map<String, String> taskActions = new JobUtil().getTaskAction();
		List<CommonTasks> commonTasks = tasksRepository.findBySourceAndSourceIdAndActionRequiredAndStatus("Job",
				jobDetails.getJobId(), taskActions.get(fileDetails.getFileCategory()),
				XcelerateConstants.TASK_STATUS_OPEN);
		if (commonTasks != null) {
			java.sql.Timestamp currentDate = new java.sql.Timestamp(
					new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime());
			for (CommonTasks commonTask : commonTasks) {
				commonTask.setStatus("Completed");
				commonTask.setAssignType("User");
				commonTask.setAssignTo(user.getUserId());
				commonTask.setUpdatedBy(user);
				if (user != null && user.getTimeZone() != null) {
					commonTask.setUpdatedDatetime(
							new java.sql.Timestamp(new DateUtil().getCurrentTime(user.getTimeZone()).getTime()));
				} else {
					commonTask.setUpdatedDatetime(new java.sql.Timestamp(
							new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
				}
				commonTask.setActualEndDatetime(currentDate);
				tasksRepository.save(commonTask);
				CommonTasks task = tasksRepository.findBySourceIdAndTaskSequence(commonTask.getSourceId(),
						commonTask.getTaskSequence() + 1);
				if (task != null) {
					if (task.getExpectedStartDatetime() == null) {
						task.setExpectedStartDatetime(commonTask.getActualEndDatetime());
						Date dueDate = getDueDate(commonTask.getActualEndDatetime(), task.getTimeToComplete(),
								task.getTimeUom());
						task.setExpectedEndDatetime(new java.sql.Timestamp(dueDate.getTime()));
					}
					if (task.getTriggerEvent().equalsIgnoreCase("Previous Task")
							|| task.getStatus().equals("Completed")) {
						List<CommonFileDetails> documents = fileRepository.findBySourceCategoryAndSourceId("Job",
								task.getSourceId());
						if (!task.getStatus().equals("Completed")) {

							if (task.getActionRequired().equals("Upload Estimate")
									|| task.getActionRequired().equals("Upload WO")
									|| task.getActionRequired().equals("Upload Invoice")
									|| task.getActionRequired().equals("Upload of COC")) {
								if (documents != null && documents.size() > 0) {
									boolean docExists = false;
									for (CommonFileDetails jobDocument : documents) {
										if (jobDocument.getFileCategory()
												.equals(docCategory.get(task.getActionRequired()))) {
											docExists = true;
											break;
										}
									}
									if (docExists) {
										task.setActualEndDatetime(currentDate);
										task.setStatus("Completed");
										task.setAssignType("User");
										task.setAssignTo(user.getUserId());
										task.setUpdatedBy(user);
										if (user != null && user.getTimeZone() != null) {
											task.setUpdatedDatetime(new java.sql.Timestamp(
													new DateUtil().getCurrentTime(user.getTimeZone()).getTime()));
										} else {
											task.setUpdatedDatetime(new java.sql.Timestamp(new DateUtil()
													.getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
										}
									} else {
										task.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
										task.setExpectedStartDatetime(commonTask.getActualEndDatetime());
										Date dueDate = getDueDate(commonTask.getActualEndDatetime(),
												task.getTimeToComplete(), task.getTimeUom());
										task.setExpectedEndDatetime(new java.sql.Timestamp(dueDate.getTime()));
									}
								} else {
									task.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
									task.setExpectedStartDatetime(commonTask.getActualEndDatetime());
									Date dueDate = getDueDate(commonTask.getActualEndDatetime(),
											task.getTimeToComplete(), task.getTimeUom());
									task.setExpectedEndDatetime(new java.sql.Timestamp(dueDate.getTime()));
								}
							} else {
								task.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
								task.setExpectedStartDatetime(commonTask.getActualEndDatetime());
								Date dueDate = getDueDate(commonTask.getActualEndDatetime(), task.getTimeToComplete(),
										task.getTimeUom());
								task.setExpectedEndDatetime(new java.sql.Timestamp(dueDate.getTime()));
							}
							task = tasksRepository.save(task);
						}
						List<CommonTasks> parallelTasks = tasksRepository
								.findBySourceIdAndTaskSequenceIsGreaterThanOrderByTaskSequence(task.getSourceId(),
										task.getTaskSequence());
						if (parallelTasks != null) {
							if (task.getStatus().equals("Open")) {
								for (CommonTasks parallelTask : parallelTasks) {
									if (parallelTask.getTriggerEvent().equalsIgnoreCase("Simultaneously")) {

										// parallelTask.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
										if (parallelTask.getActionRequired().equals("Upload Estimate")
												|| parallelTask.getActionRequired().equals("Upload WO")
												|| parallelTask.getActionRequired().equals("Upload Invoice")
												|| parallelTask.getActionRequired().equals("Upload of COC")) {
											if (documents != null && documents.size() > 0) {
												boolean docExists = false;
												for (CommonFileDetails jobDocument : documents) {
													if (jobDocument.getFileCategory().equals(
															docCategory.get(parallelTask.getActionRequired()))) {
														docExists = true;
														break;
													}
												}
												if (docExists) {
													parallelTask
															.setExpectedStartDatetime(task.getExpectedStartDatetime());
													Date dueDate = getDueDate(parallelTask.getExpectedStartDatetime(),
															parallelTask.getTimeToComplete(),
															parallelTask.getTimeUom());
													parallelTask.setExpectedEndDatetime(
															new java.sql.Timestamp(dueDate.getTime()));
													parallelTask.setStatus("Completed");
													parallelTask.setActualEndDatetime(currentDate);
													parallelTask.setAssignType("User");
													parallelTask.setAssignTo(user.getUserId());
													parallelTask.setUpdatedBy(user);
													if (user != null && user.getTimeZone() != null) {
														parallelTask.setUpdatedDatetime(
																new java.sql.Timestamp(new DateUtil()
																		.getCurrentTime(user.getTimeZone()).getTime()));
													} else {
														parallelTask.setUpdatedDatetime(
																new java.sql.Timestamp(new DateUtil()
																		.getCurrentTime(
																				XcelerateConstants.DEFAULT_TIMEZONE)
																		.getTime()));
													}
												} else {
													parallelTask
															.setExpectedStartDatetime(task.getExpectedStartDatetime());
													Date dueDate = getDueDate(parallelTask.getExpectedStartDatetime(),
															parallelTask.getTimeToComplete(),
															parallelTask.getTimeUom());
													parallelTask.setExpectedEndDatetime(
															new java.sql.Timestamp(dueDate.getTime()));
													parallelTask.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
												}
											} else {
												parallelTask.setExpectedStartDatetime(task.getExpectedStartDatetime());
												Date dueDate = getDueDate(parallelTask.getExpectedStartDatetime(),
														parallelTask.getTimeToComplete(), parallelTask.getTimeUom());
												parallelTask.setExpectedEndDatetime(
														new java.sql.Timestamp(dueDate.getTime()));
												parallelTask.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
											}
										} else {
											parallelTask.setExpectedStartDatetime(task.getExpectedStartDatetime());
											Date dueDate = getDueDate(parallelTask.getExpectedStartDatetime(),
													parallelTask.getTimeToComplete(), parallelTask.getTimeUom());
											parallelTask
													.setExpectedEndDatetime(new java.sql.Timestamp(dueDate.getTime()));
											parallelTask.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
										}
										tasksRepository.save(parallelTask);
									} else {
										break;
									}
								}
							} else if (task.getStatus().equals("Completed")) {
								CommonTasks previousTask = null;
								for (CommonTasks parallelTask : parallelTasks) {
									if (parallelTask.getTriggerEvent().equalsIgnoreCase("Simultaneously")
											|| previousTask == null || previousTask.getStatus().equals("Completed")) {

										// parallelTask.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
										if (parallelTask.getActionRequired().equals("Upload Estimate")
												|| parallelTask.getActionRequired().equals("Upload WO")
												|| parallelTask.getActionRequired().equals("Upload Invoice")
												|| parallelTask.getActionRequired().equals("Upload of COC")) {
											if (documents != null && documents.size() > 0) {
												boolean docExists = false;
												for (CommonFileDetails jobDocument : documents) {
													if (jobDocument.getFileCategory().equals(
															docCategory.get(parallelTask.getActionRequired()))) {
														docExists = true;
														break;
													}
												}
												if (docExists) {
													parallelTask.setStatus("Completed");
													parallelTask.setAssignType("User");
													parallelTask.setAssignTo(user.getUserId());
													parallelTask.setUpdatedBy(user);
													if (user != null && user.getTimeZone() != null) {
														parallelTask.setUpdatedDatetime(
																new java.sql.Timestamp(new DateUtil()
																		.getCurrentTime(user.getTimeZone()).getTime()));
													} else {
														parallelTask.setUpdatedDatetime(
																new java.sql.Timestamp(new DateUtil()
																		.getCurrentTime(
																				XcelerateConstants.DEFAULT_TIMEZONE)
																		.getTime()));
													}
												} else {
													parallelTask
															.setExpectedStartDatetime(task.getExpectedStartDatetime());
													Date dueDate = getDueDate(parallelTask.getExpectedStartDatetime(),
															parallelTask.getTimeToComplete(),
															parallelTask.getTimeUom());
													parallelTask.setExpectedEndDatetime(
															new java.sql.Timestamp(dueDate.getTime()));
													parallelTask.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
												}
											} else {
												parallelTask.setExpectedStartDatetime(task.getExpectedStartDatetime());
												Date dueDate = getDueDate(parallelTask.getExpectedStartDatetime(),
														parallelTask.getTimeToComplete(), parallelTask.getTimeUom());
												parallelTask.setExpectedEndDatetime(
														new java.sql.Timestamp(dueDate.getTime()));
												parallelTask.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
											}
										} else {
											parallelTask.setExpectedStartDatetime(task.getExpectedStartDatetime());
											Date dueDate = getDueDate(parallelTask.getExpectedStartDatetime(),
													parallelTask.getTimeToComplete(), parallelTask.getTimeUom());
											parallelTask
													.setExpectedEndDatetime(new java.sql.Timestamp(dueDate.getTime()));
											parallelTask.setStatus(XcelerateConstants.TASK_STATUS_OPEN);
										}
										parallelTask = tasksRepository.save(parallelTask);
										previousTask = parallelTask;
									} else {
										break;
									}
								}
							}
						}
					}
				}
			}
		}

		LOGGER.info("*********  Updating task  are  Completed  ***********");
	}

	/**
	 * Method to calculating kpi for encircle documents sync
	 * @param job-the     job Job details.
	 * @param userId-the  user ID Integer.
	 * @param webhook-the webhook Media webhook.
	 */
	private void calculateJobKpis(JobDetails job, int userId, MediaWebhook webhook) {
		TenantContext.setCurrentTenant(webhook.getTenant());
		LOGGER.info("********* calculating kpi  for encircle documents sync are  Started  ***********");
		try {
			CommonMilestones milestone = milestonesRepository.findBySourceId(job.getJobId());
			List<JobKpiTargets> kpiList = jobKpiTargetsRepository.findAll();
			Map<String, JobKpiTargets> kpis = new HashMap<String, JobKpiTargets>();
			JobKpiTargets target = null;
			for (JobKpiTargets kpiTarget : kpiList) {
				kpis.put(kpiTarget.getKpiName(), kpiTarget);
			}
			JobUtil jobUtil = new JobUtil();
			DateUtil dateUtil = new DateUtil();
			if (job.getJobType().equalsIgnoreCase("Water") || job.getJobType().equalsIgnoreCase("Bio")
					|| job.getJobType().equalsIgnoreCase("Board Up")) {

				// Onsite arrival time
				if (milestone.getDateinspected() != null) {
					// Onsite Arrival Time
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Onsite Arrival Time");
					target = kpis.get("Onsite Arrival Time");
					if (jobKpiDetails != null && milestone.getDateReceived() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateReceived(),
								milestone.getDateinspected(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateReceived() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Onsite Arrival Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateReceived(),
									milestone.getDateinspected(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}
				if (milestone.getStartdate() != null) {

					// Days To Start
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Days to Start");
					target = kpis.get("Days to Start (WA date to Start Date)");
					if (jobKpiDetails != null && milestone.getDateofWorkAuthorization() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getStartdate(),
								milestone.getDateofWorkAuthorization(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {

						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateofWorkAuthorization() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Days to Start");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getStartdate(),
									milestone.getDateofWorkAuthorization(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}
				if (milestone.getDateOfcOs() != null) {

					// WIP Time calculation
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"WIP Time");
					target = kpis.get("WIP Time (Start to COC)");
					if (jobKpiDetails != null && milestone.getStartdate() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getStartdate(),
								milestone.getDateOfcOs(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getStartdate() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("WIP Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getStartdate(),
									milestone.getDateOfcOs(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}

					// Target Completion Accuracy
					jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Target Completion Accuracy");
					target = kpis.get("Target Completion Accuracy (COC Date versus Target Complete)");
					if (jobKpiDetails != null && milestone.getTargetcompletiondate() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getTargetcompletiondate(),
								milestone.getDateOfcOs(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getTargetcompletiondate() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Target Completion Accuracy");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getTargetcompletiondate(),
									milestone.getDateOfcOs(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}
				if (milestone.getDateEstimatesent() != null) {
					// Estimated Bid Time
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Estimate Bid Time");
					target = kpis.get("Estimate Bid Time (Inspected to Estimate)");
					if (jobKpiDetails != null && milestone.getDateinspected() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateinspected(),
								milestone.getDateEstimatesent(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateinspected() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Estimate Bid Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateinspected(),
									milestone.getDateEstimatesent(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}
				if (milestone.getFinalInvoiceDate() != null) {
					// Cycle Time calculation
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Cycle Time");
					target = kpis.get("Cycle Time (Received to Final Invoiced)");
					if (jobKpiDetails != null && milestone.getDateReceived() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateReceived(),
								milestone.getFinalInvoiceDate(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateReceived() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Cycle Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateReceived(),
									milestone.getFinalInvoiceDate(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}

					// Total Final Invoice Time
					jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Total Final Invoice Time");
					target = kpis.get("Total Final Invoice Time (COC to Final Invoice)");
					if (jobKpiDetails != null && milestone.getDateOfcOs() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateOfcOs(),
								milestone.getFinalInvoiceDate(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateOfcOs() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Total Final Invoice Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateOfcOs(),
									milestone.getFinalInvoiceDate(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}
			} else {
				if (milestone.getDateinspected() != null) {
					// Onsite Arrival Time
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Onsite Arrival Time");
					target = kpis.get("Onsite Arrival Time");
					if (jobKpiDetails != null && milestone.getDateReceived() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateReceived(),
								milestone.getDateinspected(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateReceived() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Onsite Arrival Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateReceived(),
									milestone.getDateinspected(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}
				if (milestone.getDateEstimatesent() != null) {
					
					// Estimated Bid Time
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Estimate Bid Time");
					target = kpis.get("Estimate Bid Time (Inspected to Estimate)");
					if (jobKpiDetails != null && milestone.getDateinspected() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateinspected(),
								milestone.getDateEstimatesent(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {

						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateinspected() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Estimate Bid Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateinspected(),
									milestone.getDateEstimatesent(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}
				if (milestone.getStartdate() != null) {
					// Days To Start
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Days to Start");
					target = kpis.get("Days to Start (WA date to Start Date)");
					if (jobKpiDetails != null && milestone.getDateofWorkAuthorization() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateofWorkAuthorization(),
								milestone.getStartdate(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateofWorkAuthorization() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Days to Start");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateofWorkAuthorization(),
									milestone.getStartdate(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}
				if (milestone.getDateOfcOs() != null) {
					
					// WIP Time calculation
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"WIP Time");
					target = kpis.get("WIP Time (Start to COC)");
					if (jobKpiDetails != null && milestone.getStartdate() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getStartdate(),
								milestone.getDateOfcOs(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getStartdate() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("WIP Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getStartdate(),
									milestone.getDateOfcOs(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}

					// Target Completion Accuracy
					jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Target Completion Accuracy");
					target = kpis.get("Target Completion Accuracy (COC Date versus Target Complete)");
					if (jobKpiDetails != null && milestone.getTargetcompletiondate() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getTargetcompletiondate(),
								milestone.getDateOfcOs(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getTargetcompletiondate() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Target Completion Accuracy");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getTargetcompletiondate(),
									milestone.getDateOfcOs(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}

				// Cycle Time Calculation
				if (milestone.getFinalInvoiceDate() != null) {
					JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Cycle Time");
					target = kpis.get("Cycle Time (Received to Final Invoiced)");
					if (jobKpiDetails != null && milestone.getDateReceived() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateReceived(),
								milestone.getFinalInvoiceDate(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateReceived() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Cycle Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateReceived(),
									milestone.getFinalInvoiceDate(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}

					// Total Final Invoice Time
					jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
							"Total Final Invoice Time");
					target = kpis.get("Total Final Invoice Time (COC to Final Invoice)");
					if (jobKpiDetails != null && milestone.getDateOfcOs() != null) {
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateOfcOs(),
								milestone.getFinalInvoiceDate(), target.getUom()));
						jobKpiDetails.setUpdatedBy(userId);
						jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						kpiDetailsRepository.save(jobKpiDetails);
					} else {
						if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null
								&& milestone.getDateOfcOs() != null) {
							jobKpiDetails = new JobKpiDetails();
							jobKpiDetails.setKpiId(target.getKpiId());
							jobKpiDetails.setKpiName("Total Final Invoice Time");
							jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
							jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateOfcOs(),
									milestone.getFinalInvoiceDate(), target.getUom()));
							jobKpiDetails.setUom(target.getUom());
							jobKpiDetails.setSource(XcelerateConstants.SOURCE);
							jobKpiDetails.setCreatedBy(userId);
							jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
									new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
							jobKpiDetails.setJobId(job.getJobId());
							kpiDetailsRepository.save(jobKpiDetails);
						}
					}
				}
			}
			if (milestone.getDateinspected() != null && milestone.getDateofWorkAuthorization() != null) {

				// Days to Sign (Inspected to Work Autho)
				JobKpiDetails jobKpiDetails = kpiDetailsRepository.findByJobIdAndKpiNameLike(job.getJobId(),
						"Days to Sign");
				target = kpis.get("Days to Sign (Inspected to Work Authorization)");
				if (jobKpiDetails != null) {
					jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateinspected(),
							milestone.getDateofWorkAuthorization(), target.getUom()));
					jobKpiDetails.setUpdatedBy(userId);
					jobKpiDetails.setUpdatedDatetime(new java.sql.Timestamp(
							new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
					kpiDetailsRepository.save(jobKpiDetails);
				} else {
					if (target != null && jobUtil.getKpiTargetValue(target, job.getJobType()) != null) {
						jobKpiDetails = new JobKpiDetails();
						jobKpiDetails.setKpiId(target.getKpiId());
						jobKpiDetails.setKpiName("Days to Sign");
						jobKpiDetails.setKpiTargetValue(jobUtil.getKpiTargetValue(target, job.getJobType()));
						jobKpiDetails.setKpiValue(dateUtil.getDuration(milestone.getDateinspected(),
								milestone.getDateofWorkAuthorization(), target.getUom()));
						jobKpiDetails.setUom(target.getUom());
						jobKpiDetails.setSource(XcelerateConstants.SOURCE);
						jobKpiDetails.setCreatedBy(userId);
						jobKpiDetails.setCreatedDatetime(new java.sql.Timestamp(
								new DateUtil().getCurrentTime(XcelerateConstants.DEFAULT_TIMEZONE).getTime()));
						jobKpiDetails.setJobId(job.getJobId());
						kpiDetailsRepository.save(jobKpiDetails);
					}
				}
			}
			LOGGER.info("*********  calculating kpi  for encircle documents sync is completed  ***********");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to get Due date.
	 * @param date-the    date Date.
	 * @param time-the    time Integer.
	 * @param timeUOM-the timeUOM String.
	 * @return-due date as Date.
	 */
	private Date getDueDate(Date date, int time, String timeUOM) {
		Date dueDate = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		final long MINUTE = 60000;// milli seconds for a minute
		if (timeUOM.equalsIgnoreCase("Minutes")) {
			dueDate = new Date(date.getTime() + (time * MINUTE));
		} else if (timeUOM.equalsIgnoreCase("Hours")) {

			// calculating due date if time to complete is configured in hours
			dueDate = new Date(date.getTime() + (time * 60 * MINUTE));
		} else if (timeUOM.equalsIgnoreCase("Days")) {

			// calculating due date if time to complete is configured in days
			dueDate = new Date(date.getTime() + (time * 24 * 60 * MINUTE));
		} else if (timeUOM.equalsIgnoreCase("Weeks")) {

			// calculating due date if time to complete is configured in Weeks
			dueDate = new Date(date.getTime() + (time * 7 * 24 * 60 * MINUTE));
		}
		String finalDueDate = dateFormat.format(dueDate);
		try {
			dueDate = dateFormat.parse(finalDueDate);
		} catch (Exception e) {
		}
		return dueDate;
	}
}