/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.tenant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.wissen.xcelerate.encircle.service.RestorationCompanyService;
import com.wissen.xcelerate.encircle.util.XcelerateConstants;

/**
 * Interceptor class for Tenant.
 */
@Component
public class TenantInterceptor extends HandlerInterceptorAdapter {
	private static final Logger LOGGER = LogManager.getLogger(TenantInterceptor.class);
	@Autowired
	private RestorationCompanyService service;
	@Autowired
	LocalContainerEntityManagerFactoryBean em;
	
	/**
	 * Method to get tenant set.
	 * @return-tenant set as Boolean.
	 */
	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws Exception {
		System.out.println("tenantContext Inteceptor get tenant RequestURI----->" + req.getRequestURI());
		LOGGER.info("tenantContext Inteceptor get tenant RequestURI ----->" + req.getRequestURI());
		System.out.println("tenantContext Inteceptor get tenant RequestURL----->" + req.getRequestURL());
		LOGGER.info("TenantContext Inteceptor get tenant RequestURL ----->" + req.getRequestURL());
		String originalHost = req.getHeader("x-original-host");
		String clientUrl = req.getHeader("x-original-url");
		System.out.println("base URL--------->" + originalHost);
		System.out.println("url--------->" + clientUrl);
		LOGGER.info("url--------->" + clientUrl);
		LOGGER.info("base URL--------->" + originalHost);
		String delivaryUrl = "https://" + originalHost + clientUrl;
		System.out.println("webhook Delivary Url ====>" + delivaryUrl);
		boolean tenantSet = false;
		if (originalHost != null && clientUrl != null) {
			
			// if(req.getRequestURL().indexOf(originalHost)>0 ){
			// if(req.getRequestURL().indexOf("encircle")>0 ){
			if (originalHost.equals(XcelerateConstants.ENCIRCLE_UAT_HOST_URL)) {
				try {
					String tenant = service.getSchemaDetailsByEncircleDelivaryUrl(delivaryUrl);
					if (tenant != null) {
						System.out.println("Tenant --------->" + tenant);
						TenantContext.setCurrentTenant(tenant);
						tenantSet = true;
					}
				} catch (Exception e) {
					throw new Exception(" Exception: " + e.getMessage());
				}
			}
		}

		/*
		 * else{ if(req.getRequestURL().indexOf("health")>0 ){
		 * System.out.println("<======Request come to Health Api======>");
		 * LOGGER.info("Application is Up and Running");
		 * //TenantContext.setCurrentTenant("NoTenant"); tenantSet = true; }
		 * 
		 * }
		 */
		return tenantSet;
	}

	/**
	 * Method to clear tenant context. 
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		String a = TenantContext.getCurrentTenant();
		System.out.println("<======Current tenant  in postHandle method of Interceptor======>" + a);
		TenantContext.clear();
	}
}