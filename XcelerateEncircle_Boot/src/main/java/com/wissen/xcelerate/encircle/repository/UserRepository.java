package com.wissen.xcelerate.encircle.repository;

/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.wissen.xcelerate.encircle.model.UserDetails;

/**
 * Repository class for user.
 */
public interface UserRepository extends Repository<UserDetails, Long> {
	UserDetails save(UserDetails user);

//	@Query(value="EXECUTE AS USER=:schema",nativeQuery=true)
//	void executeUser(@Param("schema") String schema);

	UserDetails findByUserId(@Param("userId") int userId);

}