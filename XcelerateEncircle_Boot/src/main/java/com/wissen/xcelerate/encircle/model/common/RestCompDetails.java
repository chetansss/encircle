/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model.common;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * The persistent class for the rest_comp_details database table. 
 */
@Entity
@Table(name="rest_comp_details",schema="common")
//@NamedQuery(name="RestCompDetails.findAll", query="SELECT r FROM RestCompDetails r")
public class RestCompDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="rest_comp_id")
	private int restCompId;

	@Column(name="rest_comp_address")
	private String restCompAddress;

	@Column(name="rest_comp_code")
	private String restCompCode;

	@Column(name="rest_comp_email")
	private String restCompEmail;

	@Column(name="rest_comp_hashcode")
	private String restCompHashcode;

	@Column(name="rest_comp_inactive_date")
	private Timestamp restCompInactiveDate;

	@Column(name="rest_comp_inactive_reason")
	private Timestamp restCompInactiveReason;

	@Column(name="rest_comp_logo")
	private int restCompLogo;

	@Column(name="rest_comp_max_users")
	private int restCompMaxUsers;

	@Column(name="rest_comp_name")
	private String restCompName;

	@Column(name="rest_comp_phone_number1")
	private String restCompPhoneNumber1;

	@Column(name="rest_comp_phone_number2")
	private String restCompPhoneNumber2;

	@Column(name="rest_comp_plan_id")
	private int restCompPlanId;

	@Column(name="rest_comp_registered_date")
	private Timestamp restCompRegisteredDate;

	@Column(name="rest_comp_service_end_date")
	private Timestamp restCompServiceEndDate;

	@Column(name="rest_comp_service_start_date")
	private Timestamp restCompServiceStartDate;

	@Column(name="rest_comp_setup_date")
	private Timestamp restCompSetupDate;

	@Column(name="rest_comp_short_name")
	private String restCompShortName;

	@Column(name="rest_comp_size")
	private String restCompSize;

	@Column(name="rest_comp_status")
	private String restCompStatus;

	@Column(name="rest_comp_title")
	private String restCompTitle;

	@Column(name="schema_name")
	private String schemaName;
	
	@Column(name = "updated_by")
	private Integer updatedBy;

	@Column(name = "updated_datetime")
	private Timestamp updatedDatetime;
	
    private String source;
	
    @Column(name="smtp_email")
    private String smtpEmail;
    
    @Column(name="smtp_auth")
	private String smtpAuth;
    
    @Column(name="encircle_api_token")
    private String encircleApiToken;
     
    @Column(name = "rest_comp_fax_number")
	private String restCompFaxNumber;
	
	@Column(name="rest_comp_website")
	private String restCompWebsite;
	
	@Column(name="rest_comp_url")
    private String restCompUrl;
	
	@Column(name="qbo_check")
    private String qboCheck;
	
	@Column(name="qbo_client_id")
    private String qboClientId;
	
	@Column(name="qbo_client_secret")
    private String qboClientSecret;
	
	@Column(name="qbo_realm_id")
    private String qboRealmId;
	
	@Column(name="qbo_start_date")
    private Date qboStartDate;
	
	@Column(name="qbo_verifier_token")
	private String  qboVerifierToken;
	
	@Column(name="customer_name_preference")
	private String  customerNamePreference;
	
	@Column(name="zapier_token")
	private String zapierToken;
	
	@Column(name="qbo_redirect_uri")
    private String  qboRedirectUri;
   
   @Column(name="qbo_access_token")
   private String  qboAccessToken;
   
   @Column(name="qbo_refresh_token")
   private String  qboRefreshToken;
   
   @Column(name="qbo_webhook_token")
   private String  qboWebhookToken;
   
   @Column(name="qbo_app_scope")
   private String  qboAppScope;
	
   @Column(name="webhook_delivery_url")
	private String webhookDeliveryUrl;
	
	@Column(name="webhook_id")
	private Integer webhookId;
	
	/**
	 * Method to Rest CompDetails.
	 */
	public RestCompDetails() {
	}

	/**
	 * Method to get Rest CompId.
	 * @return Rest CompId as integer.
	 */
	public int getRestCompId() {
		return this.restCompId;
	}

	/**
	 * Method to set Rest CompId.
	 * @param  Rest CompId-the Rest CompId is integer
	 */
	public void setRestCompId(int restCompId) {
		this.restCompId = restCompId;
	}

	/**
	 * Method to get Rest CompAddress.
	 * @return Rest CompAddress as string.
	 */
	public String getRestCompAddress() {
		return this.restCompAddress;
	}

	/**
	 * Method to set Rest CompAddress.
	 * @param  Rest CompAddress-the Rest CompAddress is integer
	 */
	public void setRestCompAddress(String restCompAddress) {
		this.restCompAddress = restCompAddress;
	}

	/**
	 * Method to get Rest CompCode.
	 * @return Rest CompCode as string.
	 */
	public String getRestCompCode() {
		return this.restCompCode;
	}

	/**
	 * Method to set Rest CompCode.
	 * @param  Rest CompCode-the Rest CompCode is string.
	 */
	public void setRestCompCode(String restCompCode) {
		this.restCompCode = restCompCode;
	}

	/**
	 * Method to get Rest CompEmail.
	 * @return Rest CompEmail as string.
	 */
	public String getRestCompEmail() {
		return this.restCompEmail;
	}

	/**
	 * Method to set Rest CompEmail.
	 * @param  Rest CompEmail-the Rest CompEmail is string.
	 */
	public void setRestCompEmail(String restCompEmail) {
		this.restCompEmail = restCompEmail;
	}

	/**
	 * Method to get Rest CompHashcode.
	 * @return Rest CompHashcode as string.
	 */
	public String getRestCompHashcode() {
		return this.restCompHashcode;
	}

	/**
	 * Method to set Rest CompHashcode.
	 * @param  Rest CompHashcode-the Rest CompHashcode is string.
	 */
	public void setRestCompHashcode(String restCompHashcode) {
		this.restCompHashcode = restCompHashcode;
	}

	/**
	 * Method to get Rest CompInactiveDate.
	 * @return Rest CompInactiveDate as timestamp.
	 */
	public Timestamp getRestCompInactiveDate() {
		return this.restCompInactiveDate;
	}

	/**
	 * Method to set Rest CompInactiveDate.
	 * @param  Rest CompInactiveDate-the Rest CompInactiveDate is timestamp.
	 */
	public void setRestCompInactiveDate(Timestamp restCompInactiveDate) {
		this.restCompInactiveDate = restCompInactiveDate;
	}

	/**
	 * Method to get Rest CompInactiveReason.
	 * @return Rest CompInactiveReason as timestamp.
	 */
	public Timestamp getRestCompInactiveReason() {
		return this.restCompInactiveReason;
	}

	/**
	 * Method to set Rest CompInactiveReason.
	 * @param  Rest CompInactiveReason-the Rest CompInactiveReason is timestamp.
	 */
	public void setRestCompInactiveReason(Timestamp restCompInactiveReason) {
		this.restCompInactiveReason = restCompInactiveReason;
	}

	/**
	 * Method to get Rest CompLogo.
	 * @return Rest CompLogo as integer.
	 */
	public int getRestCompLogo() {
		return this.restCompLogo;
	}

	/**
	 * Method to set Rest CompLogo.
	 * @param  Rest CompLogo-the Rest CompLogo is integer.
	 */
	public void setRestCompLogo(int restCompLogo) {
		this.restCompLogo = restCompLogo;
	}

	/**
	 * Method to get Rest CompMaxUsers.
	 * @return Rest CompMaxUsers as integer.
	 */
	public int getRestCompMaxUsers() {
		return this.restCompMaxUsers;
	}

	/**
	 * Method to set Rest CompMaxUsers.
	 * @param  Rest CompMaxUsers-the Rest CompMaxUsers is integer.
	 */
	public void setRestCompMaxUsers(int restCompMaxUsers) {
		this.restCompMaxUsers = restCompMaxUsers;
	}

	/**
	 * Method to get Rest CompName.
	 * @return Rest CompName as string.
	 */
	public String getRestCompName() {
		return this.restCompName;
	}

	/**
	 * Method to set Rest CompName.
	 * @param  Rest CompName-the Rest CompName is string.
	 */
	public void setRestCompName(String restCompName) {
		this.restCompName = restCompName;
	}

	/**
	 * Method to get Rest CompPhoneNumber1.
	 * @return Rest CompPhoneNumber1 as string.
	 */
	public String getRestCompPhoneNumber1() {
		return this.restCompPhoneNumber1;
	}

	/**
	 * Method to set Rest CompPhoneNumber1.
	 * @param  Rest CompPhoneNumber1-the Rest CompPhoneNumber1 is string.
	 */
	public void setRestCompPhoneNumber1(String restCompPhoneNumber1) {
		this.restCompPhoneNumber1 = restCompPhoneNumber1;
	}

	/**
	 * Method to get Rest CompPhoneNumber2.
	 * @return Rest CompPhoneNumber2 as string.
	 */
	public String getRestCompPhoneNumber2() {
		return this.restCompPhoneNumber2;
	}

	/**
	 * Method to set Rest CompPhoneNumber2.
	 * @param  Rest CompPhoneNumber2-the Rest CompPhoneNumber2 is string.
	 */
	public void setRestCompPhoneNumber2(String restCompPhoneNumber2) {
		this.restCompPhoneNumber2 = restCompPhoneNumber2;
	}

	/**
	 * Method to get Rest CompPlanId.
	 * @return Rest CompPlanId as integer.
	 */
	public int getRestCompPlanId() {
		return this.restCompPlanId;
	}

	/**
	 * Method to set Rest CompPlanId.
	 * @param  Rest CompPlanId-the Rest CompPlanId is integer.
	 */
	public void setRestCompPlanId(int restCompPlanId) {
		this.restCompPlanId = restCompPlanId;
	}

	/**
	 * Method to get Rest CompRegisteredDate.
	 * @return Rest CompRegisteredDate as timestamp.
	 */
	public Timestamp getRestCompRegisteredDate() {
		return this.restCompRegisteredDate;
	}

	/**
	 * Method to set Rest CompRegisteredDate.
	 * @param  Rest CompRegisteredDate-the Rest CompRegisteredDate is timestamp.
	 */
	public void setRestCompRegisteredDate(Timestamp restCompRegisteredDate) {
		this.restCompRegisteredDate = restCompRegisteredDate;
	}

	/**
	 * Method to get Rest CompServiceEndDate.
	 * @return Rest CompServiceEndDate as timestamp.
	 */
	public Timestamp getRestCompServiceEndDate() {
		return this.restCompServiceEndDate;
	}

	/**
	 * Method to set Rest CompServiceEndDate.
	 * @param  Rest CompServiceEndDate-the Rest CompServiceEndDate is timestamp.
	 */
	public void setRestCompServiceEndDate(Timestamp restCompServiceEndDate) {
		this.restCompServiceEndDate = restCompServiceEndDate;
	}

	/**
	 * Method to get Rest CompServiceStartDate.
	 * @return Rest CompServiceStartDate as timestamp.
	 */

	public Timestamp getRestCompServiceStartDate() {
		return this.restCompServiceStartDate;
	}

	/**
	 * Method to set Rest CompServiceStartDate.
	 * @param  Rest CompServiceStartDate-the Rest CompServiceStartDate is timestamp.
	 */
	public void setRestCompServiceStartDate(Timestamp restCompServiceStartDate) {
		this.restCompServiceStartDate = restCompServiceStartDate;
	}

	/**
	 * Method to get Rest CompSetupDate.
	 * @return Rest CompSetupDate as timestamp.
	 */
	public Timestamp getRestCompSetupDate() {
		return this.restCompSetupDate;
	}

	/**
	 * Method to set Rest CompSetupDate.
	 * @param  Rest CompSetupDate-the Rest CompSetupDate is timestamp.
	 */
	public void setRestCompSetupDate(Timestamp restCompSetupDate) {
		this.restCompSetupDate = restCompSetupDate;
	}

	/**
	 * Method to get Rest CompShortName.
	 * @return Rest CompShortName as string.
	 */
	public String getRestCompShortName() {
		return this.restCompShortName;
	}

	/**
	 * Method to set Rest CompShortName.
	 * @param  Rest CompShortName-the Rest CompShortName is string.
	 */
	public void setRestCompShortName(String restCompShortName) {
		this.restCompShortName = restCompShortName;
	}

	/**
	 * Method to get Rest CompSize.
	 * @return Rest CompSize as string.
	 */
	public String getRestCompSize() {
		return this.restCompSize;
	}

	/**
	 * Method to set Rest CompSize.
	 * @param  Rest CompSize-the Rest CompSize is string.
	 */
	public void setRestCompSize(String restCompSize) {
		this.restCompSize = restCompSize;
	}

	/**
	 * Method to get Rest CompStatus.
	 * @return Rest CompStatus as string.
	 */
	public String getRestCompStatus() {
		return this.restCompStatus;
	}

	/**
	 * Method to set Rest CompStatus.
	 * @param  Rest CompStatus-the Rest CompStatus is string.
	 */

	public void setRestCompStatus(String restCompStatus) {
		this.restCompStatus = restCompStatus;
	}

	/**
	 * Method to get Rest CompTitle.
	 * @return Rest CompTitle as string.
	 */
	public String getRestCompTitle() {
		return this.restCompTitle;
	}

	/**
	 * Method to set  CompTitle.
	 * @param   CompTitle-the Rest CompTitle is string.
	 */
	public void setRestCompTitle(String restCompTitle) {
		this.restCompTitle = restCompTitle;
	}

	/**
	 * Method to get  SchemaName.
	 * @return  SchemaName as string.
	 */
	public String getSchemaName() {
		return this.schemaName;
	}

	/**
	 * Method to set  SchemaName.
	 * @param   SchemaName-the  SchemaName is string.
	 */
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	/**
	 * Method to get  UpdatedBy.
	 * @return  UpdatedBy as integer.
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set  UpdatedBy.
	 * @param  UpdatedBy-the  UpdatedBy is integer.
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get  UpdatedDatetime.
	 * @return  UpdatedDatetime as timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set  UpdatedDatetime.
	 * @param  UpdatedDatetime-the  UpdatedDatetime is timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get Source.
	 * @return Source as string.
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Method to set  Source.
	 * @param  Source-the Source is string.
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Method to get Smtp Email.
	 * @return Smtp Email as string.
	 */
	public String getSmtpEmail() {
		return smtpEmail;
	}

	/**
	 * Method to set  Smtp Email.
	 * @param  Smtp Email-the Smtp Email is string.
	 */
	public void setSmtpEmail(String smtpEmail) {
		this.smtpEmail = smtpEmail;
	}

	/**
	 * Method to get Smtp Auth.
	 * @return Smtp Auth as string.
	 */
	public String getSmtpAuth() {
		return smtpAuth;
	}

	/**
	 * Method to set  Smtp Auth.
	 * @param  Smtp Auth-the Auth Email is string.
	 */
	public void setSmtpAuth(String smtpAuth) {
		this.smtpAuth = smtpAuth;
	}

	/**
	 * Method to get Encircle ApiToken.
	 * @return Encircle ApiToken as string.
	 */
	public String getEncircleApiToken() {
		return encircleApiToken;
	}

	/**
	 * Method to set  Encircle ApiToken.
	 * @param  Encircle ApiToken-the Encircle ApiToken is string.
	 */
	public void setEncircleApiToken(String encircleApiToken) {
		this.encircleApiToken = encircleApiToken;
	}

	/**
	 * Method to get Rest CompFaxNumber.
	 * @return Rest CompFaxNumber as string.
	 */
	public String getRestCompFaxNumber() {
		return restCompFaxNumber;
	}

	/**
	 * Method to set  rest CompFaxNumber.
	 * @param  rest CompFaxNumber-the rest CompFaxNumber is string.
	 */
	public void setRestCompFaxNumber(String restCompFaxNumber) {
		this.restCompFaxNumber = restCompFaxNumber;
	}

	/**
	 * Method to get Rest CompWebsite.
	 * @return Rest CompWebsite as string.
	 */
	public String getRestCompWebsite() {
		return restCompWebsite;
	}

	/**
	 * Method to set  rest CompWebsite.
	 * @param  rest CompWebsite-the rest CompWebsite is string.
	 */
	public void setRestCompWebsite(String restCompWebsite) {
		this.restCompWebsite = restCompWebsite;
	}

	/**
	 * Method to get Rest CompUrl.
	 * @return Rest CompUrl as string.
	 */
	public String getRestCompUrl() {
		return restCompUrl;
	}

	/**
	 * Method to set  rest CompUrl.
	 * @param  rest CompUrl-the rest CompUrl is string.
	 */
	public void setRestCompUrl(String restCompUrl) {
		this.restCompUrl = restCompUrl;
	}

	/**
	 * Method to get QboCheck.
	 * @return QboCheck as string.
	 */
	public String getQboCheck() {
		return qboCheck;
	}

	/**
	 * Method to set  QboCheck.
	 * @param  QboCheck-the QboCheck is string.
	 */
	public void setQboCheck(String qboCheck) {
		this.qboCheck = qboCheck;
	}

	/**
	 * Method to get qbo ClientId.
	 * @return Qbo ClientId as string.
	 */
	public String getQboClientId() {
		return qboClientId;
	}

	/**
	 * Method to set  Qbo ClientId.
	 * @param  Qbo ClientId-the Qbo ClientId is string.
	 */
	public void setQboClientId(String qboClientId) {
		this.qboClientId = qboClientId;
	}

	/**
	 * Method to get qbo ClientSecre.
	 * @return Qbo ClientSecre as string.
	 */
	public String getQboClientSecret() {
		return qboClientSecret;
	}

	/**
	 * Method to set  Qbo ClientSecret.
	 * @param  Qbo ClientSecret-the Qbo ClientSecret is string.
	 */
	public void setQboClientSecret(String qboClientSecret) {
		this.qboClientSecret = qboClientSecret;
	}

	/**
	 * Method to get qbo RealmId.
	 * @return Qbo RealmId as string.
	 */
	public String getQboRealmId() {
		return qboRealmId;
	}

	/**
	 * Method to set  Qbo RealmId.
	 * @param  Qbo RealmId-the Qbo RealmId is string.
	 */
	public void setQboRealmId(String qboRealmId) {
		this.qboRealmId = qboRealmId;
	}

	/**
	 * Method to get qbo VerifierToken.
	 * @return Qbo VerifierToken as string.
	 */
	public String getQboVerifierToken() {
		return qboVerifierToken;
	}

	/**
	 * Method to set  Qbo VerifierToken.
	 * @param  Qbo VerifierToken-the Qbo VerifierToken is string.
	 */
	public void setQboVerifierToken(String qboVerifierToken) {
		this.qboVerifierToken = qboVerifierToken;
	}

	/**
	 * Method to get qbo StartDate.
	 * @return Qbo StartDate as date.
	 */
	public Date getQboStartDate() {
		return qboStartDate;
	}

	/**
	 * Method to set  Qbo StartDate.
	 * @param  Qbo StartDate-the Qbo StartDate is date.
	 */
	public void setQboStartDate(Date qboStartDate) {
		this.qboStartDate = qboStartDate;
	}

	/**
	 * Method to get Customer Name Preference.
	 * @return Customer Name Preference as string.
	 */
	public String getCustomerNamePreference() {
		return customerNamePreference;
	}

	/**
	 * Method to set  Customer Name Preference.
	 * @param  Customer Name Preference-the Customer Name Preference is string.
	 */
	public void setCustomerNamePreference(String customerNamePreference) {
		this.customerNamePreference = customerNamePreference;
	}

	/**
	 * Method to get Zapier Token.
	 * @return Zapier Token as string.
	 */
	public String getZapierToken() {
		return zapierToken;
	}

	/**
	 * Method to set  Zapier Token.
	 * @param  Zapier Token-the Zapier Token is string.
	 */
	public void setZapierToken(String zapierToken) {
		this.zapierToken = zapierToken;
	}

	/**
	 * Method to get Qbo RedirectUri.
	 * @return Qbo RedirectUri Token as string.
	 */
	public String getQboRedirectUri() {
		return qboRedirectUri;
	}

	/**
	 * Method to set  Qbo RedirectUri.
	 * @param Qbo RedirectUri-the Qbo RedirectUri is string.
	 */
	public void setQboRedirectUri(String qboRedirectUri) {
		this.qboRedirectUri = qboRedirectUri;
	}

	/**
	 * Method to get Qbo AccessToken.
	 * @return Qbo AccessToken as string.
	 */
	public String getQboAccessToken() {
		return qboAccessToken;
	}

	/**
	 * Method to set  Qbo AccessToken.
	 * @param Qbo AccessToken-the Qbo AccessToken is string.
	 */
	public void setQboAccessToken(String qboAccessToken) {
		this.qboAccessToken = qboAccessToken;
	}

	/**
	 * Method to get Qbo RefreshToken.
	 * @return Qbo RefreshToken as string.
	 */
	public String getQboRefreshToken() {
		return qboRefreshToken;
	}

	/**
	 * Method to set  Qbo RefreshToken.
	 * @param Qbo RefreshToken-the Qbo RefreshToken is string.
	 */
	public void setQboRefreshToken(String qboRefreshToken) {
		this.qboRefreshToken = qboRefreshToken;
	}

	/**
	 * Method to get Qbo WebhookToken.
	 * @return Qbo WebhookToken as string.
	 */
	public String getQboWebhookToken() {
		return qboWebhookToken;
	}

	/**
	 * Method to set  Qbo WebhookToken.
	 * @param Qbo WebhookToken-the Qbo WebhookToken is string.
	 */
	public void setQboWebhookToken(String qboWebhookToken) {
		this.qboWebhookToken = qboWebhookToken;
	}

	/**
	 * Method to get Qbo AppScope.
	 * @return Qbo AppScope as string.
	 */
	public String getQboAppScope() {
		return qboAppScope;
	}

	/**
	 * Method to set  Qbo AppScope.
	 * @param Qbo AppScope-the Qbo AppScope is string.
	 */
	public void setQboAppScope(String qboAppScope) {
		this.qboAppScope = qboAppScope;
	}

	/**
	 * Method to get Qbo WebhookDeliveryUrl.
	 * @return Qbo WebhookDeliveryUrl as string.
	 */
	public String getWebhookDeliveryUrl() {
		return webhookDeliveryUrl;
	}

	/**
	 * Method to set  Qbo WebhookDeliveryUrl.
	 * @param Qbo WebhookDeliveryUrl-the Qbo WebhookDeliveryUrl is string.
	 */
	public void setWebhookDeliveryUrl(String webhookDeliveryUrl) {
		this.webhookDeliveryUrl = webhookDeliveryUrl;
	}

	/**
	 * Method to get Qbo WebhookId.
	 * @return Qbo WebhookId as integer.
	 */
	public Integer getWebhookId() {
		return webhookId;
	}

	/**
	 * Method to set  Qbo WebhookId.
	 * @param Qbo WebhookId-the Qbo WebhookId is integer.
	 */
	public void setWebhookId(Integer webhookId) {
		this.webhookId = webhookId;
	}
	
	
}