/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/
package com.wissen.xcelerate.encircle.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;

/**
 * The persistent class for the User_User_Details database table.
 */
@Entity
@Table(name = "user_user_details")
//@NamedQuery(name="UserDetails.findAll", query="SELECT u FROM UserDetails u")
public class UserDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private int userId;

	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "created_datetime")
	private Timestamp createdDatetime;

	@Column(name = "contact_id")
	private Integer contactId;

	@Column(name = "email_id")
	private String emailId;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "inactive_reason")
	private String inactiveReason;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "mobile_no")
	private String mobileNo;

	@Column(name = "phone_no")
	private String phoneNo;

	@Column(name = "profile_pic")
	private Integer profilePic;

	@Column(name = "time_zone")
	private String timeZone;

	private String status;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "updated_by")
	private Integer updatedBy;

	@Column(name = "updated_datetime")
	private Timestamp updatedDatetime;

	@Column(name = "user_kpis")
	private String userKpis;

	@Column(name = "privacy_policy")
	private String privacyPolicy;

	@Column(name = "admin_access")
	private String adminAccess;

	@Column(name = "time_tracking_access")
	private String timeTrackingAccess;

	@Column(name = "fcm_token")
	private String fcmToken;

	@Column(name = "mobile_device_fcm_token")
	private String mobileDeviceFcmToken;

	@Column(name = "mobile_device_os")
	private String mobileDeviceOs;

	// bi-directional many-to-one association to CommonFileHistory
	/*
	 * @OneToMany(mappedBy="userUserDetail") private List<CommonFileHistory>
	 * commonFileHistories;
	 * 
	 * //bi-directional many-to-one association to CommonMilestones
	 * 
	 * @OneToMany(mappedBy="userUserDetail") private List<CommonMilestones>
	 * commonMilestones;
	 * 
	 * //bi-directional many-to-one association to CommonTasks
	 * 
	 * @OneToMany(mappedBy="userUserDetail1") private List<CommonTasks>
	 * commonTasks1;
	 * 
	 * //bi-directional many-to-one association to CommonTasks
	 * 
	 * @OneToMany(mappedBy="userUserDetail2") private List<CommonTasks>
	 * commonTasks2;
	 * 
	 * //bi-directional many-to-one association to CustomerDetails
	 * 
	 * @OneToMany(mappedBy="userUserDetail1") private List<CustomerDetails>
	 * customerDetails1;
	 * 
	 * //bi-directional many-to-one association to CustomerDetails
	 * 
	 * @OneToMany(mappedBy="userUserDetail2") private List<CustomerDetails>
	 * customerDetails2;
	 * 
	 * //bi-directional many-to-one association to JobDetails
	 * 
	 * @OneToMany(mappedBy="userUserDetail1") private List<JobDetails> jobDetails1;
	 * 
	 * //bi-directional many-to-one association to JobDetails
	 * 
	 * @OneToMany(mappedBy="userUserDetail2") private List<JobDetails> jobDetails2;
	 * 
	 * //bi-directional many-to-one association to JobDetails
	 * 
	 * @OneToMany(mappedBy="userUserDetail3") private List<JobDetails> jobDetails3;
	 * 
	 * //bi-directional many-to-one association to JobEstimateRI
	 * 
	 * @OneToMany(mappedBy="userUserDetail1") private List<JobEstimateRI>
	 * jobEstimateRis1;
	 * 
	 * //bi-directional many-to-one association to JobEstimateRI
	 * 
	 * @OneToMany(mappedBy="userUserDetail2") private List<JobEstimateRI>
	 * jobEstimateRis2;
	 * 
	 * //bi-directional many-to-one association to JobRIAssignment
	 * 
	 * @OneToMany(mappedBy="userUserDetail") private List<JobRIAssignment>
	 * jobRiAssignments;
	 */

	// bi-directional many-to-one association to UserUserRoles
	/*
	 * @OneToMany(mappedBy="userUserDetail") private List<UserUserRoles>
	 * userUserRoles;
	 */

	@Transient
	private UserAuthentication authentication;

	public UserDetails() {
	}

	/**
	 * Method to get User ID.
	 * @return-user ID as Integer.
	 */
	public int getUserId() {
		return this.userId;
	}

	/**
	 * Method to set User ID. 
	 * @param userId-the user ID Integer.
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Method to get Created by. 
	 * @return-created by as Integer.
	 */
	public int getCreatedBy() {
		return this.createdBy;
	}

	/**
	 * Method to set Created by. 
	 * @param createdBy-the created by Integer.
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Method to get Created dat and time. 
	 * @return-created date and time as Timestamp. 
	 */
	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	/**
	 * Method to set created date and time.
	 * @param createdDatetime-the created date and time Timestamp.
	 */
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	/**
	 * Method to get Contact ID. 
	 * @return-contact ID as Integer. 
	 */
	public Integer getContactId() {
		return contactId;
	}

	/**
	 * Method to set Contact ID.  
	 * @param contactId-the contact ID Integer.
	 */
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	/**
	 * Method to get Email ID. 
	 * @return-EMail id as String.
	 */
	public String getEmailId() {
		return this.emailId;
	}

	/**
	 * Method to set EMail ID.
	 * @param emailId-Email id as String.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * Method to get First name.
	 * @return-first name as String.
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Method to set First name.
	 * @param firstName-the first name String.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Method to get inactive reason.
	 * @return-inactive reason as String.
	 */
	public String getInactiveReason() {
		return this.inactiveReason;
	}

	/**
	 * Method to set inactive reason. 
	 * @param inactiveReason-the inactive reason String.
	 */
	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}

	/**
	 * Method to get Last name.
	 * @return-last name as String.
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Method to set last name.
	 * @param lastName-the last name String.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Method to get mobile no.
	 * @return-mobile no as String. 
	 */
	public String getMobileNo() {
		return this.mobileNo;
	}

	/**
	 * Method to set mobile no.
	 * @param mobileNo-the mobile no String.
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * Method to get phone no.
	 * @return-phone no as Integer.
	 */
	public String getPhoneNo() {
		return this.phoneNo;
	}

	/**
	 * MEthod to set phone no.
	 * @param phoneNo-the phone no String.
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * Method to get profile pic.
	 * @return-profile pic as Integer.
	 */
	public Integer getProfilePic() {
		return this.profilePic;
	}

	/**
	 * Method to set profile Picture.
	 * @param profilePic-the profile picture Integer.
	 */
	public void setProfilePic(Integer profilePic) {
		this.profilePic = profilePic;
	}

	/**
	 * Method to get Timezone.
	 * @return-timezone as String.
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * Method to set time zone.
	 * @param timeZone-the time zone String.
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * Method to get status.
	 * @return-status as String.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Method to set status.
	 * @param status-the status String.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Method to get user name.
	 * @return-user name as String.
	 */
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Method to set user name.
	 * @param userName-the user name String.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/*
	 * public List<CommonFileHistory> getCommonFileHistories() { return
	 * this.commonFileHistories; }
	 * 
	 * public void setCommonFileHistories(List<CommonFileHistory>
	 * commonFileHistories) { this.commonFileHistories = commonFileHistories; }
	 * 
	 * public CommonFileHistory addCommonFileHistory(CommonFileHistory
	 * commonFileHistory) { getCommonFileHistories().add(commonFileHistory);
	 * commonFileHistory.setUserUserDetail(this);
	 * 
	 * return commonFileHistory; }
	 * 
	 * public CommonFileHistory removeCommonFileHistory(CommonFileHistory
	 * commonFileHistory) { getCommonFileHistories().remove(commonFileHistory);
	 * commonFileHistory.setUserUserDetail(null);
	 * 
	 * return commonFileHistory; }
	 * 
	 * public List<CommonMilestones> getCommonMilestones() { return
	 * this.commonMilestones; }
	 * 
	 * public void setCommonMilestones(List<CommonMilestones> commonMilestones) {
	 * this.commonMilestones = commonMilestones; }
	 * 
	 * public CommonMilestones addCommonMilestone(CommonMilestones commonMilestone)
	 * { getCommonMilestones().add(commonMilestone);
	 * commonMilestone.setUserUserDetail(this);
	 * 
	 * return commonMilestone; }
	 * 
	 * public CommonMilestones removeCommonMilestone(CommonMilestones
	 * commonMilestone) { getCommonMilestones().remove(commonMilestone);
	 * commonMilestone.setUserUserDetail(null);
	 * 
	 * return commonMilestone; }
	 * 
	 * public List<CommonTasks> getCommonTasks1() { return this.commonTasks1; }
	 * 
	 * public void setCommonTasks1(List<CommonTasks> commonTasks1) {
	 * this.commonTasks1 = commonTasks1; }
	 * 
	 * public CommonTasks addCommonTasks1(CommonTasks commonTasks1) {
	 * getCommonTasks1().add(commonTasks1); commonTasks1.setUserUserDetail1(this);
	 * 
	 * return commonTasks1; }
	 * 
	 * public CommonTasks removeCommonTasks1(CommonTasks commonTasks1) {
	 * getCommonTasks1().remove(commonTasks1);
	 * commonTasks1.setUserUserDetail1(null);
	 * 
	 * return commonTasks1; }
	 * 
	 * public List<CommonTasks> getCommonTasks2() { return this.commonTasks2; }
	 * 
	 * public void setCommonTasks2(List<CommonTasks> commonTasks2) {
	 * this.commonTasks2 = commonTasks2; }
	 * 
	 * public CommonTasks addCommonTasks2(CommonTasks commonTasks2) {
	 * getCommonTasks2().add(commonTasks2); commonTasks2.setUserUserDetail2(this);
	 * 
	 * return commonTasks2; }
	 * 
	 * public CommonTasks removeCommonTasks2(CommonTasks commonTasks2) {
	 * getCommonTasks2().remove(commonTasks2);
	 * commonTasks2.setUserUserDetail2(null);
	 * 
	 * return commonTasks2; }
	 * 
	 * public List<CustomerDetails> getCustomerDetails1() { return
	 * this.customerDetails1; }
	 * 
	 * public void setCustomerDetails1(List<CustomerDetails> customerDetails1) {
	 * this.customerDetails1 = customerDetails1; }
	 * 
	 * public CustomerDetails addCustomerDetails1(CustomerDetails customerDetails1)
	 * { getCustomerDetails1().add(customerDetails1);
	 * customerDetails1.setUserUserDetail1(this);
	 * 
	 * return customerDetails1; }
	 * 
	 * public CustomerDetails removeCustomerDetails1(CustomerDetails
	 * customerDetails1) { getCustomerDetails1().remove(customerDetails1);
	 * customerDetails1.setUserUserDetail1(null);
	 * 
	 * return customerDetails1; }
	 * 
	 * public List<CustomerDetails> getCustomerDetails2() { return
	 * this.customerDetails2; }
	 * 
	 * public void setCustomerDetails2(List<CustomerDetails> customerDetails2) {
	 * this.customerDetails2 = customerDetails2; }
	 * 
	 * public CustomerDetails addCustomerDetails2(CustomerDetails customerDetails2)
	 * { getCustomerDetails2().add(customerDetails2);
	 * customerDetails2.setUserUserDetail2(this);
	 * 
	 * return customerDetails2; }
	 * 
	 * public CustomerDetails removeCustomerDetails2(CustomerDetails
	 * customerDetails2) { getCustomerDetails2().remove(customerDetails2);
	 * customerDetails2.setUserUserDetail2(null);
	 * 
	 * return customerDetails2; }
	 * 
	 * public List<JobDetails> getJobDetails1() { return this.jobDetails1; }
	 * 
	 * public void setJobDetails1(List<JobDetails> jobDetails1) { this.jobDetails1 =
	 * jobDetails1; }
	 * 
	 * public JobDetails addJobDetails1(JobDetails jobDetails1) {
	 * getJobDetails1().add(jobDetails1); jobDetails1.setUserUserDetail1(this);
	 * 
	 * return jobDetails1; }
	 * 
	 * public JobDetails removeJobDetails1(JobDetails jobDetails1) {
	 * getJobDetails1().remove(jobDetails1); jobDetails1.setUserUserDetail1(null);
	 * 
	 * return jobDetails1; }
	 * 
	 * public List<JobDetails> getJobDetails2() { return this.jobDetails2; }
	 * 
	 * public void setJobDetails2(List<JobDetails> jobDetails2) { this.jobDetails2 =
	 * jobDetails2; }
	 * 
	 * public JobDetails addJobDetails2(JobDetails jobDetails2) {
	 * getJobDetails2().add(jobDetails2); jobDetails2.setUserUserDetail2(this);
	 * 
	 * return jobDetails2; }
	 * 
	 * public JobDetails removeJobDetails2(JobDetails jobDetails2) {
	 * getJobDetails2().remove(jobDetails2); jobDetails2.setUserUserDetail2(null);
	 * 
	 * return jobDetails2; }
	 * 
	 * public List<JobDetails> getJobDetails3() { return this.jobDetails3; }
	 * 
	 * public void setJobDetails3(List<JobDetails> jobDetails3) { this.jobDetails3 =
	 * jobDetails3; }
	 * 
	 * public JobDetails addJobDetails3(JobDetails jobDetails3) {
	 * getJobDetails3().add(jobDetails3); jobDetails3.setUserUserDetail3(this);
	 * 
	 * return jobDetails3; }
	 * 
	 * public JobDetails removeJobDetails3(JobDetails jobDetails3) {
	 * getJobDetails3().remove(jobDetails3); jobDetails3.setUserUserDetail3(null);
	 * 
	 * return jobDetails3; }
	 * 
	 * public List<JobEstimateRI> getJobEstimateRis1() { return
	 * this.jobEstimateRis1; }
	 * 
	 * public void setJobEstimateRis1(List<JobEstimateRI> jobEstimateRis1) {
	 * this.jobEstimateRis1 = jobEstimateRis1; }
	 * 
	 * public JobEstimateRI addJobEstimateRis1(JobEstimateRI jobEstimateRis1) {
	 * getJobEstimateRis1().add(jobEstimateRis1);
	 * jobEstimateRis1.setUserUserDetail1(this);
	 * 
	 * return jobEstimateRis1; }
	 * 
	 * public JobEstimateRI removeJobEstimateRis1(JobEstimateRI jobEstimateRis1) {
	 * getJobEstimateRis1().remove(jobEstimateRis1);
	 * jobEstimateRis1.setUserUserDetail1(null);
	 * 
	 * return jobEstimateRis1; }
	 * 
	 * public List<JobEstimateRI> getJobEstimateRis2() { return
	 * this.jobEstimateRis2; }
	 * 
	 * public void setJobEstimateRis2(List<JobEstimateRI> jobEstimateRis2) {
	 * this.jobEstimateRis2 = jobEstimateRis2; }
	 * 
	 * public JobEstimateRI addJobEstimateRis2(JobEstimateRI jobEstimateRis2) {
	 * getJobEstimateRis2().add(jobEstimateRis2);
	 * jobEstimateRis2.setUserUserDetail2(this);
	 * 
	 * return jobEstimateRis2; }
	 * 
	 * public JobEstimateRI removeJobEstimateRis2(JobEstimateRI jobEstimateRis2) {
	 * getJobEstimateRis2().remove(jobEstimateRis2);
	 * jobEstimateRis2.setUserUserDetail2(null);
	 * 
	 * return jobEstimateRis2; }
	 * 
	 * public List<JobRIAssignment> getJobRiAssignments() { return
	 * this.jobRiAssignments; }
	 * 
	 * public void setJobRiAssignments(List<JobRIAssignment> jobRiAssignments) {
	 * this.jobRiAssignments = jobRiAssignments; }
	 * 
	 * public JobRIAssignment addJobRiAssignment(JobRIAssignment jobRiAssignment) {
	 * getJobRiAssignments().add(jobRiAssignment);
	 * jobRiAssignment.setUserUserDetail(this);
	 * 
	 * return jobRiAssignment; }
	 * 
	 * public JobRIAssignment removeJobRiAssignment(JobRIAssignment jobRiAssignment)
	 * { getJobRiAssignments().remove(jobRiAssignment);
	 * jobRiAssignment.setUserUserDetail(null);
	 * 
	 * return jobRiAssignment; }
	 */

	/*
	 * public List<UserUserRoles> getUserUserRoles() { return this.userUserRoles; }
	 * 
	 * public void setUserUserRoles(List<UserUserRoles> userUserRoles) {
	 * this.userUserRoles = userUserRoles; }
	 */

	/**
	 * MEthod to get admin access.
	 * @return-admin access as String.
	 */
	public String getAdminAccess() {
		return adminAccess;
	}

	/**
	 * Method to set admin access.
	 * @param adminAccess-the admin access String.
	 */
	public void setAdminAccess(String adminAccess) {
		this.adminAccess = adminAccess;
	}

	/**
	 * Method to get time tracking access.
	 * @return-time tracking access as String.
	 */
	public String getTimeTrackingAccess() {
		return timeTrackingAccess;
	}

	/**
	 * Method to set time tracking access.
	 * @param timeTrackingAccess-the time tracking access String.
	 */
	public void setTimeTrackingAccess(String timeTrackingAccess) {
		this.timeTrackingAccess = timeTrackingAccess;
	}

	/**
	 * Method to get updated by.
	 * @return-updated by as Integer.
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set updated by.
	 * @param updatedBy-the updated by Integer.
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get updated date and time.
	 * @return-updated date and time as Timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set updated date and time.
	 * @param updatedDatetime-the updated date and time Timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get authentication.
	 * @return-authentication as User authentication.
	 */
	public UserAuthentication getAuthentication() {
		return authentication;
	}

	/**
	 * Method to set authentication. 
	 * @param authentication-the authentication User authentication.
	 */
	public void setAuthentication(UserAuthentication authentication) {
		this.authentication = authentication;
	}

	/**
	 * Method to get user KIP's.
	 * @return-user KPI's as String.
	 */
	public String getUserKpis() {
		return userKpis;
	}

	/**
	 * Method to set user KPI's.
	 * @param userKpis-the user KPI's String.
	 */
	public void setUserKpis(String userKpis) {
		this.userKpis = userKpis;
	}

	/**
	 * Method to get privacy policy.
	 * @return-privacy policy as String.
	 */
	public String getPrivacyPolicy() {
		return privacyPolicy;
	}

	/**
	 * Method to set privacy policy.
	 * @param privacyPolicy-the privacy policy String.
	 */
	public void setPrivacyPolicy(String privacyPolicy) {
		this.privacyPolicy = privacyPolicy;
	}

	/**
	 * Method to get FCM token.
	 * @return-FCM token as String.
	 */
	public String getFcmToken() {
		return fcmToken;
	}

	/**
	 * Method to set FCM token.
	 * @param fcmToken-the FCM token String.
	 */
	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	/**
	 * Method to get mobile device FCM token.
	 * @return-mobile device FCM token as String.
	 */
	public String getMobileDeviceFcmToken() {
		return mobileDeviceFcmToken;
	}

	/**
	 * Method to set mobile device FCM token.
	 * @param mobileDeviceFcmToken-the mobile device FCM token String.
	 */
	public void setMobileDeviceFcmToken(String mobileDeviceFcmToken) {
		this.mobileDeviceFcmToken = mobileDeviceFcmToken;
	}

	/**
	 * Method to get mobile device OS.
	 * @return-mobile device OS as String.
	 */
	public String getMobileDeviceOs() {
		return mobileDeviceOs;
	}

	/**
	 * Method to set mobile device OS.
	 * @param mobileDeviceOs-the mobile device OS String.
	 */
	public void setMobileDeviceOs(String mobileDeviceOs) {
		this.mobileDeviceOs = mobileDeviceOs;
	}
}