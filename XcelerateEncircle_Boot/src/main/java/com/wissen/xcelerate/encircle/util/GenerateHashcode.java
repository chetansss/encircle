/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.util;

import java.util.Random;

/**
 * class for generating Hash code.
 */
public class GenerateHashcode {
	
	/**
	 * Method to generate hash code.
	 * @return-hash code as Integer.
	 */
	public static int gerenateHashCode() {
		Random rand = new Random();
		return 100000 + rand.nextInt(900000);
	}
}