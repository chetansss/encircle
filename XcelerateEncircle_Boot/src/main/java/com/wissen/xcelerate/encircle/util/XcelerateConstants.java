/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.util;

/**
 * Interface for Xcelerate constants.
 */
public interface XcelerateConstants {
	public String LOGIN_SUCCESS = "Login Successfull!";
	public String LOGIN_FAILURE1 = "User Id does not exist! Please enter the correct username.";
	public String LOGIN_FAILURE2 = "Incorrect Username/Password";
	public String LOGIN_FAILURE3 = "User Id is \"deactivated\" in the system";
	public String LOGIN_VALIDATION_ERROR1 = "Username is empty";
	public String LOGIN_VALIDATION_ERROR2 = "Password is empty";
	public String SAVE_USER = "User details saved successfully!";
	public String SUCCESS = "success";
	public String FAILURE = "failure";
	public String SAVE_USER_FAILURE1 = "Username already exists ! Please provide different username!";
	public String SAVE_ROLE_SUCCESS = "Role saved successfully!";
	public String ROLE_VALIDATION_ERROR = "Role already exist!";
	public String MASTER_NUMBER = "Master Number";
	public String JOB = "Job";
	public String SAVE_MASTER_NUMBER = "Master Number Saved Successfully!";
	public String FORM_URL = "http://faisedev01.eastus.cloudapp.azure.com:9000/#/Login";
	public String STATUS_ACTIVE = "Active";
	public String STATUS_IN_ACTIVE = "In-Active";
	public String INSURANCE_COMPANY = "Insurance Company";
	public String CUSTOMER_COMPANY = "Customer Company";
	public String SAVE_EXTERNAL_AGENCY = "Organization Details Saved Successfully!";
	public String ADJUSTER = "Adjuster";
	public String PROGRAM_TYPE = "Program Type";
	public String YEAR_BUILT = "Year Built";
	public String CAUSE_OF_LOSS = "Cause of Loss";
	public String PROJECT_MANAGER = "Project Manager";
	public String PROGRAM_COORDINATOR = "Program Coordinator";
	public String SALES = "Sales Representative";
	public String ESTIMATOR = "Estimator";
	public String STATUS_NEW = "New";
	public String SAVE_NOTES = "Notes added successfully!";
	public String MAST_NUM_PREFIX = "M";
	public String JOB_PREFIX = "FN";
	public String TASK_PREFIX = "T#";
	public String TASK_PACK_PREFIX = "TP";
	public String INSURANCE_AGENT = "Insurance Agent";
	public String TPA = "Third Party Administrator";
	public String ORG_TYPE_TPA = "TPA";
	public String DEFAULT_TIMEZONE = "US/Eastern";
	public String SCHEMA = "tenant1";
	public String ACCESS_YES = "Y";
	public String ACCESS_NO = "N";
	public String MASTER_COPY_YES = "Y";
	public String MASTER_COPY_NO = "N";
	public String IS_INSURED_YES = "Y";
	public String IS_INSURED_NO = "N";
	public String BILLING_ADDR_SAME = "Y";
	public String BILLING_ADDR_DIFFERENT = "N";
	public String NOT_AUTHORIZED = "you are not authorized to view this page";
	public String ONE_TIME_TRUE = "True";
	public String ONE_TIME_FALSE = "False";
	public String SAVE_JOB_DETAILS = "Job details saved successfully!";
	public String CUSTOMER = "Customer";
	public String PROPERTY_MANAGER = "Property Manager";
	public String COMPANY = "Company";
	public String ALL_LOCATIONS = "*";
	public String SUPERVISOR = "Supervisor";
	public String COORDINATOR = "Coordinator";
	public String FINANCE = "Finance";
	public String SOURCE = "xcelerate";
	public String FILE_TYPE_DOCUMENT = "Document";
	public String FILE_TYPE_PICTURE = "Picture";
	public String FILE_TYPE_ESTIMATE = "Estimate";
	public String SAVE_COMPANY_FAILURE1 = "Company already exist!";
	public String STATUS_DELETED = "Deleted";
	public String TASK_STATUS_INPROGRESS = "In Progress";
	public String SOURCE_MOBILE = "Mobile";
	public String JOB_STATUS_SALES = "Sales";
	public String JOB_STATUS_ESTIMATING = "Estimating";
	public String JOB_STATUS_INVOICING = "Invoicing";
	public String JOB_STATUS_RECEIVABLES = "Receivables";
	public String JOB_STATUS_PLANNING = "Planning";
	public String JOB_STATUS_PRODUCTION = "Production";
	public String JOB_STATUS_CLOSED = "Closed";
	public String JOB_STATUS_REJECTED = "Rejected";
	public String JOB_STATUS_REOPEN = "Reopen";
	public String JOB_STATUS_WARRANTY = "Warranty";
	public String TASK_STATUS_OPEN = "Open";
	public String TASK_STATUS_TODO = "To Do";
	public String DIVISION = "Division";
	public String USER = "User";
	public String ROLE = "Role";
	public String MIN_CREATED_DATE = "2016-12-31 23:59:59";
	public String MAST_NUM_STATUS_OPEN = "Open";
	public String FORM_SOURCE = "Xcelerate";
	public int REGULAR_WORK_MINUTES = 480;
	public String NPS_URL = "https://prodservices.xceleraterestoration.com:8082/xcelerate/nps";
	public String REFERRED_SOURCE = "Referred Source";
	public String SUPPORT_EMAIL = "support@xlrestorationsoftware.com";
	public String ORG_TYPE_INSURANCE = "INSURANCE";
	public String HOUR_TYPE_REGULAR = "Regular";
	public String HOUR_TYPE_OT = "Overtime";
	public String LABOUR_TYPE_TRAVEL = "Travel Time";
	public String LABOUR_TYPE_LABOR = "Labor";
	public String COMPLETED = "Completed";
	public String NOT_COMPLETED = "Not Completed";
	public String JOB_LIST_ACTIVE = "Active";
	public String JOB_LIST_MY = "My";
	public String JOB_LIST_ALL = "All";
	public String MY_OPEN_ACTIVITY = "My Open";
	public String ALL_OPEN_ACTIVITY = "All Open";
	public String ALL_ORGANIZATIONS = "All";
	public String MY_ORGANIZATIONS = "My";
	public String ALL_CONTACTS = "All";
	public String MY_CONTACTS = "My";
	public String ASSIGN_TECHNICIAN = "Technicians are Saved Successfully";
	public String SCRATCH_PAD = "Scratch Pad Saved Successfully";
	public String ARRIVAL_TIME = "Arrival Time Saved Successfully";
	public String TYPE_OF_VISIT = "Type Of Visit Saved Successfully";
	public String EXPECTED_DURATION = "Expected Duration Saved Successfully";
	public String FLAG_YES = "Yes";
	public String FLAG_NO = "No";
	public String JOB_STATUS_ESTIMATING_INITIAL = "Estimating (Initial)";
	public String JOB_STATUS_ESTIMATING_FINAL = "Estimating (Final)";
	public String JOB_STATUS_PAID = "Paid In Full";
	public String CUSTOMER_ORGANIZATION = "Customer Organization";
	public String SOURCE_CATEGORY_FORMS = "Forms";
	public String DEFAULT_SENDER = "service@xlrestorationsoftware.com";
	public String DOC_CATEGORY_WORK_AUTH = "Work Authorization";
	public String DOC_CATEGORY_COC = "COC";
	public String HOUR_TYPE_VACATION = "Vacation";
	public String HOUR_TYPE_SICK = "Sick";
	public String PAYROLL_EXIST = "Record already exist for the selected dates";
	public String SAVE_PAYROLL = "Payroll Saved Successfully";
	public String UPDATE_PAYROLL = "Payroll Updated Successfully";
	public String SAVE_TIME_OFF = "Time Off Saved Successfully";
	public String SOURECE_WEB = "Web";
	public String DUPLICATE_TIME_OFF = "Record already exist for the selected date, please update existing entry";
	public String UPDATE_TIME_OFF = "Time Off Updated Successfully";
	public String UPDATE_USER_CLOCKOUT = "Clock out details updated successfully";
	public String SCREEN_SCHEDULE = "Schedule";
	public String MOBILE_OS_IOS = "ios";
	public String MOBILE_OS_ANDROID = "android";
	public String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
	public String FIREBASE_SERVER_KEY = "AAAAYKaBKj4:APA91bESMNzxZMzorABpORHxpLS-xEzRWLEDze084B5HLS8F8X0udqWQS5beU0zHyBIpSGNhlC9OcRUBUoSxVMTKKo7LW8mOxnDHc88bHNjlAXbyADIbRjbu4_OSVtP8cO14ATDyjhm8";

	// public String
	// FIREBASE_SERVER_KEY_IOS="AAAA5y1mxVo:APA91bErHBSVVGj9O47I-J9pszB8L15-jyZsH0rNZu47_nCoJLKMAJ-SkiDitnaGRHY2aGzbJNkwLc-truZLz0ARZhZLBbvLJKBUl_ZLBCWO8igEM5nGcBJagsS37sOf5XFuI1O3w0yh";
	public String FIREBASE_SERVER_KEY_IOS = "AAAAsMo91w4:APA91bH7RUvLxEpKVnCibK1JZUylkZItH4k0C_vPfjUTGe7FpA_6inMMlXJQmfps9kFu6Mpn1EkY68zlG5eguJYsCCX0BKd1bXQpZHZOouiNUrdgFcD6YEawz0p24qG8lu45cZULvfSP";
	public String PICTURE_URL = "https://prodservices.xceleraterestoration.com:8082/xcelerate/file/displayPicture/";
	public String ENCIRCLE_UAT_HOST_URL = "demoencircle.xceleraterestoration.com";
}