package com.wissen.xcelerate.encircle.repository;

/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.wissen.xcelerate.encircle.model.CommonFileDetails;

/**
 * Repository class for common file detail list of find By Source Category And SourceId , 
 * find By Source Category And SourceId And FileName ,
 * find By Source Category And Source Id And FileType, find Job Documents,
 * find Job Documents For Technicians ,
 * find By Source Category And Source Id And File Type And Source,find Encircle Url Details
 */
public interface CommonFileDetailsRepository extends Repository<CommonFileDetails, Long> {

	List<CommonFileDetails> findBySourceCategoryAndSourceId(@Param("sourceCategory") String sourceCategory,
			@Param("sourceId") int sourceId);

	CommonFileDetails findBySourceCategoryAndSourceIdAndFileName(@Param("sourceCategory") String sourceCategory,
			@Param("sourceId") int sourceId, @Param("fileName") String fileName);

	List<CommonFileDetails> findBySourceCategoryAndSourceIdAndFileType(@Param("sourceCategory") String sourceCategory,
			@Param("sourceId") int sourceId, @Param("fileType") String fileType);

	@Query("SELECT c FROM CommonFileDetails c where c.sourceCategory='Job' and c.status='Active' and c.sourceId=:jobId"
			+ " and c.fileType IN('Document','Estimate')")
	List<CommonFileDetails> findJobDocuments(@Param("jobId") int jobId);

	@Query("SELECT c FROM CommonFileDetails c where c.sourceCategory='Job' and c.status='Active' and c.sourceId=:jobId and "
			+ "c.fileType='Document' and c.fileCategory NOT IN('Estimate','Invoice')")
	List<CommonFileDetails> findJobDocumentsForTechnicians(@Param("jobId") int jobId);

	CommonFileDetails findByFileId(@Param("fileId") int fileId);

	

	void delete(CommonFileDetails commonFileDetails);

	List<CommonFileDetails> findBySourceCategoryAndSourceIdAndFileTypeAndSource(
			@Param("sourceCategory") String sourceCategory, @Param("sourceId") int sourceId,
			@Param("fileType") String fileType, @Param("source") String source);

	CommonFileDetails save(CommonFileDetails commonFileDetails);

	@Query("SELECT c FROM CommonFileDetails c where c.sourceCategory=:sourceCategory and c.sourceId=:sourceId  "
			+ "and c.fileName=:fileName and c.source like %:source% and c.status='Active' ")
	CommonFileDetails findEncircleUrlDetails(@Param("sourceCategory") String sourceCategory,
			@Param("sourceId") int sourceId, @Param("fileName") String fileName, @Param("source") String source);

}
