/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the common_file_details database table.
 */
@Entity
@Table(name="common_file_details")

/**
 *@NamedQuery(name="CommonFileDetails.findAll", query="SELECT c FROM CommonFileDetails c")
 */
public class CommonFileDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="file_id")
	private int fileId;

	@Column(name="file_category")
	private String fileCategory;

	@Column(name="file_comments")
	private String fileComments;

	@Column(name="file_hash")
	private String fileHash;

	@Column(name="file_name")
	private String fileName;

	@Column(name="file_path")
	private String filePath;

	@Column(name="file_sub_category")
	private String fileSubCategory;

	@Column(name="folder_id")
	private Integer folderId;

	@Column(name="internal_file_name")
	private String internalFileName;
	
	@Column(name="display_name")
	private String displayName;
	
	@Column(name="file_type")
	private String fileType;

	@Column(name="is_public")
	private String isPublic;

	@Column(name="source_category")
	private String sourceCategory;

	@Column(name="source_id")
	private Integer sourceId;
	
	@Column(name="source_sub_category")
	private String sourceSubCategory;
	
	@Column(name="sub_source_id")
	private Integer subSourceId;

	private String status;

	@Column(name="uploaded_by")
	private int uploadedBy;

	@Column(name="uploaded_datetime")
	private Timestamp uploadedDatetime;
	
	private String source;
	
	@Column(name="file_tag")
	private String fileTag;
	
	@Column(name="thumbnail")
	private String thumbnail;
	
   /**
    * Method for Common File Details.
    */
	public CommonFileDetails() {
	}

	/**
	 * Method to get File Id.
	 * @return file id as Integer.
	 */
	public int getFileId() {
		return fileId;
	}

	/**
	 * Method to set File Id.
	 * @param  file Id-the file id is integer.
	 */
	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	/**
	 * Method to get File Category.
	 * @return File Category as string.
	 */
	public String getFileCategory() {
		return fileCategory;
	}

	/**
	 * Method to set File Category.
	 * @param  File Category-the File Category is string.
	 */
	public void setFileCategory(String fileCategory) {
		this.fileCategory = fileCategory;
	}

	/**
	 * Method to get File comments.
	 * @return File comments as string.
	 */
	public String getFileComments() {
		return fileComments;
	}

	/**
	 * Method to set File Comments.
	 * @param  File Comments-the File Comments is string.
	 */
	public void setFileComments(String fileComments) {
		this.fileComments = fileComments;
	}

	/**
	 * Method to get File hash.
	 * @return File hash as string.
	 */
	public String getFileHash() {
		return fileHash;
	}

	/**
	 * Method to set File Hash.
	 * @param  File Hash-the File  Hash is string.
	 */
	public void setFileHash(String fileHash) {
		this.fileHash = fileHash;
	}

	/**
	 * Method to get File name.
	 * @return File name as string.
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Method to set File Name.
	 * @param  File Name-the File Name is string.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Method to get File path.
	 * @return File path as string.
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * Method to set file Path.
	 * @param  File path-the File path is string.
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * Method to get File SubCategory.
	 * @return File SubCategory as string.
	 */
	public String getFileSubCategory() {
		return fileSubCategory;
	}

	/**
	 * Method to set file Sub Category.
	 * @param  File Sub Category-the File Sub Category is string.
	 */
	public void setFileSubCategory(String fileSubCategory) {
		this.fileSubCategory = fileSubCategory;
	}

	/**
	 * Method to get FolderId.
	 * @return FFolderId as integer.
	 */
	public Integer getFolderId() {
		return folderId;
	}

	/**
	 * Method to set FolderId.
	 * @param  FolderId-the FolderId is integer.
	 */
	public void setFolderId(Integer folderId) {
		this.folderId = folderId;
	}

	/**
	 * Method to get Internal FileName.
	 * @return Internal FileName as string.
	 */
	public String getInternalFileName() {
		return internalFileName;
	}

	/**
	 * Method to set Internal File Name.
	 * @param  Internal File Name-the Internal File Name is string.
	 */
	public void setInternalFileName(String internalFileName) {
		this.internalFileName = internalFileName;
	}

	/**
	 * Method to get Display Name.
	 * @return DisplayName as string.
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Method to set Display Name.
	 * @param  Display Name-the Display Name is string.
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Method to get File Type.
	 * @return FileType as string.
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * Method to set File Type.
	 * @param  File Type-the File Type is string.
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * Method to get Is Public.
	 * @return IsPublic as string.
	 */
	public String getIsPublic() {
		return isPublic;
	}

	/**
	 * Method to set Is Public.
	 * @param  Is Public-the Is Public is string.
	 */
	public void setIsPublic(String isPublic) {
		this.isPublic = isPublic;
	}

	/**
	 * Method to get Source Category.
	 * @return Source Category as string.
	 */
	public String getSourceCategory() {
		return sourceCategory;
	}

	/**
	 * Method to set Source Category.
	 * @param  Source Category-the Source Category is string.
	 */
	public void setSourceCategory(String sourceCategory) {
		this.sourceCategory = sourceCategory;
	}

	/**
	 * Method to get SourceId.
	 * @return SourceId as integer.
	 */
	public Integer getSourceId() {
		return sourceId;
	}

	/**
	 * Method to set Source id.
	 * @param  Source id-the Source id is integer.
	 */
	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * Method to get Source SubCategory.
	 * @return Source SubCategory as string.
	 */
	public String getSourceSubCategory() {
		return sourceSubCategory;
	}

	/**
	 * Method to set Source Sub Category.
	 * @param  Source Sub Category-the Source Sub Category is string.
	 */
	public void setSourceSubCategory(String sourceSubCategory) {
		this.sourceSubCategory = sourceSubCategory;
	}

	/**
	 * Method to get SubSourceId.
	 * @return SubSourceId as integer.
	 */
	public Integer getSubSourceId() {
		return subSourceId;
	}

	/**
	 * Method to set Sub Source Id.
	 * @param  SubSource Id-the Sub Source Id is integer.
	 */
	public void setSubSourceId(Integer subSourceId) {
		this.subSourceId = subSourceId;
	}

	/**
	 * Method to get Source Status.
	 * @return Status as string.
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Method to set Status.
	 * @param  Status-the Status is string.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Method to get UploadedBy.
	 * @return UploadedBy as integer.
	 */
	public int getUploadedBy() {
		return uploadedBy;
	}

	/**
	 * Method to set Uploaded By.
	 * @param UploadedBy-the UploadedBy is integer.
	 */
	public void setUploadedBy(int uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	/**
	 * Method to get Uploaded Datetime.
	 * @return Uploaded Datetime as timestamp.
	 */
	public Timestamp getUploadedDatetime() {
		return uploadedDatetime;
	}

	/**
	 * Method to set Uploaded Datetime.
	 * @param Uploaded Datetime-the Uploaded Datetime is timestamp.
	 */
	public void setUploadedDatetime(Timestamp uploadedDatetime) {
		this.uploadedDatetime = uploadedDatetime;
	}

	/**
	 * Method to get source.
	 * @return source as string.
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Method to source.
	 * @param  source-the source is string.
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Method to get FileTag.
	 * @return FileTag as string.
	 */
	public String getFileTag() {
		return fileTag;
	}

	/**
	 * Method to set fileTag.
	 * @param  fileTag-the fileTag is string.
	 */
	public void setFileTag(String fileTag) {
		this.fileTag = fileTag;
	}

	/**
	 * Method to get Thumbnail.
	 * @return Thumbnail as string.
	 */
	public String getThumbnail() {
		return thumbnail;
	}

	/**
	 * Method to set Thumbnail.
	 * @param  Thumbnail-the thumbnail is string.
	 */
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
}
