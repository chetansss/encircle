/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model.common;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;

/**
 * The persistent class for the user_user_authentication database table.
 */
@Entity
@Table(name="user_user_authentication",schema="common")
//@NamedQuery(name="UserUserAuthentication.findAll", query="SELECT u FROM UserUserAuthentication u")
public class UserUserAuthentication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_authentication_id")
	private int userAuthenticationId;

	@Column(name="last_signin_date")
	private String lastSigninDate;

	private String password;

	@Column(name="restoration_company_id")
	private int restorationCompanyId;

	@Column(name="signin_attempts")
	private Integer signinAttempts;

	@Column(name="user_id")
	private int userId;

	@Column(name="user_name")
	private String userName;
	
	@Column(name="updated_by")
	private Integer updatedBy;

	@Column(name="updated_datetime")
	private Timestamp updatedDatetime;

	//bi-directional many-to-one association to UserUserDetails
	@ManyToOne
	@JoinColumn(name="user_detail_id")
	private UserUserDetails userUserDetail;
	
	@Column(name="old_password")
	private String oldPassword;

	/**
	 * Method to User Authentication
	 */
	public UserUserAuthentication() {
	}

	/**
	 * Method to get User AuthenticationId.
	 * @return User AuthenticationId as integer.
	 */
	public int getUserAuthenticationId() {
		return this.userAuthenticationId;
	}

	/**
	 * Method to set User AuthenticationId.
	 * @param  User AuthenticationId-the User AuthenticationId is integer
	 */
	public void setUserAuthenticationId(int userAuthenticationId) {
		this.userAuthenticationId = userAuthenticationId;
	}

	/**
	 * Method to get Last SigninDate.
	 * @return Last SigninDate as string.
	 */
	public String getLastSigninDate() {
		return this.lastSigninDate;
	}

	/**
	 * Method to set Last SigninDate.
	 * @param  Last SigninDate-the Last SigninDate is string
	 */
	public void setLastSigninDate(String lastSigninDate) {
		this.lastSigninDate = lastSigninDate;
	}

	/**
	 * Method to get Password.
	 * @return Password as string.
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Method to set Password.
	 * @param  Password-the Password is string
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	
	/**
	 * Method to get Restoration CompanyId
	 * @return Restoration CompanyId as integer.
	 */
	public int getRestorationCompanyId() {
		return this.restorationCompanyId;
	}

	/**
	 * Method to set Restoration CompanyId.
	 * @param  Restoration CompanyId-the Restoration CompanyId is integer
	 */
	public void setRestorationCompanyId(int restorationCompanyId) {
		this.restorationCompanyId = restorationCompanyId;
	}

	/**
	 * Method to get Signin Attempts
	 * @return Signin Attempts as integer.
	 */
	public Integer getSigninAttempts() {
		return this.signinAttempts;
	}

	/**
	 * Method to set Signin Attempts.
	 * @param  Signin Attempts-the Signin Attempts is integer
	 */
	public void setSigninAttempts(Integer signinAttempts) {
		this.signinAttempts = signinAttempts;
	}

	/**
	 * Method to get UserId .
	 * @return UserId  as integer.
	 */
	public int getUserId() {
		return this.userId;
	}

	/**
	 * Method to set UserId.
	 * @param  UserId-the UserId  is integer.
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Method to get UserName .
	 * @return UserName  as string.
	 */
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Method to set UserName.
	 * @param  UserName-the UserName is string.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Method to get Updated By .
	 * @return UpdatedBy  as integer.
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set UpdatedBy.
	 * @param  UpdatedBy-the UpdatedBy is string.
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get Updated Datetime.
	 * @return Updated Datetime  as timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	/**
	 * Method to set Updated Datetime.
	 * @param  Updated Datetime-the Updated Datetime is timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 * Method to get User UserDetail.
	 * @return User UserDetail as User UserDetail.
	 */
	public UserUserDetails getUserUserDetail() {
		return this.userUserDetail;
	}

	/**
	 * Method to set User UserDetail.
	 * @param  User UserDetail-the User UserDetail is User UserDetail.
	 */
	public void setUserUserDetail(UserUserDetails userUserDetail) {
		this.userUserDetail = userUserDetail;
	}

	/**
	 * Method to get Old Password.
	 * @return Old Password as string.
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * Method to set Old Password.
	 * @param   Old Password-the  Old Password is string.
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	
}