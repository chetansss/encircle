package com.wissen.xcelerate.encircle.repository;

/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.wissen.xcelerate.encircle.model.JobKpiDetails;

/**
 * Repository class for job kpi details list of job kpi details repository job kpi detail findAll,find By KpiId,
 * find By job Id And Kpi Value Greater Than Equal Order By KpiId,findBy Kpi Name,
 * find Average Kpi Values,find My Kpi Values,find Average KpiValues By Current Month,
 * find Average Kpi ValuesBy Last Month,findMy Kpi Values For Current Month,
 * findMy Kpi Values For Last Month
 * @author Rama
 */
public interface JobKpiDetailsRepository extends Repository<JobKpiDetails, Long> {
	
	List<JobKpiDetails> findAll();
	
	List<JobKpiDetails> findByKpiId(@Param("kpiId") int kpiId);
	
	List<JobKpiDetails> findByjobIdAndKpiValueGreaterThanEqualOrderByKpiId(@Param("jobId") long jobId,@Param("kpiValue") float kpiValue);
	
	List<JobKpiDetails> findByKpiName(@Param("kpiName") String kpiName);
	
	JobKpiDetails save(JobKpiDetails jobKpiDetails); 
	
	void delete(JobKpiDetails jobKpiDetails);
	
	JobKpiDetails findByJobIdAndKpiNameLike(@Param("jobId") long jobId,@Param("kpiName") String kpiName);
	
	@Query(name="KpiAverage",value="select j.kpi_name AS kpiName,avg(j.kpi_value) AS average,avg(j.kpi_target_value)"
			+ " AS target,count(*) AS totalCount,"+
		   "(select count(*) from job_kpi_details k, job_details d where k.job_id=d.job_id and d.job_type=?1 "+
		   "and d.current_job_status NOT IN('Closed','Rejected') and k.kpi_id=j.kpi_id and k.kpi_value>0 and"
		   + " k.kpi_value<=k.kpi_target_value) AS metTarget,j.kpi_id "+
		   "from job_kpi_details j,job_details jd where j.job_id=jd.job_id and jd.current_job_status NOT IN('Closed','Rejected')"
		   + " and jd.job_type=?1 "+
		   "and j.kpi_value>=0 and j.kpi_id IN ?2 group by j.kpi_name,j.kpi_id order by j.kpi_id",nativeQuery=true)
	List<Object[]> findAverageKpiValues(@Param("jobType") String jobType,@Param("kpis") List<Integer> kpis);
	
	@Query(name="KpiAverage",value="select j.kpi_name AS kpiName,avg(j.kpi_value) AS average,avg(j.kpi_target_value) "
			+ "AS target,count(*) AS totalCount,"+
			   "(select count(*) from job_kpi_details k, job_details d where k.job_id=d.job_id and d.job_type=?1 "+
			   "and d.current_job_status NOT IN('Closed','Rejected') and k.kpi_id=j.kpi_id and k.kpi_value>0 and "
			   + "k.kpi_value<=k.kpi_target_value and"
			   + " d.job_id IN(select job_id from job_roles where user_id=?2)) AS metTarget,j.kpi_id "+
			   "from job_kpi_details j,job_details jd where j.job_id=jd.job_id and jd.current_job_status NOT"
			   + " IN('Closed','Rejected') and jd.job_type=?1 "+
			   "and j.kpi_value>=0 and j.kpi_id IN ?3 and j.job_id IN(select job_id from job_roles where user_id=?2)"
			   + " group by j.kpi_name,j.kpi_id order by"
			   + " j.kpi_id",nativeQuery=true)
		List<Object[]> findMyKpiValues(@Param("jobType") String jobType,@Param("userId") int userId,@Param("kpis") List<Integer> kpis);
		
		//Calculate kpi for current month for all
		@Query(name = "KpiAverage", value = "select j.kpi_name AS kpiName,avg(j.kpi_value) AS average,avg(j.kpi_target_value) "
				+ "AS target,count(*) AS totalCount,"
				+ "(select count(*) from job_kpi_details k, job_details d where k.job_id=d.job_id and d.job_type=?1 and "
				+ "k.created_datetime>=DATEFROMPARTS(YEAR(GETDATE()),MONTH(GETDATE()),1)"
				+ "and d.current_job_status NOT IN('Closed','Rejected') and k.kpi_id=j.kpi_id and k.kpi_value>0 and"
				+ " k.kpi_value<=k.kpi_target_value) AS metTarget,j.kpi_id "
				+ "from job_kpi_details j,job_details jd where j.job_id=jd.job_id and jd.current_job_status NOT"
				+ " IN('Closed','Rejected') and jd.job_type=?1 and j.created_datetime>=DATEFROMPARTS(YEAR(GETDATE()),MONTH(GETDATE()),1) "
				+ "and j.kpi_value>0 and j.kpi_id IN ?2 group by j.kpi_name,j.kpi_id order by j.kpi_id", nativeQuery = true)
		List<Object[]> findAverageKpiValuesByCurrentMonth(@Param("jobType") String jobType, @Param("kpis") List<Integer> kpis);

		//Calculate KPI for last month for all
		@Query(name = "KpiAverage", value = "select j.kpi_name AS kpiName,avg(j.kpi_value) AS average,avg(j.kpi_target_value)"
				+ " AS target,count(*) AS totalCount,"
				+ "(select count(*) from job_kpi_details k, job_details d where k.job_id=d.job_id and d.job_type=?1 and "
				+ "k.created_datetime>=DATEADD(DAY,1,EOMONTH(GETDATE(),-2)) and k.created_datetime<DATEFROMPARTS(YEAR(GETDATE()),MONTH(GETDATE()),1) "
				+ "and d.current_job_status NOT IN('Closed','Rejected') and k.kpi_id=j.kpi_id and k.kpi_value>0 and k.kpi_value<=k.kpi_target_value) "
				+ "AS metTarget,j.kpi_id "
				+ "from job_kpi_details j,job_details jd where j.job_id=jd.job_id and jd.current_job_status NOT IN('Closed','Rejected') and "
				+ "jd.job_type=?1  and j.created_datetime>=DATEADD(DAY,1,EOMONTH(GETDATE(),-2)) and j.created_datetime<DATEFROMPARTS(YEAR(GETDATE()),"
				+ "MONTH(GETDATE()),1) "
				+ "and j.kpi_value>0 and j.kpi_id IN ?2 group by j.kpi_name,j.kpi_id order by j.kpi_id", nativeQuery = true)
		List<Object[]> findAverageKpiValuesByLastMonth(@Param("jobType") String jobType, @Param("kpis") List<Integer> kpis);
		
		//Calculate my kpi for current month for all
		@Query(name = "KpiAverage", value = "select j.kpi_name AS kpiName,avg(j.kpi_value) AS average,avg(j.kpi_target_value) AS target,count(*) "
				+ "AS totalCount,"
				+ "(select count(*) from job_kpi_details k, job_details d where k.job_id=d.job_id and d.job_type=?1  and"
				+ " k.created_datetime>=DATEFROMPARTS(YEAR(GETDATE()),MONTH(GETDATE()),1)"
				+ "and d.current_job_status NOT IN('Closed','Rejected') and k.kpi_id=j.kpi_id and k.kpi_value>0 and"
				+ " k.kpi_value<=k.kpi_target_value and d.job_id IN(select job_id from job_roles where user_id=?2)) AS metTarget,j.kpi_id "
				+ "from job_kpi_details j,job_details jd where j.job_id=jd.job_id and jd.current_job_status NOT IN('Closed','Rejected')"
				+ " and jd.job_type=?1 and j.created_datetime>=DATEFROMPARTS(YEAR(GETDATE()),MONTH(GETDATE()),1)"
				+ "and j.kpi_value>0 and j.kpi_id IN ?3 and j.job_id IN(select job_id from job_roles where user_id=?2) "
				+ "group by j.kpi_name,j.kpi_id order by j.kpi_id", nativeQuery = true)
		List<Object[]> findMyKpiValuesForCurrentMonth(@Param("jobType") String jobType, @Param("userId") int userId, @Param("kpis") List<Integer> kpis);
		
		//Calculate KPI for last month for all
		@Query(name = "KpiAverage", value = "select j.kpi_name AS kpiName,avg(j.kpi_value) AS average,avg(j.kpi_target_value) AS "
				+ "target,count(*) AS totalCount,"
				+ "(select count(*) from job_kpi_details k, job_details d where k.job_id=d.job_id and d.job_type=?1 and "
				+ "k.created_datetime>=DATEADD(DAY,1,EOMONTH(GETDATE(),-2)) and k.created_datetime<DATEFROMPARTS(YEAR(GETDATE()),MONTH(GETDATE()),1) "
				+ "and d.current_job_status NOT IN('Closed','Rejected') and k.kpi_id=j.kpi_id and k.kpi_value>0 and k.kpi_value<=k.kpi_target_value and"
				+ " d.job_id IN(select job_id from job_roles where user_id=?2)) AS metTarget,j.kpi_id "
				+ "from job_kpi_details j,job_details jd where j.job_id=jd.job_id and jd.current_job_status NOT IN('Closed','Rejected') "
				+ "and jd.job_type=?1   and j.created_datetime>=DATEADD(DAY,1,EOMONTH(GETDATE(),-2)) and"
				+ " j.created_datetime<DATEFROMPARTS(YEAR(GETDATE()),MONTH(GETDATE()),1) "
				+ "and j.kpi_value>0 and j.kpi_id IN ?3 and j.job_id IN(select job_id from job_roles where user_id=?2)"
				+ " group by j.kpi_name,j.kpi_id order by j.kpi_id", nativeQuery = true)
		List<Object[]> findMyKpiValuesForLastMonth(@Param("jobType") String jobType, @Param("userId") int userId,
				@Param("kpis") List<Integer> kpis);
}
