/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Configuration class for Web security.
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	/**
	 * Method to configure using HTTP.
	 */
    @Override
    public void configure(HttpSecurity  http) throws Exception {
         http.csrf().disable();
         http.cors().and();
    }
}
