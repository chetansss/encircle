/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.hibernate;

import org.hibernate.EmptyInterceptor;

import com.wissen.xcelerate.encircle.tenant.TenantContext;

/**
 * Interceptor class for Connection.
 */
public class ConnectionInterceptor extends EmptyInterceptor {
	private static final long serialVersionUID = 1L;

	/**
	 * Method to create prepare statement.
	 * @param-SQl as String.
	 * @return-prepare statement as String.
	 */
	@Override
	public String onPrepareStatement(String sql) {
		String prepedStatement = super.onPrepareStatement(sql);
		System.out.println("Checking multi tenancy");
		prepedStatement = prepedStatement.replaceAll("tenant1", TenantContext.getCurrentTenant());
		return prepedStatement;
	}
}