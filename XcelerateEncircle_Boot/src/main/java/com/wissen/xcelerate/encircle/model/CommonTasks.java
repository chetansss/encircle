/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or
* disclosure of this software is subject to restrictions set forth in your
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model;


import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
* The persistent class for the common_tasks database table.
*/
@Entity
@Table(name="common_tasks")
//@NamedQuery(name="CommonTasks.findAll", query="SELECT c FROM CommonTasks c")
public class CommonTasks implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="task_id")
    private int taskId;

    @Column(name="actual_end_datetime")
    private Timestamp actualEndDatetime;

    @Column(name="actual_start_datetime")
    private Timestamp actualStartDatetime;

    @Column(name="allowed_end_datetime")
    private Timestamp allowedEndDatetime;

    @Column(name="allowed_start_datetime")
    private Timestamp allowedStartDatetime;

    @Column(name="assigned_to_role")
    private String assignedToRole;

    @Column(name="closed_reason")
    private String closedReason;

    private String comments;

    @Column(name="delay_reason")
    private String delayReason;

    @Column(name="expected_end_datetime")
    private Timestamp expectedEndDatetime;

    @Column(name="expected_start_datetime")
    private Timestamp expectedStartDatetime;

    private String source;

    @Column(name="source_id")
    private Integer sourceId;

    private String status;

    @Column(name="task_desc")
    private String taskDesc;

    @Column(name="task_title")
    private String taskTitle;
    
    @Column(name="task_sequence")
    private Integer taskSequence;

    @Column(name="updated_datetime")
    private Timestamp updatedDatetime;

    @Column(name="assign_type")
    private String assignType;
    
    @Column(name="assigned_to")
    private Integer assignTo;
    
    @Column(name="action_required")
    private String actionRequired;
    
    @Column(name="action_type")
    private String actionType;
    
    @Column(name="action_value")
    private String actionValue;
    
    @Column(name="trigger_event")
    private String triggerEvent;
    
    @Column(name="event_type")
    private String eventType;
    
    @Column(name="rejected_reason")
    private String rejectedReason;

    //bi-directional many-to-one association to UserUserDetails
    @ManyToOne
    @JoinColumn(name="updated_by")
    private UserDetails updatedBy;
    
    @Column(name="time_to_complete")
    private Integer timeToComplete;
    
    @Column(name="time_uom")
    private String timeUom;
    
    @Column(name="previous_status")
    private String previousStatus;

    /**
     * Method to get Source.
     */
    public CommonTasks() {
    }

    /**
     * Method to get Task Id.
     * @return Task Id as integer.
     */
    public int getTaskId() {
        return this.taskId;
    }

    /**
     * Method to set Task Id.
     * @param  Task Id-the Task Id integer.
     */
    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    /**
     * Method to get Actual EndDatetime.
     * @return Actual EndDatetime as timestamp.
     */
    public Timestamp getActualEndDatetime() {
        return this.actualEndDatetime;
    }

    /**
     * Method to set Actual EndDatetime.
     * @param  actual EndDatetime-the actual EndDatetime timestamp.
     */
    public void setActualEndDatetime(Timestamp actualEndDatetime) {
        this.actualEndDatetime = actualEndDatetime;
    }

    /**
     * Method to get Actual StartDatetime.
     * @return Actual StartDatetime as timestamp.
     */
    public Timestamp getActualStartDatetime() {
        return this.actualStartDatetime;
    }

    /**
     * Method to set Actual StartDatetime.
     * @param  actual StartDatetime-the actual StartDatetimetimestamp.
     */
    public void setActualStartDatetime(Timestamp actualStartDatetime) {
        this.actualStartDatetime = actualStartDatetime;
    }

    /**
     * Method to get Allowed EndDatetime.
     * @return Allowed EndDatetime as timestamp.
     */
    public Timestamp getAllowedEndDatetime() {
        return this.allowedEndDatetime;
    }

    /**
     * Method to set allowed end date and time.
     * @param  actual StartDatetime-the allowed date and time Timestamp.
     */
    public void setAllowedEndDatetime(Timestamp allowedEndDatetime) {
        this.allowedEndDatetime = allowedEndDatetime;
    }

    /**
     * Method to get Allowed StartDatetime.
     * @return Allowed StartDatetime as timestamp.
     */
    public Timestamp getAllowedStartDatetime() {
        return this.allowedStartDatetime;
    }


    /**
     * Method to set Allowed start date time.
     * @param allowedStartDatetime-the start date time TImestamp.
     */
	public void setAllowedStartDatetime(Timestamp allowedStartDatetime) {
		this.allowedStartDatetime = allowedStartDatetime;
	}

	/**
	 * Method to get Assigned to role.
	 * @return-assigned time to role as String.
	 */
	public String getAssignedToRole() {
		return this.assignedToRole;
	}

	/**
	 * Method to set Assigned to role.
	 * @param assignedToRole-the assigned to role String.
	 */
	public void setAssignedToRole(String assignedToRole) {
		this.assignedToRole = assignedToRole;
	}

	/**
	 * Method to get Closed reason.
	 * @return-closed reasonas String.
	 */
	public String getClosedReason() {
		return this.closedReason;
	}

	/**
	 * Method to set closed reason.
	 * @param closedReason-the closed reason String.
	 */
	public void setClosedReason(String closedReason) {
		this.closedReason = closedReason;
	}

	/**
	 * Method to get comments.
	 * @return-comments as String.
	 */
	public String getComments() {
		return this.comments;
	}

	/**
	 * Method to set comments.
	 * @param comments-the comments String.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Method to get delay reason.
	 * @return-delay reason as String.
	 */
	public String getDelayReason() {
		return this.delayReason;
	}

	/**
	 * Method to set delay reason.
	 * @param delayReason-the delay reason String.
	 */
	public void setDelayReason(String delayReason) {
		this.delayReason = delayReason;
	}

	/**
	 * Method to get expected end date time. 
	 * @return-expected end date time as Timestamp.
	 */
	public Timestamp getExpectedEndDatetime() {
		return this.expectedEndDatetime;
	}

	/**
	 * Method to set expected date time.
	 * @param expectedEndDatetime-the expected date time Timestamp.
	 */
	public void setExpectedEndDatetime(Timestamp expectedEndDatetime) {
		this.expectedEndDatetime = expectedEndDatetime;
	}

	/**
	 * Method to get Expected start date time.
	 * @return-expected start date time as Timestamp.
	 */
	public Timestamp getExpectedStartDatetime() {
		return this.expectedStartDatetime;
	}

	/**
	 * Method to set Expected start date time. 
	 * @param expectedStartDatetime-the expected date time Timestamp.
	 */
	public void setExpectedStartDatetime(Timestamp expectedStartDatetime) {
		this.expectedStartDatetime = expectedStartDatetime;
	}

	/**
	 * Method to get Source.
	 * @return-source as String.
	 */
	public String getSource() {
		return this.source;
	}

	/**
	 * Method to set source.
	 * @param source-the source String.
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Method to get Source ID.
	 * @return-source ID as Integer.
	 */
	public Integer getSourceId() {
		return this.sourceId;
	}

	/**
	 * Method to set source ID.
	 * @param sourceId-the source ID Integer.
	 */
	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * Method to get status.
	 * @return-status as String.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Method to set status.
	 * @param status-the status String.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Method to get Task description.
	 * @return-task description as String.
	 */
	public String getTaskDesc() {
		return this.taskDesc;
	}

	/**
	 * Method to set task description.
	 * @param taskDesc-the task description String.
	 */
	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}

	/**
	 * Method to get task title.
	 * @return-task title as String.
	 */
	public String getTaskTitle() {
		return this.taskTitle;
	}

	/**
	 * Method to set task title.
	 * @param taskTitle-the task title String.
	 */
	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	/**
	 * Method to get task sequence.
	 * @return-task sequence as Integer.
	 */
	public Integer getTaskSequence() {
		return taskSequence;
	}

	/**
	 * method to set task ssequence.
	 * @param taskSequence-the task sequence Integer.
	 */
	public void setTaskSequence(Integer taskSequence) {
		this.taskSequence = taskSequence;
	}

	/**
	 * Method to get Updated Date and time.
	 * @return-Updated Date and time as Timestamp.
	 */
	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}
    
	/**
	 * Method to set Updated Date and time.
	 * @param updatedDatetime-the Updated Date and time Timestamp.
	 */
	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	/**
	 *  Method to get Assign Type.
	 * @return-Assign Type as String.
	 */
	public String getAssignType() {
		return assignType;
	}

	/**
	 * Method to set Assign type.
	 * @param assignType-the assign type String.
	 */
	public void setAssignType(String assignType) {
		this.assignType = assignType;
	}

	/**
	 * Method to get Assign to.
	 * @return-assign type as Integer.
	 */
	public Integer getAssignTo() {
		return assignTo;
	}

	/**
	 * Method to set assign to. 
	 * @param assignTo-the assign to Integer.
	 */
	public void setAssignTo(Integer assignTo) {
		this.assignTo = assignTo;
	}

	/**
	 * Method to get updated by. 
	 * @return-updated by as User details.
	 */
	public UserDetails getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Method to set updated by. 
	 * @param updatedBy-the updated by User Details.
	 */
	public void setUpdatedBy(UserDetails updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Method to get Action required. 
	 * @return-action required as String.
	 */
	public String getActionRequired() {
		return actionRequired;
	}

	/**
	 * Method to set Action required. 
	 * @param actionRequired-the action required String.
	 */
	public void setActionRequired(String actionRequired) {
		this.actionRequired = actionRequired;
	}

	/**
	 * Method to get Action type. 
	 * @return-action type as String.
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Method to set action type. 
	 * @param actionType-the action type String.
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	 /**
	  * Method to get Action value.
	  * @return-action value as String.
	  */
	public String getActionValue() {
		return actionValue;
	}

	/**
	 * Method to set Action value. 
	 * @param actionValue-the action  value String.
	 */
	public void setActionValue(String actionValue) {
		this.actionValue = actionValue;
	}

	/**
	 * Method to get trigger event. 
	 * @return-trigger event as String.
	 */
	public String getTriggerEvent() {
		return triggerEvent;
	}

	/**
	 * Method to set trigger event.
	 * @param triggerEvent-the trigger event String.
	 */
	public void setTriggerEvent(String triggerEvent) {
		this.triggerEvent = triggerEvent;
	}

	/**
	 * Method to get Event type.
	 * @return-event type as String.
	 */
	public String getEventType() {
		return eventType;
	}

	/**
	 * Method to set event type. 
	 * @param eventType-the event type String.
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	/**
	 * Method to get Rejected reason. 
	 * @return-rejected reason as String.
	 */
	public String getRejectedReason() {
		return rejectedReason;
	}

	/**
	 * Method to set rejected reason. 
	 * @param rejectedReason-the rejected reason String.
	 */
	public void setRejectedReason(String rejectedReason) {
		this.rejectedReason = rejectedReason;
	}

	/**
	 * Method to get time to complete. 
	 * @return-time to complete as Integer.
	 */
	public Integer getTimeToComplete() {
		return timeToComplete;
	}

	/**
	 * Method to set time to complete. 
	 * @param timeToComplete-the time to complete Integer. 
	 */
	public void setTimeToComplete(Integer timeToComplete) {
		this.timeToComplete = timeToComplete;
	}

	/**
	 * Method to get time UOM. 
	 * @return-time UOM as String.
	 */
	public String getTimeUom() {
		return timeUom;
	}

	/**
	 * Method to set time uom as String. 
	 * @param timeUom-the time uom String.
	 */
	public void setTimeUom(String timeUom) {
		this.timeUom = timeUom;
	}

	/**
	 * Method to getprevious status. 
	 * @return-previous status as String.
	 */
	public String getPreviousStatus() {
		return previousStatus;
	}

	/**
	 * Method to set previous status. 
	 * @param previousStatus-the previous status String.
	 */
	public void setPreviousStatus(String previousStatus) {
		this.previousStatus = previousStatus;
	}
}