package com.wissen.xcelerate.encircle.pojo;

import java.io.Serializable;

/**
 * class for Media Webhook.
 */
public class MediaWebhook implements Serializable {

	private static final long serialVersionUID = 1L;
	private String tenant;
	private String event;
	private String propertyClaimId;
	private String filename;
	private String contentType;
	private String downloadUri;
	private String sourceType;
	private String primaryId;
	
	/**
	 * Method to get event.
	 * @return event as string.
	 */
	public String getEvent() {
		return event;
	}
	
	/**
	 * Method to set event.
	 * @param  event-the event is string.
	 */
	public void setEvent(String event) {
		this.event = event;
	}
	
	/**
	 * Method to get Property ClaimId.
	 * @return Property ClaimId as string.
	 */
	public String getPropertyClaimId() {
		return propertyClaimId;
	}
	
	/**
	 * Method to set Property ClaimId.
	 * @param  Property ClaimId-the Property ClaimId is string.
	 */
	public void setPropertyClaimId(String propertyClaimId) {
		this.propertyClaimId = propertyClaimId;
	}
	
	/**
	 * Method to get File name.
	 * @return File name as string.
	 */
	public String getFilename() {
		return filename;
	}
	
	/**
	 * Method to set File name.
	 * @param  File name-the File name is string.
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	/**
	 * Method to get Content Type.
	 * @return Content Type as string.
	 */
	public String getContentType() {
		return contentType;
	}
	
	/**
	 * Method to set Content Type.
	 * @param  Content Type-the Content Type is string.
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	/**
	 * Method to get Download Uri.
	 * @return Download Uri as string.
	 */
	public String getDownloadUri() {
		return downloadUri;
	}
	
	/**
	 * Method to set Download Uri.
	 * @param  Download Uri-the Download Uri is string.
	 */
	public void setDownloadUri(String downloadUri) {
		this.downloadUri = downloadUri;
	}
	
	/**
	 * Method to get Source Type.
	 * @return Source Type as string.
	 */
	public String getSourceType() {
		return sourceType;
	}
	
	/**
	 * Method to set Source Type.
	 * @param  Source Type-the Source Type is string.
	 */
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	
	/**
	 * Method to get Primary Id.
	 * @return Primary Id as string.
	 */
	public String getPrimaryId() {
		return primaryId;
	}
	
	/**
	 * Method to set primary Id.
	 * @param  primary Id-the primary Id is string.
	 */
	public void setPrimaryId(String primaryId) {
		this.primaryId = primaryId;
	}
	
	/**
	 * Method to get Tenant.
	 * @return Tenant as string.
	 */
	public String getTenant() {
		return tenant;
	}
	
	/**
	 * Method to set Tenant.
	 * @param  Tenant-the Tenant is string.
	 */
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}	
}