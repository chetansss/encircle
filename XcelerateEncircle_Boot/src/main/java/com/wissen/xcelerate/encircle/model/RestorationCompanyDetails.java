/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

/**
 * The persistent class for the rest_comp_details database table.\
 */
@Entity
@Table(name = "rest_comp_details")
//@NamedQuery(name="RestorationCompanyDetails.findAll", query="SELECT r FROM RestCompDetails r")
public class RestorationCompanyDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rest_comp_id")
	private int restCompId;

	@Column(name = "rest_comp_address")
	private String restCompAddress;

	@Column(name = "rest_comp_code")
	private String restCompCode;

	@Column(name = "rest_comp_email")
	private String restCompEmail;

	@Column(name = "rest_comp_hash_code")
	private String restCompHashCode;

	@Column(name = "rest_comp_inactive_date")
	private Timestamp restCompInactiveDate;

	@Column(name = "rest_comp_inactive_reason")
	private Timestamp restCompInactiveReason;

	@Column(name = "rest_comp_logo")
	private Integer restCompLogo;

	@Column(name = "rest_comp_max_users")
	private int restCompMaxUsers;

	@Column(name = "rest_comp_name")
	private String restCompName;

	@Column(name = "rest_comp_phone_number1")
	private String restCompPhoneNumber1;

	@Column(name = "rest_comp_phone_number2")
	private String restCompPhoneNumber2;

	@Column(name = "rest_comp_plan_id")
	private int restCompPlanId;

	@Column(name = "rest_comp_registered_date")
	private Timestamp restCompRegisteredDate;

	@Column(name = "rest_comp_service_end_date")
	private Timestamp restCompServiceEndDate;

	@Column(name = "rest_comp_service_start_date")
	private Timestamp restCompServiceStartDate;

	@Column(name = "rest_comp_setup_date")
	private Timestamp restCompSetupDate;

	@Column(name = "rest_comp_short_name")
	private String restCompShortName;

	@Column(name = "rest_comp_size")
	private String restCompSize;

	@Column(name = "rest_comp_status")
	private String restCompStatus;

	@Column(name = "rest_comp_title")
	private String restCompTitle;

	@Column(name = "updated_by")
	private Integer restCompUpdatedBy;

	@Column(name = "updated_datetime")
	private Timestamp restCompUpdatedDatetime;

	@Column(name = "rest_comp_fax_number")
	private String restCompFaxNumber;

	@Column(name = "source")
	private String restCompSource;

	@Column(name = "rest_comp_website")
	private String restCompWebsite;

	@Column(name = "encircle_api_token")
	private String encircleApiToken;

	@Column(name = "encircle_organization_id")
	private String encircleOrganizationId;

	@Column(name = "encircle_brand_id")
	private Integer encircleBrandId;

	@Column(name = "encircle_check")
	private String encircleCheck;

	@Column(name = "qb_check")
	private String qbCheck;

	@Column(name = "qb_token")
	private String qbToken;

	@Column(name = "qb_start_date")
	private Date qbStartDate;

	@Column(name = "app_url")
	private String appUrl;

	@Column(name = "customer_name_preference")
	private String customerNamePreference;

	public RestorationCompanyDetails() {
	}

	/**
	 * Method to get Restoration company ID.
	 * @return-restoration company ID as Integer.
	 */
	public int getRestCompId() {
		return this.restCompId;
	}

	/**
	 * Method to set Restoration company ID. 
	 * @param restCompId-the restoration company ID Integer.
	 */
	public void setRestCompId(int restCompId) {
		this.restCompId = restCompId;
	}

	/**
	 * Method to get Restoration company address.
	 * @return-restoration company address as String.
	 */
	public String getRestCompAddress() {
		return this.restCompAddress;
	}

	/**
	 * Method to set Restoration company address. 
	 * @param restCompAddress-the restoration company address String.
	 */
	public void setRestCompAddress(String restCompAddress) {
		this.restCompAddress = restCompAddress;
	}

	/**
	 * Method to get Restoration company code. 
	 * @return-restoration company code as String.
	 */
	public String getRestCompCode() {
		return this.restCompCode;
	}

	/**
	 * Method to set Restoration company code.  
	 * @param restCompCode-the restoration company code String.
	 */
	public void setRestCompCode(String restCompCode) {
		this.restCompCode = restCompCode;
	}

	/**
	 * Method to get Restoration company E-mail. 
	 * @return-restoration company Email as String.
	 */
	public String getRestCompEmail() {
		return this.restCompEmail;
	}

	/**
	 * Method to set Restoration company E-mail.  
	 * @param restCompEmail-the restoration company Email String.
	 */
	public void setRestCompEmail(String restCompEmail) {
		this.restCompEmail = restCompEmail;
	}

	/**
	 * Method to get Restoration company Hash code. 
	 * @return-restoration company hash code as String.
	 */
	public String getRestCompHashCode() {
		return this.restCompHashCode;
	}

	/**
	 * Method to set Restoration company Hash code. 
	 * @param restCompHashCode-the restoration company hash code String.
	 */
	public void setRestCompHashCode(String restCompHashCode) {
		this.restCompHashCode = restCompHashCode;
	}

	/**
	 * Method to get Restoration company inactive date. 
	 * @return-restoration company inactive date as Timestamp. 
	 */
	public Timestamp getRestCompInactiveDate() {
		return this.restCompInactiveDate;
	}

	/**
	 * Method to set Restoration company inactive date.  
	 * @param restCompInactiveDate-the restoration company inactive date Timestamp. 
	 */
	public void setRestCompInactiveDate(Timestamp restCompInactiveDate) {
		this.restCompInactiveDate = restCompInactiveDate;
	}

	/**
	 * Method to get Restoration company inactive reason. 
	 * @return-restoration company inactive reason as Timestamp. 
	 */
	public Timestamp getRestCompInactiveReason() {
		return this.restCompInactiveReason;
	}

	/**
	 * Method to set Restoration company inactive reason.  
	 * @param restCompInactiveReason-the restoration company inactive reason Timestamp. 
	 */
	public void setRestCompInactiveReason(Timestamp restCompInactiveReason) {
		this.restCompInactiveReason = restCompInactiveReason;
	}

	/**
	 * Method to get Restoration company logo. 
	 * @return-restoration company logo as Integer.
	 */
	public Integer getRestCompLogo() {
		return this.restCompLogo;
	}

	/**
	 * Method to set Restoration company logo.  
	 * @param restCompLogo-the restoration company logo Integer.
	 */
	public void setRestCompLogo(Integer restCompLogo) {
		this.restCompLogo = restCompLogo;
	}

	/**
	 * Method to get Restoration company max users. 
	 * @return-restoration company max users as Integer.
	 */
	public int getRestCompMaxUsers() {
		return this.restCompMaxUsers;
	}

	/**
	 * Method to set Restoration company max users.  
	 * @param restCompMaxUsers-the restoration company max users Integer.
	 */
	public void setRestCompMaxUsers(int restCompMaxUsers) {
		this.restCompMaxUsers = restCompMaxUsers;
	}

	/**
	 * Method to get Restoration company name. 
	 * @return-restoration company name as String. 
	 */
	public String getRestCompName() {
		return this.restCompName;
	}

	/**
	 * Method to set Restoration company name.  
	 * @param restCompName-the restoration company name String.
	 */
	public void setRestCompName(String restCompName) {
		this.restCompName = restCompName;
	}

	/**
	 * Method to get Restoration company phone number1. 
	 * @return-restoration company phone number1 as String.
	 */
	public String getRestCompPhoneNumber1() {
		return this.restCompPhoneNumber1;
	}

	/**
	 * Method to set Restoration company phone number1.  
	 * @param restCompPhoneNumber1-the restoration company phone number1 String.
	 */
	public void setRestCompPhoneNumber1(String restCompPhoneNumber1) {
		this.restCompPhoneNumber1 = restCompPhoneNumber1;
	}

	/**
	 * Method to get Restoration company phone number2. 
	 * @return-restoration company phone number2 as String.
	 */
	public String getRestCompPhoneNumber2() {
		return this.restCompPhoneNumber2;
	}

	/**
	 * Method to set Restoration company phone number2.   
	 * @param restCompPhoneNumber2-the restoration company phone number2 String.
	 */
	public void setRestCompPhoneNumber2(String restCompPhoneNumber2) {
		this.restCompPhoneNumber2 = restCompPhoneNumber2;
	}

	/**
	 * Method to get Restoration company plan ID. 
	 * @return-restoration company plan ID as Integer.
	 */
	public int getRestCompPlanId() {
		return this.restCompPlanId;
	}

	/**
	 * Method to set Restoration company plan ID.  
	 * @param restCompPlanId-the restoration company plan ID Integer.
	 */
	public void setRestCompPlanId(int restCompPlanId) {
		this.restCompPlanId = restCompPlanId;
	}

	/**
	 * Method to get Restoration company registered date. 
	 * @return-restoration company registered date as Timestamp.
	 */
	public Timestamp getRestCompRegisteredDate() {
		return this.restCompRegisteredDate;
	}

	/**
	 * Method to set Restoration company registered date.  
	 * @param restCompRegisteredDate-the restoration company registered date Timestamp.
	 */
	public void setRestCompRegisteredDate(Timestamp restCompRegisteredDate) {
		this.restCompRegisteredDate = restCompRegisteredDate;
	}

	/**
	 * Method to get Restoration company service end date. 
	 * @return-restoration company service end date as Timestamp. 
	 */
	public Timestamp getRestCompServiceEndDate() {
		return this.restCompServiceEndDate;
	}

	/**
	 * Method to set Restoration company serrvice end date.  
	 * @param restCompServiceEndDate-the restoration company service end date Timestamp. 
	 */
	public void setRestCompServiceEndDate(Timestamp restCompServiceEndDate) {
		this.restCompServiceEndDate = restCompServiceEndDate;
	}

	/**
	 * Method to get Restoration company service start date Timestamp. 
	 * @return-restoration company service start date as Timestamp.
	 */
	public Timestamp getRestCompServiceStartDate() {
		return this.restCompServiceStartDate;
	}

	/**
	 * Method to set Restoration company service start date Timestamp.  
	 * @param restCompServiceStartDate-the restoration company service start date Timestamp. 
	 */
	public void setRestCompServiceStartDate(Timestamp restCompServiceStartDate) {
		this.restCompServiceStartDate = restCompServiceStartDate;
	}

	/**
	 * Method to get Restoration company setup date. 
	 * @return-restoration company setup date as Timestamp. 
	 */
	public Timestamp getRestCompSetupDate() {
		return this.restCompSetupDate;
	}

	/**
	 * Method to set Restoration company setup date.  
	 * @param restCompSetupDate-the restoration company setup date Timestamp.
	 */
	public void setRestCompSetupDate(Timestamp restCompSetupDate) {
		this.restCompSetupDate = restCompSetupDate;
	}

	/**
	 * Method to get Restoration company short name.
	 * @return-restoration company  short name as String.
	 */
	public String getRestCompShortName() {
		return this.restCompShortName;
	}

	/**
	 * Method to set Restoration company short name. 
	 * @param restCompShortName-the restoration company  short name String.
	 */
	public void setRestCompShortName(String restCompShortName) {
		this.restCompShortName = restCompShortName;
	}

	/**
	 * Method to get Restoration company size. 
	 * @return-restoration company size as String. 
	 */
	public String getRestCompSize() {
		return this.restCompSize;
	}

	/**
	 * Method to set Restoration company size.  
	 * @param restCompSize-the restoration company size String.
	 */
	public void setRestCompSize(String restCompSize) {
		this.restCompSize = restCompSize;
	}

	/**
	 * Method to get Restoration company status. 
	 * @return-restoration company status as String.
	 */
	public String getRestCompStatus() {
		return this.restCompStatus;
	}

	/**
	 * Method to set Restoration company status.  
	 * @param restCompStatus-the restoration company status String.
	 */
	public void setRestCompStatus(String restCompStatus) {
		this.restCompStatus = restCompStatus;
	}

	/**
	 * Method to get Restoration company title. 
	 * @return-restoration company title as String. 
	 */
	public String getRestCompTitle() {
		return this.restCompTitle;
	}

	/**
	 * Method to set Restoration company title. 
	 * @param restCompTitle-the restoration company title String.
	 */
	public void setRestCompTitle(String restCompTitle) {
		this.restCompTitle = restCompTitle;
	}

	/**
	 * Method to get Restoration company updated by. 
	 * @return-restoration company updated by as Integer.
	 */
	public Integer getRestCompUpdatedBy() {
		return restCompUpdatedBy;
	}

	/**
	 * Method to set Restoration company updated by. 
	 * @param restCompUpdatedBy-the restoration company updated by Integer.
	 */
	public void setRestCompUpdatedBy(Integer restCompUpdatedBy) {
		this.restCompUpdatedBy = restCompUpdatedBy;
	}

	/**
	 * Method to get Restoration company updated date and time. 
	 * @return-restoration company updated date and time as Timestamp. 
	 */
	public Timestamp getRestCompUpdatedDatetime() {
		return restCompUpdatedDatetime;
	}

	/**
	 * Method to set restoration company updated date and time. 
	 * @param restCompUpdatedDatetime-the restoration company updated date and time Timestamp.
	 */
	public void setRestCompUpdatedDatetime(Timestamp restCompUpdatedDatetime) {
		this.restCompUpdatedDatetime = restCompUpdatedDatetime;
	}

	/**
	 * Method to get Restoration company fax number. 
	 * @return-restoration company fax number as String. 
	 */
	public String getRestCompFaxNumber() {
		return restCompFaxNumber;
	}

	/**
	 * Method to set Restoration company fax number.  
	 * @param restCompFaxNumber-the restoration company fax number as String.
	 */
	public void setRestCompFaxNumber(String restCompFaxNumber) {
		this.restCompFaxNumber = restCompFaxNumber;
	}

	/**
	 * Method to get Restoration company source. 
	 * @return-restoration company source as String.
	 */
	public String getRestCompSource() {
		return restCompSource;
	}

	/**
	 * Method to set Restoration company source.  
	 * @param restCompSource-the restoration company source String.
	 */
	public void setRestCompSource(String restCompSource) {
		this.restCompSource = restCompSource;
	}

	/**
	 * Method to get Restoration company website. 
	 * @return-restoration company website as String. 
	 */
	public String getRestCompWebsite() {
		return restCompWebsite;
	}

	/**
	 * Method to set Restoration company website.  
	 * @param restCompWebsite-the restoration company website String. 
	 */
	public void setRestCompWebsite(String restCompWebsite) {
		this.restCompWebsite = restCompWebsite;
	}

	/**
	 * Method to get encircle API token. 
	 * @return-encircle API token as String.
	 */
	public String getEncircleApiToken() {
		return encircleApiToken;
	}

	/**
	 * Method to set encircle API token.  
	 * @param encircleApiToken-the encircle API token String.
	 */
	public void setEncircleApiToken(String encircleApiToken) {
		this.encircleApiToken = encircleApiToken;
	}

	/**
	 * Method to get encircle organization ID. 
	 * @return-encircle organization ID as String.
	 */
	public String getEncircleOrganizationId() {
		return encircleOrganizationId;
	}

	/**
	 * Method to set encircle organization ID.  
	 * @param encircleOrganizationId-the encircle organization ID String. 
	 */
	public void setEncircleOrganizationId(String encircleOrganizationId) {
		this.encircleOrganizationId = encircleOrganizationId;
	}

	/**
	 * Method to get encircle brand ID. 
	 * @return-encircle brand ID as Integer.
	 */
	public Integer getEncircleBrandId() {
		return encircleBrandId;
	}

	/**
	 * Method to set encircle brand ID.  
	 * @param encircleBrandId-the encircle brand ID Integer.
	 */
	public void setEncircleBrandId(Integer encircleBrandId) {
		this.encircleBrandId = encircleBrandId;
	}

	/**
	 * Method to get encircle check. 
	 * @return-encircle check as String.
	 */
	public String getEncircleCheck() {
		return encircleCheck;
	}

	/**
	 * Method to set encircle check.  
	 * @param encircleCheck-the encircle check String.
	 */
	public void setEncircleCheck(String encircleCheck) {
		this.encircleCheck = encircleCheck;
	}

	/**
	 * Method to get QB check. 
	 * @return-QB check as String.
	 */
	public String getQbCheck() {
		return qbCheck;
	}

	/**
	 * Method to set QB check.  
	 * @param qbCheck-the QB check String.
	 */
	public void setQbCheck(String qbCheck) {
		this.qbCheck = qbCheck;
	}

	/**
	 * Method to get QB token.
	 * @return-QB token as String.
	 */
	public String getQbToken() {
		return qbToken;
	}

	/**
	 * Method to set QB token.
	 * @param qbToken-the QB token String.
	 */
	public void setQbToken(String qbToken) {
		this.qbToken = qbToken;
	}

	/**
	 * Method to get QB Start date. 
	 * @return-QB start date as Date.
	 */
	public Date getQbStartDate() {
		return qbStartDate;
	}

	/**
	 * Method to set QB Start date.  
	 * @param qbStartDate-the QB start date Date.
	 */
	public void setQbStartDate(Date qbStartDate) {
		this.qbStartDate = qbStartDate;
	}

	/**
	 * Method to get app URl. 
	 * @return-app URl as String.
	 */
	public String getAppUrl() {
		return appUrl;
	}

	/**
	 * Method to set app URl.  
	 * @param appUrl-the app URl String.
	 */
	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	/**
	 * Method to get customer name preference. 
	 * @return-customer name preference as String.
	 */
	public String getCustomerNamePreference() {
		return customerNamePreference;
	}

	/**
	 * Method to set customer name preference. 
	 * @param customerNamePreference-the customer name preference String.
	 */
	public void setCustomerNamePreference(String customerNamePreference) {
		this.customerNamePreference = customerNamePreference;
	}
}
