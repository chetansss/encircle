/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.wissen.xcelerate.encircle.config.RabbitMQProperties;
import com.wissen.xcelerate.encircle.pojo.MediaWebhook;
import com.wissen.xcelerate.encircle.tenant.TenantContext;

/**
 * Service class for Encircle webhook.
 */
@Service
public class EncircleWebhookServiceImpl implements EncircleWebhookService {
	private static final Logger LOGGER = LogManager.getLogger(EncircleWebhookServiceImpl.class);

	/**
	 * Method to get Media web hook.
	 * @param-the payload String.
	 * @return-webhhook as Media web hook.
	 */
	public MediaWebhook sendToQueue(String payload) {
		LOGGER.info("******** Encircle Webhook Payload is received into Service***********");
		System.out.println("payload is =======>" + payload);
		JsonObject jsonObject = new JsonParser().parse(payload).getAsJsonObject();
		String event = jsonObject.get("event").getAsString();
		System.out.println("event=========>" + event);
		JsonObject element = jsonObject.get(event).getAsJsonObject();
		System.out.println(event + "object====>" + element);
		String propertyClaimId = element.get("property_claim_id").getAsString();
		System.out.println("propertyClaimId==========>" + propertyClaimId);
		JsonObject media = element.get("media").getAsJsonObject();
		String filename = media.get("filename").getAsString();
		System.out.println("filename=========>" + filename);
		String contentType = media.get("content_type").getAsString();
		System.out.println("contentType======>" + contentType);
		String downloadUri = media.get("download_uri").getAsString();
		System.out.println("downloadUri=======>" + downloadUri);
		JsonObject source = media.get("source").getAsJsonObject();
		String sourceType = source.get("type").getAsString();
		System.out.println("sourceType=============>" + sourceType);
		String primaryId = source.get("primary_id").getAsString();
		System.out.println("primaryId==============>" + primaryId);
		MediaWebhook webhook = new MediaWebhook();
		webhook.setEvent(event);
		webhook.setPropertyClaimId(propertyClaimId);
		webhook.setFilename(filename);
		webhook.setContentType(contentType);
		webhook.setDownloadUri(downloadUri);
		webhook.setSourceType(sourceType);
		webhook.setPrimaryId(primaryId);
		String tenant = TenantContext.getCurrentTenant();
		webhook.setTenant(tenant);
		System.out.println("tenant before Queue Push ====>" + tenant);
		System.out.println(" ******** Encircle Webhook Payload is Pushed to Queue ********");
		LOGGER.info(" ******** Encircle Webhook Payload is Pushed to Queue ********");
		return webhook;
	}
}