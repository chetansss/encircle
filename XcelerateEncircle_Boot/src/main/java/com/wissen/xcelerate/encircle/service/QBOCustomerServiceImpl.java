/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wissen.xcelerate.encircle.config.RabbitMQProperties;
import com.wissen.xcelerate.encircle.model.JobDetails;
import com.wissen.xcelerate.encircle.model.MastNumDetails;
import com.wissen.xcelerate.encircle.model.common.RestCompDetails;
import com.wissen.xcelerate.encircle.pojo.Customer;
import com.wissen.xcelerate.encircle.pojo.JobUpdate;
import com.wissen.xcelerate.encircle.pojo.StatusMessage;
import com.wissen.xcelerate.encircle.repository.JobDetailsRepository;
import com.wissen.xcelerate.encircle.repository.RestorationCompanyRepository;
import com.wissen.xcelerate.encircle.util.XcelerateConstants;

/**
 * Class for QBO Customer service implementation.
 */
@Service
public class QBOCustomerServiceImpl implements QBOCustomerService {
	@Autowired
	private RabbitMQProperties rabbitMQProperties;
	@Autowired
	private RabbitTemplate rabbitTemplate;
	@Autowired
	private JobDetailsRepository jobDetailsRepository;
	@Autowired
	private RestorationCompanyRepository restCompRepository;

	/**
	 * Method to create QBPO Customer.
	 * @param customer-the customer JOB update.
	 */
	public void createQboCustomer(JobUpdate customer) {
		// System.out.println("Sending Customer details to Queue");
		// TODO Auto-generated method stub
		// System.out.println("Rabbit MQ Exchage name
		// ===>"+rabbitMQProperties.getExchangeName());
		// System.out.println("RabbitMQ Routing
		// key==>"+rabbitMQProperties.getRoutingKey());
		rabbitTemplate.convertAndSend(rabbitMQProperties.getExchangeName(), rabbitMQProperties.getRoutingKey(),
				customer);
	}

	/**
	 * Method to create QBO customer.
	 * @param-the Job ID Integer.
	 * @param-the company ID Integer.
	 */
	@Override
	public StatusMessage createQboCustomer(int jobId, int companyId) {
		// System.out.println("========Qbo Customer Creation is Block is
		// Excuiting=============");
		// System.out.println("QBo Job Id=========>"+jobId);
		// System.out.println("Company Id=========>"+companyId);
		StatusMessage message = new StatusMessage();
		try {
			boolean action = true;
			RestCompDetails restCompany = restCompRepository.findByRestCompId(companyId);
			if (restCompany != null) {
				
				// System.out.println("Company Id=========>"+restCompany.getRestCompId());
				JobDetails jobdetails = jobDetailsRepository.findByJobId(jobId);

				// System.out.println("JOb code =======>"+jobdetails.getJobCode());
				String qboStatus = jobdetails.getQboStatus();
				if (qboStatus != null && qboStatus.equals("Success")) {
					action = false;
				
					// System.out.println("***** QBO Customer is Already Present *******");
				}
				MastNumDetails mastNumDetails = jobdetails.getMastNumDetails();
				if (restCompany.getQboVerifierToken() != null
						&& restCompany.getQboVerifierToken().equalsIgnoreCase("true")) {
					if (action) {
						String customerNamePreference = restCompany.getCustomerNamePreference();
						
						// System.out.println("customerNamePreference=======>"+customerNamePreference);
						// creating Parent Customer details
						JobUpdate qboJob = new JobUpdate();
						qboJob.setRestCompId(companyId);
						Customer parent = new Customer();
						if (mastNumDetails.getCustomerType() != null) {
							if (mastNumDetails.getCustomerType().equalsIgnoreCase(XcelerateConstants.CUSTOMER)) {
								
								// System.out.println(mastNumDetails.getCustomerType());
								// here customer pref
								if (customerNamePreference.equals("Last, First")) {
									parent.setCustomerName(
											mastNumDetails.getLastName() + "," + " " + mastNumDetails.getFirstName());
						
									// System.out.println(mastNumDetails.getLastName()+","+"
									// "+mastNumDetails.getFirstName());
									parent.setFirstName(mastNumDetails.getFirstName());
									
									// System.out.println(mastNumDetails.getFirstName());
									parent.setLastName(mastNumDetails.getLastName());
									
									// System.out.println(mastNumDetails.getLastName());
								} else if (customerNamePreference.equals("First, Last")) {
									parent.setCustomerName(mastNumDetails.getFirstName() + "," + " " + 
								mastNumDetails.getLastName());
							
									// System.out.println(mastNumDetails.getFirstName()+","+"
									// "+mastNumDetails.getLastName());
									parent.setFirstName(mastNumDetails.getFirstName());
									// System.out.println(mastNumDetails.getFirstName());
									parent.setLastName(mastNumDetails.getLastName());
									// System.out.println(mastNumDetails.getLastName());
								}
								parent.setCustomerType(mastNumDetails.getCustomerType());
							} else {
								
								// System.out.println(mastNumDetails.getCustomerType());
								parent.setCustomerName(mastNumDetails.getCustomerCompanyName());
								
								// System.out.println(mastNumDetails.getCustomerCompanyName());
								parent.setCustomerType(mastNumDetails.getCustomerType());
							}
						}
						parent.setAddress1(mastNumDetails.getLossAddress());
						parent.setEmail(mastNumDetails.getEmail());
						parent.setCity(mastNumDetails.getLossAddressCity());
						parent.setState(mastNumDetails.getLossAddressState());
						parent.setZip(mastNumDetails.getLossAddressZip());
						parent.setMobile(mastNumDetails.getMobile());
						parent.setPhone(mastNumDetails.getPhone1());
						parent.setWorkPhone(mastNumDetails.getWorkPhone());
						parent.setLossDescription(mastNumDetails.getMastNumDesc());

						// Sub- Customer is Creation
						Customer childJob = new Customer();
						childJob.setCustomerName(jobdetails.getJobCode().replace("FN", "").replaceAll("_", " "));
						childJob.setFirstName(jobdetails.getFirstName());
						childJob.setLastName(jobdetails.getLastName());
						childJob.setAddress1(jobdetails.getBillingAddress());
						childJob.setEmail(jobdetails.getEmail());
						childJob.setCity(jobdetails.getBillingAddressCity());
						childJob.setState(jobdetails.getBillingAddressState());
						childJob.setZip(jobdetails.getBillingAddressZip());
						childJob.setMobile(jobdetails.getMobile());
						childJob.setPhone(jobdetails.getPhone1());
						childJob.setWorkPhone(jobdetails.getWorkPhone());
						childJob.setLossDescription(jobdetails.getJobDesc());
						if (jobdetails.getCustomerType() != null) {
							
							// System.out.println(jobdetails.getCustomerType());
							childJob.setCustomerType(jobdetails.getCustomerType());
						}
						qboJob.setParent(parent);
						qboJob.setJobDetails(childJob);
						createQboCustomer(qboJob);
						message.setStatus(XcelerateConstants.SUCCESS);
					}
				}
			} else {
				// System.out.println("Rest Company details are null");
			}
		} catch (Exception e) {
			
			// System.out.println(e.getLocalizedMessage());
			message.setStatus(XcelerateConstants.FAILURE);
			message.setMessage(e.getMessage());
			e.printStackTrace();
		}
		return message;
	}
}