/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.repository;

import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.wissen.xcelerate.encircle.model.common.RestCompDetails;

/**
 * Repository class for Restoration company.
 */
public interface RestorationCompanyRepository extends Repository<RestCompDetails, Long> {

	RestCompDetails findByWebhookDeliveryUrl(@Param("webhookDeliveryUrl") String webhookDeliveryUrl);

	RestCompDetails findByRestCompId(@Param("restCompId") int restCompId);
}
