/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.service;

/**
 * Interface for Restoration company.
 */
public interface RestorationCompanyService {

	/**
	 * Abstract method to get schema details by encircle delevery URL.
	 * @param url-the URL String.
	 */
	String getSchemaDetailsByEncircleDelivaryUrl(String url);
}
