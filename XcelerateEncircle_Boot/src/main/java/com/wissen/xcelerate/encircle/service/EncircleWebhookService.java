/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.service;

import org.springframework.stereotype.Service;

import com.wissen.xcelerate.encircle.pojo.MediaWebhook;

/**
 * Interface for Encircle webhook Service.
 */
@Service
public interface EncircleWebhookService {
	public MediaWebhook sendToQueue(String payload);
}