package com.wissen.xcelerate.encircle.repository;

/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.wissen.xcelerate.encircle.model.RestorationCompanyDetails;

/**
 * Repository class for restoration company schemas list of Restoration company
 * schema find All .
 */
public interface RestorationCompanySchemaRepository extends Repository<RestorationCompanyDetails, Long> {
	RestorationCompanyDetails findByRestCompId(@Param("restCompId") int restCompId);

	@Query("SELECT r FROM RestorationCompanyDetails r")
	List<RestorationCompanyDetails> findAll();

	RestorationCompanyDetails findByRestCompStatus(@Param("restCompStatus") String status);

	RestorationCompanyDetails findByRestCompName(@Param("restCompName") String restCompName);

	RestorationCompanyDetails save(RestorationCompanyDetails comps);
}
