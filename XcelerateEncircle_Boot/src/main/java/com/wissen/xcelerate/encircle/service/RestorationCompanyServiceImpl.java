/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wissen.xcelerate.encircle.model.common.RestCompDetails;
import com.wissen.xcelerate.encircle.repository.RestorationCompanyRepository;
import com.wissen.xcelerate.encircle.util.RestorationCompany;

/**
 * Service class for Restoration company implementation.
 */
@Service
public class RestorationCompanyServiceImpl implements RestorationCompanyService {

	@Autowired
	private RestorationCompanyRepository commonRepository;
	RestCompDetails companyDetails;

	/**
	 * Method to get Schema Details by Encircle Delivery URL.
	 * @return-schema as String.
	 */
	@Override
	public String getSchemaDetailsByEncircleDelivaryUrl(String url) {
		String schema = "";
		companyDetails = commonRepository.findByWebhookDeliveryUrl(url);
		if (companyDetails != null) {
			schema = companyDetails.getSchemaName();
			System.out.println("schema name===>" + schema);
			int compantId = companyDetails.getRestCompId();
			RestorationCompany company = new RestorationCompany();
			company.setCompanyId(compantId);
			System.out.println(compantId);
		} else {
			schema = null;
		}
		return schema;
	}

	/**
	 * Method to get Restoration company ID.
	 * @return-company ID as Integer.
	 */
	public int getRestorationCompanyId() {
		int companyId = 0;
		if (companyDetails != null) {
			companyId = companyDetails.getRestCompId();
		}
		return companyId;
	}
}