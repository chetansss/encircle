/**
* Copyright © 2019, xlrestorationsoftware.com ALL RIGHTS RESERVED.
* <p>
* This software is the confidential information of xlrestorationsoftware.com,
* and is licensed as restricted rights software. The use,reproduction, or 
* disclosure of this software is subject to restrictions set forth in your 
* license agreement with xlrestorationsoftware.com.
*/

package com.wissen.xcelerate.encircle.controller;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wissen.xcelerate.encircle.config.RabbitMQProperties;
import com.wissen.xcelerate.encircle.pojo.MediaWebhook;
import com.wissen.xcelerate.encircle.service.EncircleWebhookServiceImpl;

/**
 * Controller class for Encircle.
 */
@RestController
//@RequestMapping("/uat")
public class EncircleController {

	private static final Logger LOGGER = LogManager.getLogger(EncircleController.class);
	@Autowired
	private EncircleWebhookServiceImpl service;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private RabbitMQProperties rabbitMQProperties;

	/**
	 * Method to send post request to recieve webhook. 
	 * @param headers
	 * @param requestBody
	 * @return-response entity as String.
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> receiveWebhook(@RequestHeader MultiValueMap<String, String> headers,
			@RequestBody(required = false) String requestBody) {
		Set<String> keys = headers.keySet();
		for (String key : keys) {
			LOGGER.info("header in controller....");
			LOGGER.info("Key ===>" + key);
			LOGGER.info("Values =>= " + headers.get(key));
			LOGGER.info("============ header  values reading in controller Completed=========");
		}
		if (requestBody != null) {
			System.out.println("Reeached 1.....");

			System.out.println("requestBody in controller...." + requestBody);
			LOGGER.info("requestBody in controller...." + requestBody);
			MediaWebhook webhhok = service.sendToQueue(requestBody);

			rabbitTemplate.convertAndSend(rabbitMQProperties.getEncircleWebhookExchange(),
					rabbitMQProperties.getEncirclePropertyClaimMediaRoutingKey(), webhhok);
		} else {
			System.out.println("No request payload");
			LOGGER.info("No Request payload" + headers);
		}
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("accept", "allow");
		return ResponseEntity.ok().headers(responseHeaders).body("Allow");
	}

	/**
	 * Method to verify the application status.
	 */
	@RequestMapping(value = "/uat/health", method = RequestMethod.GET)
	public void heartBeat() {
		System.out.println("Application is Up and Running");
		LOGGER.info("Application is Up and Running");
	}
}